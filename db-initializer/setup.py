import pandas as pd
import json
import os
from py2neo import Graph
import urllib.request
from py2neo.data import Relationship
import uuid

PATH_JSON = "https://gitlab.com/stairwai/wp3/knowledge-representation/-/raw/developer/swai_ams.json"

class Driver:
    def __init__(self, uri_l, user_n, password_n):
        # initialize the connection to the Neo4j database
        self.graph = Graph(uri_l, auth=(user_n, password_n))
        # Initialize the database
        self.init_db()
        # Generate fix data-structures from the static file
        _, class_owl, _, _, technique_hierarchy, business_hierarchy, _, _, _, owg_terms, _ = self.read_var_file()
        self.create_instance_of(class_owl, technique_hierarchy, business_hierarchy, owg_terms)

    @staticmethod
    def read_var_file():
        """
        This method load all the fix data-structures of the AMS from a json file.
        """
        # load all the static data-structures from the file
        with urllib.request.urlopen(PATH_JSON) as url:
            res = json.load(url)

        return (res['uri'], pd.DataFrame.from_dict(res['class_owl'], orient='index'),
                pd.DataFrame.from_dict(res['properties'], orient='index'), res['inverses'], res['technique_hierarchy'],
                res['business_hierarchy'], res['insta_classes'], res['forbidden_clauses'], res['hwp2env'],
                res['owg_terms'], res['prefixes'])

    def get_inverse_dict(self):
        """
        This method is used to generate the dictionary where the inverse relations are stored, obtaining a mapping
        between inverse relations extracted from the ontology imported.
        :return: a dictionary in which the inverse relation is obtained based on key-value relation.
        """
        # get all the object properties of the ontology
        res_dict = {}
        for node in self.graph.nodes.match("owl__ObjectProperty").all():
            # find the database name of the property
            db_name = node["swai__ontologyName"]
            # find the db_name of the inverse relation
            inverse_rel = self.graph.match((node,), r_type="owl__inverseOf").first()
            inverse_db_name = inverse_rel.end_node["swai__ontologyName"] if inverse_rel is not None else None
            # if it does not have an inverse, set the value to none
            if inverse_db_name is None and db_name not in list(res_dict.keys()):
                res_dict[db_name] = None
            # if the relation has an inverse
            if inverse_db_name is not None:
                res_dict[db_name] = inverse_db_name
                res_dict[inverse_db_name] = db_name
        return res_dict

    def __extract_properties(self, class_owl):
        """
        This function extract the main characteristics of all the current properties (data or object properties), and
        store them in a pandas dataframe. Characteristics such as: label, DB name, domain, range, etc.
        :return: it returns the generated dataframe.
        """
        # get all the data properties of the ontology
        data = []
        for node in self.graph.nodes.match("owl__DatatypeProperty").all():
            # find the label and the database name of the property
            label = node["rdfs__label"]
            db_name = node["swai__ontologyName"]
            # find the labels of all the domain classes
            domain = []
            domain_rel = self.graph.match((node,), r_type="rdfs__domain").first()
            domain_node = domain_rel.end_node if domain_rel is not None else None
            if domain_node is not None and domain_node["rdfs__label"] is None and domain_node in self.graph.nodes.match(
                    "owl__Class", ).all():
                domain += self.__recursive_search(
                    self.graph.match((domain_node,), r_type="owl__unionOf").first().end_node, [])
            elif domain_node is not None and domain_node["rdfs__label"] is not None:
                domain.append(domain_node["rdfs__label"])
            # inference the subdomain
            domain = self.inference_subclasses(domain, class_owl)
            domain = [d for d in domain if d not in ['Organization', 'organización']]
            data.append(
                {'label': label, 'db_name': db_name, 'domain': domain, 'range': None, 'type': 'DatatypeProperty'})

        # get all the object properties of the ontology
        for node in self.graph.nodes.match("owl__ObjectProperty").all():
            # find the label and the database name of the property
            label = node["rdfs__label"]
            db_name = node["swai__ontologyName"]
            # find the labels of all the domain classes
            domain = []
            domain_rel = self.graph.match((node,), r_type="rdfs__domain").first()
            domain_node = domain_rel.end_node if domain_rel is not None else None
            if domain_node is not None and domain_node["rdfs__label"] is None and domain_node in self.graph.nodes.match(
                    "owl__Class", ).all():
                domain += self.__recursive_search(
                    self.graph.match((domain_node,), r_type="owl__unionOf").first().end_node, [])
            elif domain_node is not None and domain_node["rdfs__label"] is not None:
                domain.append(domain_node["rdfs__label"])
            # inference the subdomain
            domain = self.inference_subclasses(domain, class_owl)
            domain = [d for d in domain if d not in ['Organization', 'organización']]
            # find the labels of all the range classes
            neo_range = []
            range_rel = self.graph.match((node,), r_type="rdfs__range").first()
            range_node = range_rel.end_node if range_rel is not None else None
            if range_node is not None and range_node["rdfs__label"] is None and range_node in self.graph.nodes.match(
                    "owl__Class", ).all():
                neo_range += self.__recursive_search(
                    self.graph.match((range_node,), r_type="owl__unionOf").first().end_node, [])
            elif range_node is not None and range_node["rdfs__label"] is not None:
                neo_range.append(range_node["rdfs__label"])
            # inference the sub-range
            neo_range = self.inference_subclasses(neo_range, class_owl)
            neo_range = [r for r in neo_range if r not in ['Organization', 'organización']]
            # store the data of the node in the list of dictionaries
            data.append(
                {'label': label, 'db_name': db_name, 'domain': domain, 'range': neo_range, 'type': 'ObjectProperty'})
        return pd.DataFrame(data, columns=["label", "db_name", "domain", "range", "type"])

    def inference_subclasses(self, classes, class_owl):
        """
        This method is used to do a recursive search through the ontology hierarchy with the aim of finding the children
        classes of each class in a set of classes.
        :param classes: 1D array of classes in the ontology hierarchy.
        :param class_owl: data structure generated during the JSON file creation.
        :return: a set of classes composed by the original classes and the new ones.
        """
        if len(classes) > 0:
            for class_name in classes:
                name = self.graph.run(
                    "MATCH (n:owl__Class {rdfs__label: $topic_name})<-[r:rdfs__subClassOf*]-(child:owl__Class)"
                    "RETURN child.rdfs__label", topic_name=str(class_name))
                if name is not None:
                    classes += list(name.to_ndarray().ravel())
            classes = list(set(classes))
        else:
            classes = class_owl.copy()['label'].tolist()
        return classes

    def __recursive_search(self, inter_node, labels):
        """
        This function is the recursive part of the characteristics' extraction (range and domain) from all the properties
        :param inter_node: node from which we will analise the connections.
        :param labels: the extracted labels of the classes found.
        :return: the labels
        """
        # find the desired connected nodes
        rel_eval = self.graph.match((inter_node,), r_type="rdf__first").first()
        # if empty, return empty list
        if rel_eval is None:
            return []
        elif rel_eval.end_node["rdfs__label"] is not None:
            labels += [rel_eval.end_node["rdfs__label"]]
        # recursive call searching for more categories
        labels += self.__recursive_search(self.graph.match((inter_node,), r_type="rdf__rest").first().end_node, labels)
        return list(set(labels))

    def create_instance_of(self, class_owl, ai_techniques, business, owg_terms):
        """
        This function create the instanceOf relations that the current Neo4j importing function is not capable to do.
        :param class_owl: data structure generated during the JSON file creation.
        :param ai_techniques: lis of the AI Techniques in the AMS
        :param business: business areas extracted from the AMS
        :param owg_terms: ontology working group terms.
        """
        classes = list(class_owl["label"])
        for cls in classes:
            # choose the prefix required
            if cls == "Country":
                prx = 'cry__'
            elif cls in business:
                prx = 'aicat__'
            elif cls in ai_techniques:
                prx = 'aitec__'
            elif cls in owg_terms:
                prx = 'owg__'
            else:
                prx = "swai__"
            # from label to db_name
            cls = class_owl.loc[class_owl["label"] == cls].values[0][1]
            # get the correct Class node for the relation
            end_nodes = self.graph.nodes.match("owl__Class", swai__ontologyName=cls).all()
            end_node = None
            # quit external nodes (Equivalent classes)
            if len(end_nodes) == 1:
                end_node = end_nodes[0]
            else:
                for end in end_nodes:
                    if "swai/spec" in end["uri"] or 'stairwai' in end["uri"]:
                        end_node = end
            # generate the relations
            for start_node in self.graph.nodes.match(prx + cls).all():
                rel = Relationship(start_node, "owl__instanceOf", end_node)
                # if it doesn't exist, add the relationship to the DB
                if rel not in self.graph.match((start_node,), r_type="owl__instanceOf").all():
                    self.graph.create(rel)
        # generate StairwAI ids
        for individual in self.graph.nodes.match('owl__NamedIndividual').all():
            individual['swai__identifier'] = individual.identity
            individual['swai__uuidIdentifier'] = str(uuid.uuid1())
            self.graph.push(individual)

    def init_db(self):
        """
        This function initialize a StairwAI AMS new instance running the importing ontology commands and setting the
        prefixes required.
        """
        # initialise the database using the commands to set the prefixes and loading the ontologies
        print("Importing ontologies...")
        self.graph.run("CALL n10s.graphconfig.init();")
        self.graph.run("CREATE CONSTRAINT n10s_unique_uri ON (r:Resource) ASSERT r.uri IS UNIQUE;")
        self.graph.run("CALL n10s.nsprefixes.add('cry', 'https://stairwai.gitlab.io/wp3/ontology/Country.rdf#');")
        self.graph.run("CALL n10s.nsprefixes.add('aicat', 'https://stairwai.gitlab.io/wp3/ontology/topics.rdf#');")
        self.graph.run("CALL n10s.nsprefixes.add('aitec', 'https://stairwai.gitlab.io/wp3/ontology/ai_onto.rdf#');")
        self.graph.run("CALL n10s.nsprefixes.add('swai', 'http://purl.org/swai/spec#');")
        self.graph.run("CALL n10s.nsprefixes.add('owg', 'http://purl.org/owg/onto/spec#');")
        self.graph.run("CALL n10s.nsprefixes.add('foaf', 'http://xmlns.com/foaf/0.1/');")
        self.graph.run("CALL n10s.nsprefixes.add('dc', 'http://purl.org/dc/elements/1.1/');")
        self.graph.run("CALL n10s.nsprefixes.add('dcterms', 'http://purl.org/dc/terms/');")
        self.graph.run("CALL n10s.rdf.import.fetch('http://purl.org/swai/spec','RDF/XML');")
        self.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/topics.rdf', 'RDF/XML');")
        self.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/Country.rdf', 'RDF/XML');")
        self.graph.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/ai_onto.rdf', 'RDF/XML');")


# Generate the json file
# driver = Driver(uri_l='bolt://localhost:7687', user_n='neo4j', password_n='s3cr3t')
driver = Driver(uri_l='bolt://neo4j-service:7687', user_n='neo4j', password_n='s3cr3t')
