import json
import os
import urllib.request

import pandas as pd
from py2neo import Graph
from py2neo.data import Relationship

# PATH_JSON = "https://gitlab.com/stairwai/wp3/knowledge-representation/-/raw/main/serverhosted-version/swai_ams.json"
# PATH_JSON = "https://gitlab.com/stairwai/wp3/knowledge-representation/-/raw/developer/serverhosted-version/swai_ams.json"
PATH_JSON = "https://gitlab.com/stairwai/wp3/knowledge-representation/-/raw/developer/swai_ams.json"


def get_env_var(name, default=None):
    if name in os.environ:
        return os.environ[name]
    else:
        return default


def create_instance_of(driver, class_owl, business_hierarchy, technique_hierarchy):
    classes = list(class_owl["label"])
    for cls in classes:
        # choose the prefix required
        if cls == "Country":
            prx = 'cry__'
        elif cls in business_hierarchy:
            prx = 'aicat__'
        elif cls in technique_hierarchy:
            prx = 'aitec__'
        else:
            prx = 'swai__'

        # from label to db_name
        cls = class_owl.loc[class_owl["label"] == cls].values[0][1]

        # get the correct Class node for the relation
        end_nodes = driver.nodes.match("owl__Class", swai__ontologyName=cls).all()
        end_node = None

        # quit external nodes (Equivalent classes)
        if len(end_nodes) == 1:
            end_node = end_nodes[0]
        else:
            for end in end_nodes:
                if "swai/spec" in end["uri"] or 'stairwai' in end["uri"]:
                    end_node = end

        # generate the relations
        for start_node in driver.nodes.match(prx + cls).all():
            rel = Relationship(start_node, "owl__instanceOf", end_node)

            # if it doesn't exist, add the relationship to the DB
            if rel not in driver.match((start_node,), r_type="owl__instanceOf").all():
                driver.create(rel)


def read_var_file(path=None):
    """
    This method load all the fix data-structures of the AMS from a json file.
    """
    if path is None:
        path = PATH_JSON
    with urllib.request.urlopen(path) as url:
        res = json.load(url)
    return res['uri'], pd.DataFrame.from_dict(res['class_owl'], orient='index'), \
        pd.DataFrame.from_dict(res['properties'], orient='index'), res['inverses'], res['technique_hierarchy'], \
        res['business_hierarchy'], res['no_in_classes'], res['extra_classes'], res['forbidden_clauses']


# get the environment variables
bolt_url = get_env_var('NEO4J_URL', 'bolt://localhost:7687')
user = get_env_var('NEO4J_USER', 'neo4j')
password = get_env_var('NEO4J_PASSWORD', 's3cr3t')

# init the database
driver = Graph(bolt_url, auth=(user, password))

# import the ontologies required and create the prefixes
if len(driver.nodes.match("owl__Class").all()) == 0:
    print("Initialising the Neo4j database...")
    driver.run("CALL n10s.graphconfig.init();")
    driver.run("CREATE CONSTRAINT n10s_unique_uri ON (r:Resource) ASSERT r.uri IS UNIQUE;")
    driver.run("CALL n10s.nsprefixes.add('cry', 'https://stairwai.gitlab.io/wp3/ontology/Country.ttl#');")
    driver.run("CALL n10s.nsprefixes.add('aicat', 'https://stairwai.gitlab.io/wp3/ontology/topics.ttl#');")
    driver.run("CALL n10s.nsprefixes.add('aitec', 'https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#');")
    driver.run("CALL n10s.nsprefixes.add('swai', 'http://purl.org/swai/spec#');")
    driver.run("CALL n10s.nsprefixes.add('foaf', 'http://xmlns.com/foaf/0.1/');")
    driver.run("CALL n10s.nsprefixes.add('dc', 'http://purl.org/dc/elements/1.1/');")
    driver.run("CALL n10s.nsprefixes.add('dcterms', 'http://purl.org/dc/terms/');")
    driver.run("CALL n10s.rdf.import.fetch('http://purl.org/swai/spec','RDF/XML');")
    driver.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/topics.ttl', 'RDF/XML');")
    driver.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/Country.ttl', 'RDF/XML');")
    driver.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl', 'RDF/XML');")

    for individual in driver.nodes.match('owl__NamedIndividual').all():
        individual['swai__identifier'] = individual.identity
        driver.push(individual)

    _, class_owl, _, _, technique_hierarchy, business_hierarchy, _, _, _ = read_var_file()
    create_instance_of(driver, class_owl, business_hierarchy, technique_hierarchy)

# from py2neo import Graph
# import os


# def get_env_var(name, default=None):
#     if name in os.environ:
#         return os.environ[name]
#     else:
#         return default


# # get the environment variables
# bolt_url = get_env_var('NEO4J_URL', 'bolt://localhost:7687')
# user = get_env_var('NEO4J_USER', 'neo4j')
# password = get_env_var('NEO4J_PASSWORD', 's3cr3t')

# # init the database
# driver = Graph(bolt_url, auth=(user, password))

# # import the ontologies required and create the prefixes
# if len(driver.nodes.match("owl__Class").all()) == 0:
#     print("Initialising the Neo4j database...")
#     driver.run("CALL n10s.graphconfig.init();")
#     driver.run("CREATE CONSTRAINT n10s_unique_uri ON (r:Resource) ASSERT r.uri IS UNIQUE;")
#     driver.run("CALL n10s.nsprefixes.add('cry', 'https://stairwai.gitlab.io/wp3/ontology/Country.ttl#');")
#     driver.run("CALL n10s.nsprefixes.add('aicat', 'https://stairwai.gitlab.io/wp3/ontology/topics.ttl#');")
#     driver.run("CALL n10s.nsprefixes.add('aitec', 'https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl#');")
#     driver.run("CALL n10s.nsprefixes.add('swai', 'http://purl.org/swai/spec#');")
#     driver.run("CALL n10s.nsprefixes.add('foaf', 'http://xmlns.com/foaf/0.1/');")
#     driver.run("CALL n10s.nsprefixes.add('dc', 'http://purl.org/dc/elements/1.1/');")
#     driver.run("CALL n10s.nsprefixes.add('dcterms', 'http://purl.org/dc/terms/');")
#     driver.run("CALL n10s.rdf.import.fetch('http://purl.org/swai/spec','RDF/XML');")
#     driver.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/topics.ttl', 'RDF/XML');")
#     driver.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/Country.ttl', 'RDF/XML');")
#     driver.run("CALL n10s.rdf.import.fetch('https://stairwai.gitlab.io/wp3/ontology/ai_onto.ttl', 'RDF/XML');")

#     for individual in driver.nodes.match('owl__NamedIndividual').all():
#         individual['swai__identifier'] = individual.identity
#         driver.push(individual)
