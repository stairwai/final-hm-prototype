# StairwAI Asset Management System 

This directory has the code to initialize the StairwAI AMS Neo4j database as a Docker in a Server.

## Requirements

Basic system requirements:
- Docker and docker-compose installed (version 2.2.2, or greater).

## Steps to install and run the AMS

Assuming that Docker Compose in installed:
1. Download the GitLab repository

2. Run `docker-compose up`
	- this should start a Neo4j instance and then start and or modify the `setup.py` script

3. Recall to run `Ctrl + C` to the running docker terminal and `docker-compose down` once the test is over


## Contact points

- miquel.buxons@upc.edu
- javier.vazquez-salceda@upc.edu

