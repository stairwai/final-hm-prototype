from shared.configurations.file_manager import register_file_managers
from shared.configurations.data_manager import register_data_managers
from shared.configurations.processor import register_processors


def register():
    register_file_managers()
    register_data_managers()
    register_processors()
