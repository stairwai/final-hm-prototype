from core.configuration import Configuration, ParameterSet
from core.registry import Registry, Framework
from shared.components.file_manager import FileManager


class FileManagerConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = ParameterSet()

        # Directories
        parameter_set.add_short(name='base_directory',
                                description="parent directory from where to build local paths."
                                )
        parameter_set.add_short(name='logging_directory',
                                value='logging',
                                description="directory name for library logger")
        parameter_set.add_short(name='data_directory',
                                value='data',
                                description="directory name for storing datasets")
        parameter_set.add_short(name='registrations_directory',
                                value='registrations',
                                description="directory name for storing components and configurations registrations")
        parameter_set.add_short(name='calibrations_directory',
                                value='calibrations',
                                description="directory name for storing calibrated configurations")
        parameter_set.add_short(name='calibration_tasks_directory',
                                value='calibration_tasks',
                                description="directory name for storing calibration tasks")
        parameter_set.add_short(name='routines_directory',
                                value='routines',
                                description="directory name where pre-computed routine data "
                                            "(e.g., pre-built cv folds) is stored")
        parameter_set.add_short(name='serializations_directory',
                                value='serializations',
                                description="directory name where serialized data is stored")
        parameter_set.add_short(name='tasks_directory',
                                value='tasks',
                                description="directory name where task results are stored")

        # Filenames
        parameter_set.add_short(name='logging_filename',
                                value='daily_log.log',
                                description="filename for logging")
        parameter_set.add_short(name='serialization_registry_filename',
                                value='serialization_registry.json',
                                description="filename for metafile registry concerning data serialization")
        parameter_set.add_short(name='task_summary_filename',
                                value='task_summary.json',
                                description="")
        parameter_set.add_short(name='calibration_results_filename',
                                value='calibration_results.json',
                                description="filename of summary file storing calibrated model configurations.")

        # Conditions
        parameter_set.add_condition_short(name='base_directory_existence',
                                          condition=lambda parameters: 'base_directory' in parameter_set)

        return parameter_set


def register_file_managers():
    Registry.register_and_map(configuration=FileManagerConfig.get_default(),
                              component_class=FileManager,
                              framework=Framework.GENERIC,
                              name='file_manager',
                              is_default=True)
