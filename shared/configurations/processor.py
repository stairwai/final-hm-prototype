"""

TODO: add documentation

"""

import re
from typing import List, Callable

from core.configuration import Configuration
from core.data import ParameterSet
from core.registry import Framework, Registry, RegistrationKey
from shared.components.processor import TextProcessor, LabelProcessor, PipelineProcessor


class TextProcessorConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = ParameterSet()

        parameter_set.add_short(name='filters',
                                typing=List[Callable],
                                value=[
                                    lambda text: re.sub(r'https?:\/\/.*[\r\n]*', '<LINK>', text,
                                                        flags=re.MULTILINE),
                                    lambda text: text.strip(),
                                ],
                                description='List of filter functions that accept a text as input',
                                affects_serialization=True)

        return parameter_set


class LabelProcessorConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        return ParameterSet()


class PipelineProcessorConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = ParameterSet()

        parameter_set.add_short(name='processors',
                                value=[
                                    RegistrationKey(name='processor',
                                                    framework=Framework.GENERIC,
                                                    tags={'default', 'text'}),
                                    RegistrationKey(name='processor',
                                                    framework=Framework.GENERIC,
                                                    tags={'default', 'label'})
                                ],
                                is_registration=True,
                                is_required=True,
                                description='List of processor components for input processing')

        return parameter_set


def register_processors():
    # TextProcessor
    Registry.register_and_map(configuration=TextProcessorConfig.get_default(),
                              component_class=TextProcessor,
                              framework=Framework.GENERIC,
                              name='processor',
                              tags={'text'},
                              is_default=True)

    # LabelProcessor
    Registry.register_and_map(configuration=LabelProcessorConfig.get_default(),
                              component_class=LabelProcessor,
                              framework=Framework.GENERIC,
                              name='processor',
                              tags={'label'},
                              is_default=True)

    # PipelineProcessor
    Registry.register_and_map(configuration=PipelineProcessorConfig.get_default(),
                              component_class=PipelineProcessor,
                              framework=Framework.GENERIC,
                              name='processor',
                              tags={'pipeline'},
                              is_default=True)
