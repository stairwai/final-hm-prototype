from core.configuration import Configuration
from core.data import ParameterSet
from shared.components.data_manager import DataManager
from shared.components.processor import Processor


class DataTaskConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = ParameterSet()
        parameter_set.add_short(name='name',
                                typing=str,
                                is_required=True,
                                description="Task unique identifier.")
        parameter_set.add_short(name='data_manager',
                                typing=DataManager,
                                is_required=True,
                                is_registration=True,
                                description='Data manager component for loading data.')
        parameter_set.add_short(name='processor',
                                typing=Processor,
                                is_registration=True,
                                description="Processor pipeline for data processing")
        return parameter_set
