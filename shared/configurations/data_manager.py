from typing import AnyStr

from core.configuration import Configuration
from core.data import ParameterSet
from core.registry import Registry, Framework, RegistrationKey
from shared.components.data_manager import FileLabelManager, FileQueryManager, FileAssetManager, AMSLabelManager, \
    FileExpertsManager, AMSExpertsManager, FileEduResourceManager, FilePaperManager, AMSEduManager, AMSPaperManager


class DataManagerConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = ParameterSet()

        parameter_set.add_short(name='data_name',
                                is_required=True,
                                typing=str,
                                description='Unique data identifier')
        return parameter_set


class FileDataManagerConfig(DataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.add_short(name='file_manager_regr_key',
                                typing=RegistrationKey,
                                description="registration key of built FileManager component. "
                                            "Used for filesystem interfacing",
                                value=RegistrationKey(
                                    framework=Framework.GENERIC,
                                    name='file_manager',
                                    tags={'default'}
                                ))
        parameter_set.add_short(name='folder',
                                is_required=True,
                                typing=AnyStr,
                                description='Folder where data is stored')
        parameter_set.add_short(name='filename',
                                is_required=True,
                                typing=AnyStr,
                                description='Filename for storing data to load')
        parameter_set.add_short(name='resource_limit',
                                typing=int,
                                value=-1,
                                description='Maximum amount of instances to load. '
                                            'A value of -1 indicates no loading limit.')
        parameter_set.add_short(name='overwrite',
                                value=False,
                                typing=bool,
                                description='If enabled, loaded data is saved using filepath instead of update_filepath'
                                            'Thus, originally loaded data is overwritten.')
        parameter_set.add_short(name='update_filename',
                                is_required=True,
                                typing=AnyStr,
                                description='Filename for storing loaded data')

        return parameter_set


class FileLabelManagerConfig(FileDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Labels'
        parameter_set.folder = 'labels'
        parameter_set.filename = 'labels.json'
        parameter_set.update_filename = 'loaded_labels.pkl'

        return parameter_set


class FileExpertsManagerConfig(FileDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Experts'
        parameter_set.folder = 'resources'
        parameter_set.filename = 'ai-experts-2024-02.csv'
        # parameter_set.filename = 'experts_3rd.csv'
        parameter_set.update_filename = 'loaded_experts.pkl'

        return parameter_set


class FileFakeExpertsManagerConfig(FileDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Experts'
        parameter_set.folder = 'resources'
        parameter_set.filename = 'experts-fake.csv'
        parameter_set.update_filename = 'loaded_experts.pkl'

        return parameter_set


class FileQueryManagerConfig(FileDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Queries'
        parameter_set.folder = 'open_call_2'
        parameter_set.filename = 'StairwAI_2ndOC_applications with labels.xlsx'
        parameter_set.update_filename = 'loaded_queries.pkl'

        return parameter_set


class FileAssetManagerConfig(FileDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Assets'
        parameter_set.folder = 'resources'
        parameter_set.filename = 'ai-catalog-v2.csv'
        parameter_set.update_filename = 'loaded_assets.pkl'

        return parameter_set


class FilePaperManagerConfig(FileDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Papers'
        parameter_set.folder = 'resources'
        parameter_set.filename = 'ai-papers.csv'
        parameter_set.update_filename = 'loaded_papers.pkl'

        return parameter_set


class FileEduResourceManagerConfig(FileDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Edu'
        parameter_set.folder = 'resources'
        parameter_set.filename = 'ai-edu.csv'
        parameter_set.update_filename = 'loaded_edu.pkl'

        return parameter_set


class AMSDataManagerConfig(DataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.add_short(name='with_details',
                                value=True,
                                typing=bool,
                                description='If enabled, labels are retrieved with metadata from AMS DB.')

        return parameter_set


class AMSLabelManagerConfig(AMSDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Labels'
        return parameter_set


class AMSExpertsManagerConfig(AMSDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Experts'
        return parameter_set


class AMSEduManagerConfig(AMSDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Edu'
        return parameter_set


class AMSPaperManagerConfig(AMSDataManagerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.data_name = 'Paper'
        return parameter_set


def register_data_managers():
    # Labels

    # FileLabelManager
    Registry.register_and_map(configuration=FileLabelManagerConfig.get_default(),
                              component_class=FileLabelManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'labels', 'file'},
                              is_default=True)

    # AMSLabelManager
    Registry.register_and_map(configuration=AMSLabelManagerConfig.get_default(),
                              component_class=AMSLabelManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'labels', 'ams'},
                              is_default=True)

    # Queries

    # FileQueryManager
    Registry.register_and_map(configuration=FileQueryManagerConfig.get_default(),
                              component_class=FileQueryManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'queries', 'file'},
                              is_default=True)

    # Experts

    # FileExpertsManager
    Registry.register_and_map(configuration=FileExpertsManagerConfig.get_default(),
                              component_class=FileExpertsManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'experts', 'file'},
                              is_default=True)

    Registry.register_and_map(configuration=FileFakeExpertsManagerConfig.get_default(),
                              component_class=FileExpertsManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'experts', 'fake', 'file'},
                              is_default=True)

    # AMSExpertsManager
    Registry.register_and_map(configuration=AMSExpertsManagerConfig.get_default(),
                              component_class=AMSExpertsManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'experts', 'ams'},
                              is_default=True)

    # Assets

    # FileAssetsManager
    Registry.register_and_map(configuration=FileAssetManagerConfig.get_default(),
                              component_class=FileAssetManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'assets', 'file'},
                              is_default=True)

    # Papers
    Registry.register_and_map(configuration=FilePaperManagerConfig.get_default(),
                              component_class=FilePaperManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'paper', 'file'},
                              is_default=True)

    Registry.register_and_map(configuration=AMSPaperManagerConfig.get_default(),
                              component_class=AMSPaperManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'paper', 'ams'},
                              is_default=True)

    # Edu Resources
    Registry.register_and_map(configuration=FileEduResourceManagerConfig.get_default(),
                              component_class=FileEduResourceManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'edu', 'file'},
                              is_default=True)

    Registry.register_and_map(configuration=AMSEduManagerConfig.get_default(),
                              component_class=AMSEduManager,
                              framework=Framework.GENERIC,
                              name='data_manager',
                              tags={'edu', 'ams'},
                              is_default=True)
