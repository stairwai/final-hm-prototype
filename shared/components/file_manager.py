import os
from typing import AnyStr

from core.component import Component


class FileManager(Component):

    def setup(
            self,
            base_directory: AnyStr
    ):
        self.base_directory = base_directory

    def run(
            self,
            filepath: AnyStr,
            strict_existence: bool = False,
    ):
        built_path = os.path.join(self.base_directory, filepath)
        path_exists = os.path.isdir(built_path)
        if strict_existence and not path_exists:
            raise RuntimeError(f'Expected path={built_path} to exist and it is not.')

        if not path_exists:
            os.makedirs(built_path)

        return built_path
