"""

TODO: add documentation

"""

import abc
from functools import reduce
from typing import Union, Iterable

from core.component import Component
from core.data import FieldSet


class Processor(Component):

    @abc.abstractmethod
    def process(
            self,
            data: FieldSet
    ):
        pass

    def run(
            self,
            data: FieldSet = None
    ) -> FieldSet:
        data = self.process(data=data) if data is not None else None
        return data


class TextProcessor(Processor):

    def apply_chain_filters(
            self,
            text: str
    ) -> str:
        return reduce(lambda r, f: f(r), self.filters, text)

    def process(
            self,
            data: FieldSet
    ) -> FieldSet:
        text_fields = data.search_by_tag(tags='text')
        for index, field in text_fields.items():
            data[index] = list(map(lambda item: self.apply_chain_filters(text=item), field.value))

        return data


class LabelProcessor(Processor):

    def process_label(
            self,
            label: Union[str, Iterable[str]]
    ):
        if type(label) == str:
            return label
        elif type(label) in [list, set]:
            return list(map(lambda x: x, label))

    def process(
            self,
            data: FieldSet
    ) -> FieldSet:
        label_fields = data.search_by_tag(tags='label')
        if len(label_fields) == 0:
            return data

        assert len(label_fields) == 1, f'Expected a single label tagged field. Got {len(label_fields)}'
        for index, field in label_fields.items():
            data[index] = list(map(lambda l: self.process_label(l), field.value))

        return data


class PipelineProcessor(Processor):

    def process(
            self,
            data: FieldSet
    ):
        for processor in self.processors:
            data = processor.run(data=data)
        return data
