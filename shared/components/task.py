from typing import cast

from core.component import Component
from core.data import FieldSet
from utility.logging_utility import Logger


class Task(Component):

    def run(
            self,
    ) -> FieldSet:
        Logger.get_logger(self.__class__.__name__).info(self.parameter_set)
        return FieldSet()


class DataTask(Task):

    def run(
            self,
    ) -> FieldSet:
        return_field = super().run()

        # DataManager
        data = self.data_manager.run()
        data = cast(FieldSet, data)

        # Processor
        data = self.processor.run(data=data)
        data = cast(FieldSet, data)

        return data
