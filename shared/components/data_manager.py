import abc
import os
from abc import ABCMeta
from itertools import zip_longest
from typing import List, Dict, Optional, Iterable

import numpy as np
import pandas as pd
from tqdm import tqdm

from doc_api.ams_api.StairwAIDriver import Driver
from core.component import Component
from core.data import FieldSet
from core.registry import Registry
from shared.components.file_manager import FileManager
from utility.json_utility import load_json
from utility.logging_utility import Logger
from utility.pickle_utility import save_pickle, load_pickle


class DataManager(Component):

    def __init__(
            self,
            **kwargs
    ):
        super().__init__(**kwargs)
        self.data = None

    def should_build(
            self
    ) -> bool:
        return self.data is None

    @abc.abstractmethod
    def build(
            self,
    ) -> FieldSet:
        pass

    @abc.abstractmethod
    def load(
            self
    ) -> FieldSet:
        pass

    @abc.abstractmethod
    def update(
            self,
            data: FieldSet
    ):
        self.data = data

    def run(self,
            data: Optional[FieldSet] = None
            ) -> FieldSet:
        if data is None and self.should_build():
            data = self.build()
        else:
            if data is not None:
                self.update(data=data)
            else:
                data = self.load()
                self.update(data=data)
        return data


class FileDataManager(DataManager, metaclass=ABCMeta):

    def __init__(
            self,
            **kwargs
    ):
        super(FileDataManager, self).__init__(**kwargs)

        # Update directory paths
        file_manager = Registry.retrieve_built_component_from_key(self.file_manager_regr_key)
        assert isinstance(file_manager, FileManager)

        folder = file_manager.run(filepath=file_manager.data_directory)
        self.filepath = os.path.join(folder, self.folder, self.filename)
        if self.overwrite:
            update_filepath = self.filepath
        else:
            update_filepath = os.path.join(folder, self.folder, self.update_filename)
        self.update_filepath = update_filepath

    def update(
            self,
            data: FieldSet
    ):
        super().update(data=data)
        save_pickle(filepath=self.update_filepath, data=data)

    def load(
            self
    ) -> FieldSet:
        return load_pickle(filepath=self.update_filepath)


class FileLabelManager(FileDataManager):

    def build(
            self,
    ) -> FieldSet:
        data = load_json(self.filepath)["labels"]
        data = pd.DataFrame(data)

        Logger.get_logger(self.__class__.__name__).info(f'Loading labels from file...')

        resource_limit = self.resource_limit if self.resource_limit > 0 else data.shape[0]

        # TODO: check if there are some labels to reject
        return_field = FieldSet()
        return_field.add_short(name='identifier',
                               value=data.index.values[:resource_limit],
                               typing=Iterable[int],
                               description='Label unique indentifier',
                               tags={'ID'})
        return_field.add_short(name='name',
                               value=data['name'].values[:resource_limit],
                               typing=Iterable[str],
                               description='Label name',
                               tags={'label'})
        return_field.add_short(name='description',
                               value=data['description'].values[:resource_limit],
                               typing=Iterable[str],
                               tags={'text'})

        return return_field


class FileExpertsManager(FileDataManager):

    def build(
            self,
    ) -> FieldSet:
        data = pd.read_csv(self.filepath)

        Logger.get_logger(self.__class__.__name__).info(f'Loading experts from file...')

        resource_limit = self.resource_limit if self.resource_limit > 0 else data.shape[0]

        # TODO: check if there are malformed expert's profiles
        return_field = FieldSet()
        return_field.add_short(name='identifier',
                               value=data.index.values[:resource_limit],
                               typing=Iterable[int],
                               description='Unique identifier',
                               tags={'ID'})
        return_field.add_short(name='name',
                               value=data['Name'].values[:resource_limit],
                               typing=Iterable[str],
                               description="Expert's name",
                               tags={'metadata'})
        return_field.add_short(name='description',
                               value=data['Description'].values[:resource_limit],
                               typing=Iterable[str],
                               description="Expert's profile description",
                               tags={'text'})
        languages = [[l.strip().strip("'") for l in item.strip('[]').split(',')] if not pd.isnull(item) else None
                     for item in data['Languages'].values[:resource_limit]] if 'Languages' in data else None
        return_field.add_short(name='languages',
                               value=languages,
                               typing=Optional[Iterable[List[str]]],
                               description='Languages spoken by the expert',
                               tags={'metadata'})
        team_size = [int(item) if not pd.isnull(item) else None
                for item in data['Team size'].values[:resource_limit]] if 'Team size' in data else None
        return_field.add_short(name='team_size',
                               value=team_size,
                               typing=Optional[Iterable[int]],
                               description="Team size of the expert's company",
                               tags={'metadata'})
        price = [float(item) if not pd.isnull(item) else None
            for item in data['Price'].values[:resource_limit]] if 'Price' in data else None
        return_field.add_short(name='price',
                               value=price,
                               typing=Optional[Iterable[float]],
                               description='Price of the expert',
                               tags={'metadata'})
        country = [item if not pd.isnull(item) else None
                   for item in data['Country'].values[:resource_limit]] if 'Country' in data else None
        return_field.add_short(name='country',
                               value=country,
                               typing=Optional[Iterable[str]],
                               description='Country of origin of the expert',
                               tags={'metadata'})

        return return_field


class FileQueryManager(FileDataManager):

    def build(
            self,
    ) -> FieldSet:
        data = pd.read_excel(self.filepath)

        Logger.get_logger(self.__class__.__name__).info(f'Loading queries from file...')

        resource_limit = self.resource_limit if self.resource_limit > 0 else data.shape[0]

        # TODO: check if there are malformed queries
        # Double the amount of samples by considering 'AI label 1' and 'AI label 2'
        return_field = FieldSet()
        return_field.add_short(name='identifier',
                               value=np.arange(data.shape[0] * 2)[:resource_limit],
                               typing=Iterable[int],
                               description='Query unique identifier',
                               tags={'ID'})
        return_field.add_short(name='description',
                               value=np.concatenate((data['project.needs'].values,
                                                     data['project.needs'].values))[:resource_limit],
                               typing=Iterable[str],
                               description='Query textual content',
                               tags={'text'})

        labels = list(map(lambda l: l.split(','), np.concatenate((data['AI label 1'].values,
                                                                  data['AI label 2'].values))[:resource_limit]))
        return_field.add_short(name='labels',
                               value=labels,
                               typing=Iterable[List[str]],
                               description='Manually annotated labels',
                               tags={'label'})

        return return_field


class FileAssetManager(FileDataManager):

    def build(
            self,
    ) -> FieldSet:
        data = pd.read_csv(self.filepath)

        Logger.get_logger(self.__class__.__name__).info(f'Loading assets from file...')

        resource_limit = self.resource_limit if self.resource_limit > 0 else data.shape[0]

        # TODO: check if there are malformed assets
        return_field = FieldSet()
        return_field.add_short(name='identifier',
                               value=data.index.values[:resource_limit],
                               typing=Iterable[int],
                               description='Unique identifier',
                               tags={'ID'})
        return_field.add_short(name='name',
                               value=data['Name'].values[:resource_limit],
                               typing=Iterable[str],
                               description='Label name',
                               tags={"metadata"})
        return_field.add_short(name='description',
                               value=data['Description'].values[:resource_limit],
                               typing=Iterable[str],
                               description="Asset description",
                               tags={'text'})
        labels = list(map(lambda x: [col for col in data.columns if x[1][col] == 1], data.iterrows()))
        return_field.add_short(name='labels',
                               value=labels,
                               typing=Iterable[List[str]],
                               description="Manually annotated labels",
                               tags={'label'})

        return return_field


class FilePaperManager(FileDataManager):

    def build(
            self,
    ) -> FieldSet:
        data = pd.read_csv(self.filepath)

        Logger.get_logger(self.__class__.__name__).info(f'Loading paper from file...')

        resource_limit = self.resource_limit if self.resource_limit > 0 else data.shape[0]

        # TODO: check if there are malformed assets
        return_field = FieldSet()
        return_field.add_short(name='identifier',
                               value=data.index.values[:resource_limit],
                               typing=Iterable[int],
                               description='Unique identifier',
                               tags={'ID'})
        return_field.add_short(name='title',
                               value=data['Title'].values[:resource_limit],
                               typing=Iterable[str],
                               description="The paper's title",
                               tags={"metadata"})
        return_field.add_short(name='abstract',
                               value=data['Abstract'].values[:resource_limit],
                               typing=Iterable[str],
                               description="The paper's abstract.",
                               tags={'text'})
        labels = list(map(lambda x: [col for col in data.columns if x[1][col] == 1], data.iterrows()))
        return_field.add_short(name='labels',
                               value=labels,
                               typing=Iterable[List[str]],
                               description="Manually annotated labels",
                               tags={'label'})

        return return_field


class FileEduResourceManager(FileDataManager):

    def build(
            self,
    ) -> FieldSet:
        data = pd.read_csv(self.filepath)

        Logger.get_logger(self.__class__.__name__).info(f'Loading educational resources from file...')

        resource_limit = self.resource_limit if self.resource_limit > 0 else data.shape[0]

        # TODO: check if there are malformed assets
        return_field = FieldSet()
        return_field.add_short(name='identifier',
                               value=data.index.values[:resource_limit],
                               typing=Iterable[int],
                               description='Unique identifier',
                               tags={'ID'})
        return_field.add_short(name='title',
                               value=data['name'].values[:resource_limit],
                               typing=Iterable[str],
                               description="The edu resource title.",
                               tags={"metadata"})
        return_field.add_short(name='description',
                               value=data['description'].values[:resource_limit],
                               typing=Iterable[str],
                               description="The general description of the edu resource.",
                               tags={'text'})
        return_field.add_short(name='keyword',
                               value=data['Keyword'].values[:resource_limit] if 'Keyword' in data else [None] *
                                                                                                       data.shape[0],
                               typing=List[str],
                               description="The edu resource keywords.",
                               tags={'metadata'})
        return_field.add_short(name='aiod_url',
                               value=data['AIOD_URL'].values[:resource_limit] if 'AIOD_URL' in data else [None] *
                                                                                                         data.shape[0],
                               typing=List[str],
                               description="The edu resource AIOD URL.",
                               tags={'metadata'})
        labels = list(map(lambda x: [col for col in data.columns if x[1][col] == 1], data.iterrows()))
        return_field.add_short(name='labels',
                               value=labels,
                               typing=Iterable[List[str]],
                               description="Manually annotated labels",
                               tags={'label'})

        return return_field


class AMSDataManager(DataManager, metaclass=ABCMeta):

    def __init__(
            self,
            **kwargs
    ):
        super().__init__(**kwargs)
        logger = Logger.get_logger(self.__class__.__name__)

        logger.info('Initializing driver...')
        self.driver = Driver()
        logger.info('Driver initialized.')

    def load(
            self,
    ) -> FieldSet:
        # We re-read from DB
        return self.build()


class AMSLabelManager(AMSDataManager):

    def update(
            self,
            data: FieldSet
    ):
        super().update(data=data)
        # TODO: update DB

    def build(
            self,
    ) -> FieldSet:
        data = self.driver.get_aitechniques(with_description=self.with_details)

        # TODO This filter is sort of patch; it would be better to "tag" in the DB
        # the AI techniques that are actually used for matchmaking
        data = [d for d in data if d['desc'] is not None and d['id'] is not None]

        Logger.get_logger(self.__class__.__name__).info(f'Loading labels from AMS...')

        return_field = FieldSet()
        return_field.add_short(name='identifier',
                               value=list(map(lambda x: x['id'], data)),
                               typing=Iterable[int],
                               description='Unique identifier',
                               tags={'ID'})
        return_field.add_short(name='name',
                               value=list(map(lambda x: x['label'], data)),
                               typing=Iterable[str],
                               description='Label name',
                               tags={'label'})
        return_field.add_short(name='description',
                               value=list(map(lambda x: x['desc'], data)),
                               typing=Iterable[str],
                               description="Short label description",
                               tags={'text'})
        # return_field.add_short(name='identifier',
        #                        value=list(map(lambda x: x[1][0], data.items())),
        #                        typing=Iterable[int],
        #                        description='Unique identifier',
        #                        tags={'ID'})
        # return_field.add_short(name='name',
        #                        value=list(data.keys()),
        #                        typing=Iterable[str],
        #                        description='Label name',
        #                        tags={'label'})
        # return_field.add_short(name='description',
        #                        value=list(map(lambda x: x[1][1], data.items())),
        #                        typing=Iterable[str],
        #                        description="Short label description",
        #                        tags={'text'})
        return return_field


class AMSExpertsManager(AMSDataManager):

    def update(
            self,
            data: FieldSet
    ):
        logger = Logger.get_logger(self.__class__.__name__)
        logger.info('Updating experts...')

        update_info = FieldSet()
        update_info.add_short(name='insertions',
                              value=[],
                              typing=List[str],
                              description="Inserted expert's name")

        valid_labels = self.driver.get_aitechniques()

        for key in data:
            param = data.get_field(key=key)
            data.get_field(key=key).value = param.value if param.value is not None else [None]

        for index, (name,
                    description,
                    scores,
                    country,
                    languages,
                    team_size,
                    price) in tqdm(enumerate(zip_longest(data.name,
                                                         data.description,
                                                         data.labeler_score,
                                                         data.country,
                                                         data.languages,
                                                         data.team_size,
                                                         data.price))):
            if name is None:
                raise RuntimeError(f'Name for an Expert cannot be None. Got name={name}')

            expert_params = {
                'name': name,
            }
            if description is not None:
                expert_params['short description'] = description
            if scores is not None:
                expert_params['scored in'] = {key: value for key, value in scores.items() if key in valid_labels}
            if languages is not None:
                expert_params['has language'] = languages
            if team_size is not None:
                expert_params['team size'] = team_size
            if price is not None:
                expert_params['price'] = price
            if country is not None:
                expert_params['resides'] = country

            try:
                node = self.driver.query_node_by_name_class(name=name, ont_class='AI Expert')
            except Exception:
                node = None

            if node is not None:
                logger.info(f'Expert {name} already exists... Checking...')

                if 'swai__scored_in' in node and node['swai__scored_in'] is not None:
                    logger.warning(f'Expert {name} has already been indexed! Skipping...')
                    continue

            try:
                node = self.driver.insert_instance(class_type='AI expert',
                                                   param_dict=expert_params)
            except ValueError as e:
                logger.warning(f'Could not insert instance {expert_params}.'
                               f'Reason: {e}')
                continue

            node_id = node['swai__identifier']

            data.identifier[index] = node_id
            update_info.insertions.append(name)

        update_info.add_short(name="#insertions",
                              value=len(update_info.insertions),
                              typing=int,
                              description='Number of insertions')
        logger.info(update_info)

        return data

    def build(
            self,
    ) -> FieldSet:
        data = self.driver.get_aiexperts()

        Logger.get_logger(self.__class__.__name__).info(f'Loading experts from AMS...')

        return_data = {}
        for resource in tqdm(data):
            return_data.setdefault('identifier', []).append(resource['identifier'])
            if 'name' in resource:
                return_data.setdefault('name', []).append(resource['name'])
            else:
                return_data.setdefault('name', []).append('')
            if 'short description' in resource:
                return_data.setdefault('description', []).append(resource['short description'])
            else:
                return_data.setdefault('description', []).append('')
            if 'has language' in resource:
                return_data.setdefault('languages', []).append(resource['has language'])
            else:
                return_data.setdefault('languages', []).append(None)
            if 'team size' in resource:
                return_data.setdefault('team_size', []).append(resource['team size'])
            else:
                return_data.setdefault('team_size', []).append(None)
            if 'price' in resource:
                return_data.setdefault('price', []).append(resource['price'])
            else:
                return_data.setdefault('price', []).append(None)
            if 'resides' in resource:
                return_data.setdefault('country', []).append(resource['resides'])
            else:
                return_data.setdefault('country', []).append(None)
            if 'scored in' in resource:
                return_data.setdefault('scores', []).append(resource['scored in'])
            else:
                return_data.setdefault('scores', []).append(None)

        return_field = FieldSet()
        return_field.add_short(name='identifier',
                               value=return_data['identifier'],
                               typing=List[int],
                               description='Unique identifier',
                               tags={'ID'})
        return_field.add_short(name='name',
                               value=return_data['name'],
                               typing=List[str],
                               description="Expert's name",
                               tags={'metadata'})
        return_field.add_short(name='description',
                               value=return_data['description'],
                               typing=List[str],
                               description="Short label description",
                               tags={'text'})
        return_field.add_short(name='languages',
                               value=return_data['languages'],
                               typing=Optional[Iterable[List[str]]],
                               description='Languages spoken by the expert',
                               tags={'metadata'})
        return_field.add_short(name='team_size',
                               value=return_data['team_size'],
                               typing=Optional[Iterable[int]],
                               description="Team size of the expert's company",
                               tags={'metadata'})
        return_field.add_short(name='price',
                               value=return_data['price'],
                               typing=Optional[Iterable[float]],
                               description='Price of the expert',
                               tags={'metadata'})
        return_field.add_short(name='country',
                               value=return_data['country'],
                               typing=Optional[Iterable[str]],
                               description='Country of origin of the expert',
                               tags={'metadata'})
        return_field.add_short(name='labeler_score',
                               value=return_data['scores'],
                               typing=List[Dict[str, float]],
                               description='Horizontal labeler score')
        return return_field


class AMSEduManager(AMSDataManager):

    def update(
            self,
            data: FieldSet
    ):
        logger = Logger.get_logger(self.__class__.__name__)
        logger.info('Updating educational resources...')

        update_info = FieldSet()
        update_info.add_short(name='insertions',
                              value=[],
                              typing=List[str],
                              description="Inserted edu's title")

        valid_labels = self.driver.get_aitechniques()

        for key in data:
            param = data.get_field(key=key)
            data.get_field(key=key).value = param.value if param.value is not None else [None]

        for index, (title, description, scores, keyword, aiod_url) in tqdm(enumerate(zip_longest(
                data.title,
                data.description,
                data.labeler_score,
                data.keyword,
                data.aiod_url))):
            if title is None:
                raise RuntimeError(f'Title for an Educational Resource cannot be None. Got title={title}')

            expert_params = {
                'name': title,
            }
            if description is not None:
                expert_params['short description'] = description
            if scores is not None:
                expert_params['scored in'] = {key: value for key, value in scores.items() if key in valid_labels}
            if keyword is not None:
                expert_params['keyword'] = keyword
            if aiod_url is not None:
                expert_params['aiod url'] = aiod_url

            try:
                node = self.driver.query_node_by_name_class(name=title,
                                                            ont_class='Educational Resource')
            except Exception:
                node = None

            if node is not None:
                logger.info(f'Educational Resource {title} already exists... Checking...')

                if 'swai__scored_in' in node and node['swai__scored_in'] is not None:
                    logger.warning(f'Educational Resource {title} has already been indexed! Skipping...')
                    continue

            try:
                node = self.driver.insert_instance(class_type='Educational Resource',
                                                   param_dict=expert_params)
            except ValueError as e:
                logger.warning(f'Could not insert instance {expert_params}.'
                               f'Reason: {e}')
                continue

            node_id = node['swai__identifier']

            data.identifier[index] = node_id
            update_info.insertions.append(title)

        update_info.add_short(name="#insertions",
                              value=len(update_info.insertions),
                              typing=int,
                              description='Number of insertions')
        logger.info(update_info)

        return data

    def build(
            self,
    ) -> FieldSet:
        data = self.driver.get_educational_resources()

        Logger.get_logger(self.__class__.__name__).info(f'Loading educational resources from AMS...')

        return_data = {}
        for resource in tqdm(data):
            return_data.setdefault('identifier', []).append(resource['identifier'])
            return_data.setdefault('title', []).append(resource['name'])
            return_data.setdefault('description', []).append(resource['short description'])

            if 'keyword' in resource:
                return_data.setdefault('keyword', []).append(resource['keyword'])
            else:
                return_data.setdefault('keyword', []).append(None)

            if 'aiod url' in resource:
                return_data.setdefault('aiod url', []).append(resource['aiod url'])
            else:
                return_data.setdefault('aiod url', []).append(None)

            if 'scored in' in resource:
                return_data.setdefault('scores', []).append(resource['scored in'])
            else:
                return_data.setdefault('scores', []).append(None)

        return_field = FieldSet()
        return_field.add_short(name='identifier',
                               value=return_data['identifier'],
                               typing=List[int],
                               description='Unique identifier',
                               tags={'ID'})
        return_field.add_short(name='title',
                               value=return_data['title'],
                               typing=List[str],
                               description="The edu resource title.",
                               tags={'metadata'})
        return_field.add_short(name='description',
                               value=return_data['description'],
                               typing=List[str],
                               description="The general description of the edu resource.",
                               tags={'text'})
        return_field.add_short(name='keyword',
                               value=return_data['keyword'],
                               typing=List[str],
                               description="The edu resource keywords.",
                               tags={'metadata'})
        return_field.add_short(name='aiod url',
                               value=return_data['aiod url'],
                               typing=List[str],
                               description="The edu resource AIOD URL.",
                               tags={'metadata'})
        return_field.add_short(name='labeler score',
                               value=return_data['scores'],
                               typing=List[Dict[str, float]],
                               description='Horizontal labeler score')
        return return_field


class AMSPaperManager(AMSDataManager):

    def update(
            self,
            data: FieldSet
    ):
        logger = Logger.get_logger(self.__class__.__name__)
        logger.info('Updating Academic Resources...')

        update_info = FieldSet()
        update_info.add_short(name='insertions',
                              value=[],
                              typing=List[str],
                              description="Inserted Academic Resource's title")

        valid_labels = self.driver.get_aitechniques()

        for key in data:
            param = data.get_field(key=key)
            data.get_field(key=key).value = param.value if param.value is not None else [None]

        # for index, (title, description, scores, keyword, aiod_url) in tqdm(enumerate(zip_longest(
        for index, (title, description, scores) in tqdm(enumerate(zip_longest(
                data.title,
                # data.description,
                data.abstract,
                data.labeler_score,
                # data.keyword,
                # data.aiod_url
                ))):
            if title is None:
                raise RuntimeError(f'Title for an Academic Resource cannot be None. Got title={title}')

            expert_params = {
                'name': title,
            }
            if description is not None:
                expert_params['short description'] = description
            if scores is not None:
                expert_params['scored in'] = {key: value for key, value in scores.items() if key in valid_labels}
            # if keyword is not None:
            #     expert_params['keyword'] = keyword
            # if aiod_url is not None:
            #     expert_params['aiod url'] = aiod_url

            try:
                node = self.driver.query_node_by_name_class(name=title,
                                                            ont_class='Academic Resource')
            except Exception:
                node = None

            if node is not None:
                logger.info(f'Academic Resource {title} already exists... Checking...')

                if 'swai__scored_in' in node and node['swai__scored_in'] is not None:
                    logger.warning(f'Academic Resource {title} has already been indexed! Skipping...')
                    continue

            try:
                node = self.driver.insert_instance(class_type='Academic Resource',
                                                   param_dict=expert_params)
            except ValueError as e:
                logger.warning(f'Could not insert instance {expert_params}.'
                               f'Reason: {e}')
                continue

            node_id = node['swai__identifier']

            data.identifier[index] = node_id
            update_info.insertions.append(title)

        update_info.add_short(name="#insertions",
                              value=len(update_info.insertions),
                              typing=int,
                              description='Number of insertions')
        logger.info(update_info)

        return data

    def build(
            self,
    ) -> FieldSet:
        data = self.driver.get_academic_resources() # TODO this must be replaced

        Logger.get_logger(self.__class__.__name__).info(f'Loading academic resources from AMS...')

        return_data = {}
        for resource in tqdm(data):
            return_data.setdefault('identifier', []).append(resource['identifier'])
            return_data.setdefault('title', []).append(resource['name'])
            return_data.setdefault('description', []).append(resource['short description'])

            if 'keyword' in resource:
                return_data.setdefault('keyword', []).append(resource['keyword'])
            else:
                return_data.setdefault('keyword', []).append(None)

            if 'aiod_url' in resource:
                return_data.setdefault('aiod_url', []).append(resource['aiod_url'])
            else:
                return_data.setdefault('aiod_url', []).append(None)

            if 'scored in' in resource:
                return_data.setdefault('scores', []).append(resource['scored in'])
            else:
                return_data.setdefault('scores', []).append(None)

        return_field = FieldSet()
        return_field.add_short(name='identifier',
                               value=return_data['identifier'],
                               typing=List[int],
                               description='Unique identifier',
                               tags={'ID'})
        return_field.add_short(name='title',
                               value=return_data['title'],
                               typing=List[str],
                               description="The paper's title.",
                               tags={'metadata'})
        return_field.add_short(name='abstract',
                               value=return_data['description'],
                               typing=List[str],
                               description="The paper's abstract.",
                               tags={'text'})
        return_field.add_short(name='labeler_score',
                               value=return_data['scores'],
                               typing=List[Dict[str, float]],
                               description='Horizontal labeler score')
        return return_field
