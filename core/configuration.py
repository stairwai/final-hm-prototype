"""

TODO: add documentation

"""

from abc import ABC
from copy import deepcopy
from typing import Dict, Any

from core.data import ParameterSet


class Configuration(ABC):
    """
    Base configuration class.
    Configurations might instantiate a component or wrap information for a specific part of a component.
    Configurations are meant to store fields and handle field related verification processes.
    """

    def __init__(
            self,
            parameter_set: ParameterSet
    ):
        """
        Instantiates a new Configuration object.

        Args:
        """
        self.parameter_set = parameter_set

    def __deepcopy__(
            self,
            memodict={}
    ):
        copy_parameter_set = self.parameter_set.__deepcopy__()
        return self.__class__(parameter_set=copy_parameter_set)

    def get_delta_copy(
            self,
            parameter_set: ParameterSet
    ) -> 'Configuration':
        """
        Builds a delta copy of current Configuration instance with given input kwargs.

        Args:

        Returns:
            config_copy (Configuration): a copy of current Configuration instance with given override attributes.
        """
        config_copy = deepcopy(self)
        config_copy.parameter_set.merge(parameter_set)
        config_copy.parameter_set.validate()
        return config_copy

    @classmethod
    def get_delta_copy_by_dict(
            cls: 'Configuration',
            params: Dict[str, Any]
    ) -> 'Configuration':
        config = cls.get_default()
        for key, value in params.items():
            config.parameter_set[key] = value
        return config

    def post_build(
            self
    ):
        self.parameter_set.post_build()

    def validate(
            self
    ):
        self.parameter_set.validate()

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        return ParameterSet()

    @classmethod
    def get_default(
            cls
    ) -> 'Configuration':
        """
        Returns the default Configuration instance.

        Returns:
            Configuration instance.
        """
        parameter_set = cls.get_parameter_set()
        config = cls(parameter_set=parameter_set)
        return config
