"""

TODO: add documentation

"""

import ast
import importlib.util
import os
import sys
from copy import deepcopy
from enum import Enum
from typing import Type, AnyStr, List, Set, Dict, Any, Union, Tuple, Optional

from typeguard import check_type

from core.component import Component
from core.configuration import Configuration
from core.data import HashableIndex
from utility.logging_utility import Logger
from utility.registration_utility import PassFilter, parse_and_create_filter, GenericFilter, SearchableEnumMeta


class Framework(str, Enum, metaclass=SearchableEnumMeta):
    GENERIC = 'generic'
    TENSORFLOW = 'tensorflow'
    TORCH = 'torch'


class RegistrationKey(HashableIndex):

    def __init__(
            self,
            framework: Framework,
            namespace: str = 'default',
            tags: Optional[Set[str]] = None,
            **kwargs
    ):
        super().__init__(**kwargs)
        self.framework = framework
        self.namespace = namespace if namespace is not None else 'default'
        self.tags = tags if tags is not None else set()

    def __hash__(
            self
    ):
        return hash(self.__str__())

    def __str__(
            self
    ):
        to_return = f'name{self.KEY_VALUE_SEPARATOR}{self.name}{self.ATTRIBUTE_SEPARATOR}' \
                    f'framework{self.KEY_VALUE_SEPARATOR}{self.framework}'

        if self.tags is not None:
            to_return += f'{self.ATTRIBUTE_SEPARATOR}tags{self.KEY_VALUE_SEPARATOR}{sorted(list(self.tags))}'

        to_return += f'{self.ATTRIBUTE_SEPARATOR}namespace{self.KEY_VALUE_SEPARATOR}{self.namespace}'
        return to_return

    def __eq__(
            self,
            other
    ):
        default_condition = lambda other: self.name == other.name and \
                                          self.framework == other.framework

        tags_condition = lambda other: (self.tags is not None and other.tags is not None and self.tags == other.tags) \
                                       or (self.tags is None and other.tags is None)

        namespace_condition = lambda other: (self.namespace is not None
                                             and other.namespace is not None
                                             and self.namespace == other.namespace) \
                                            or (self.namespace is None and other.namespace is None)

        return default_condition(other) \
            and tags_condition(other) \
            and namespace_condition(other)

    def partial_match(
            self,
            other
    ):
        base_condition = super().partial_match(other=other)

        default_condition = lambda other: self.framework == other.framework

        tags_condition = lambda other: (self.tags is not None and other.tags is not None
                                        and self.tags.intersection(self.tags) == set(other.tags)) \
                                       or (self.tags is None and other.tags is None)

        namespace_condition = lambda other: (self.namespace is not None
                                             and other.namespace is not None
                                             and self.namespace == other.namespace) \
                                            or (self.namespace is None and other.namespace is None)

        return base_condition and tags_condition(other) and default_condition(other) and namespace_condition(other)

    @classmethod
    def from_string(
            cls,
            string_format: str
    ) -> 'RegistrationKey':
        registration_attributes = string_format.split(cls.ATTRIBUTE_SEPARATOR)
        registration_dict = {}
        for registration_attribute in registration_attributes:
            try:
                key, value = registration_attribute.split(cls.KEY_VALUE_SEPARATOR)
                if key == 'tags':
                    value = set(ast.literal_eval(value))

                registration_dict[key] = value
            except ValueError:
                Logger.get_logger(__name__).exception(
                    f'Failed parsing registration key from string.. Got: {string_format}')

        return RegistrationKey(**registration_dict)


class Registry:
    REGISTRY = {}
    MAPPINGS = {}
    BUILT_MAPPINGS = {}
    MODULES = set()
    _MAP_QUEUE = set()

    @staticmethod
    def _build(
            class_type: Type,
            args: Dict[str, Any] = None
    ):
        args = args if args is not None else {}
        return class_type(**args)

    @staticmethod
    def load_custom_module(
            module_path: AnyStr,
    ):
        module_name = os.path.split(module_path)[-1]
        module_path = os.path.join(module_path, '__init__.py')

        if not os.path.isfile(module_path):
            return

        spec = importlib.util.spec_from_file_location(name=module_name,
                                                      location=module_path)
        module = importlib.util.module_from_spec(spec=spec)
        sys.modules[module_name] = module
        spec.loader.exec_module(module=module)
        if hasattr(module, 'register'):
            module_register = getattr(module, 'register')
            module_register()
        Registry.MODULES.add(module_path)

    # Registration APIs

    @staticmethod
    def _lookup_element_from_key(
            registration_key: RegistrationKey,
            exact_match: bool = True
    ) -> Union[Any, List[Any]]:
        if exact_match:
            assert registration_key in Registry.REGISTRY, \
                f"Expected to find a registered element with key: {registration_key}. Search failed."
            return Registry.REGISTRY[registration_key]
        else:
            return [Registry.REGISTRY[key] for key in Registry.REGISTRY if key.partial_match(registration_key)]

    @staticmethod
    def _lookup_element(
            flag: str,
            framework: Framework,
            tags: Set[str] = None,
            namespace: str = None,
            exact_match: bool = True) -> Union[Any, List[Any]]:
        registration_key = RegistrationKey(name=flag,
                                           framework=framework,
                                           tags=tags,
                                           namespace=namespace)
        return Registry._lookup_element_from_key(registration_key=registration_key,
                                                 exact_match=exact_match)

    # Component

    @staticmethod
    def retrieve_component_from_key(
            config_regr_key: Union[RegistrationKey, str],
            built_register: bool = False
    ) -> Component:
        check_type('config_regr_key', config_regr_key, Union[RegistrationKey, str])

        if type(config_regr_key) == str:
            config_regr_key = RegistrationKey.from_string(string_format=config_regr_key)

        assert config_regr_key in Registry.REGISTRY, f"Could not find registered configuration {config_regr_key}." \
                                                     f" Did you register it?"
        registered_config = Registry.REGISTRY[config_regr_key]

        assert config_regr_key in Registry.MAPPINGS, f'Registered configuration is not mapped to any component.' \
                                                     f' Did you map it?'
        registered_component = Registry.MAPPINGS[config_regr_key]

        # Create a copy to allow overwrites
        built_component = Registry._build(class_type=registered_component,
                                          args={
                                              'config': deepcopy(registered_config)
                                          })

        if built_register:
            Registry.register_built_component_from_key(component=built_component,
                                                       registration_key=config_regr_key)

        return built_component

    @staticmethod
    def retrieve_component(
            name: str,
            framework: Framework = None,
            tags: Set[str] = None,
            namespace: AnyStr = None
    ) -> Component:
        config_regr_key = RegistrationKey(name=name,
                                          framework=framework,
                                          tags=tags,
                                          namespace=namespace)
        return Registry.retrieve_component_from_key(config_regr_key=config_regr_key)

    @staticmethod
    def register_built_component_from_key(
            component: Component,
            registration_key: RegistrationKey
    ):
        assert registration_key in Registry.REGISTRY, f"Could not find registration {registration_key}. " \
                                                      f"Did you register it?"
        Registry.BUILT_MAPPINGS[registration_key] = component

    @staticmethod
    def register_built_component(
            component: Component,
            framework: Framework,
            name: str,
            tags: Set[str] = None,
            namespace: str = None,
    ):
        built_regr_key = RegistrationKey(name=name,
                                         framework=framework,
                                         tags=tags,
                                         namespace=namespace)
        Registry.register_built_component_from_key(component=component,
                                                   registration_key=built_regr_key)

    @staticmethod
    def retrieve_built_component_from_key(
            config_regr_key: Union[RegistrationKey, str]
    ) -> Component:
        if type(config_regr_key) == str:
            config_regr_key = RegistrationKey.from_string(string_format=config_regr_key)

        assert config_regr_key in Registry.REGISTRY, f'Could not find registered configuration: {config_regr_key}'
        return Registry.BUILT_MAPPINGS[config_regr_key]

    @staticmethod
    def retrieve_built_component(
            framework: Framework,
            name: str,
            tags: Set[str] = None,
            namespace: str = None
    ) -> Component:
        config_regr_key = RegistrationKey(framework=framework,
                                          name=name,
                                          tags=tags,
                                          namespace=namespace)
        return Registry.retrieve_built_component_from_key(config_regr_key=config_regr_key)

    # Configuration

    @staticmethod
    def register_configuration(
            configuration: Configuration,
            framework: Framework,
            name: str,
            tags: Set[str] = None,
            namespace: str = None
    ) -> RegistrationKey:
        registration_key = RegistrationKey(name=name,
                                           tags=tags,
                                           framework=framework,
                                           namespace=namespace)

        # Check if already registered
        if registration_key in Registry.REGISTRY:
            raise RuntimeError('A configuration has already been registered with the same key!'
                               f'Got: {registration_key}')

        # Store configuration in registry
        Registry.REGISTRY[registration_key] = configuration

        return registration_key

    @staticmethod
    def retrieve_configurations(
            name: str,
            framework: Framework = None,
            tags: Set[str] = None,
            namespace: str = None,
            exact_match: bool = True
    ) -> Union[Configuration, List[Configuration]]:
        return Registry._lookup_element(
            flag=name,
            framework=framework,
            tags=tags,
            namespace=namespace,
            exact_match=exact_match)

    @staticmethod
    def retrieve_configurations_from_key(
            registration_key: Union[RegistrationKey, str],
            exact_match: bool = True
    ) -> Union[Configuration, List[Configuration]]:
        if type(registration_key) == str:
            registration_key = RegistrationKey.from_string(string_format=registration_key)

        return Registry._lookup_element_from_key(registration_key=registration_key,
                                                 exact_match=exact_match)

    @staticmethod
    def register_and_map(
            configuration: Configuration,
            component_class: Type,
            framework: Framework,
            name: str,
            tags: Set[str] = None,
            namespace: str = None,
            is_default: bool = False
    ) -> RegistrationKey:
        tags = tags.union({'default'}) if tags is not None and is_default else tags
        if tags is None and is_default:
            tags = {'default'}

        config_regr_key = Registry.register_configuration(configuration=configuration,
                                                          framework=framework,
                                                          name=name,
                                                          tags=tags,
                                                          namespace=namespace)

        Registry._MAP_QUEUE.add((config_regr_key, component_class))

        return config_regr_key

    @staticmethod
    def finalize_mappings():
        for (config_regr_key, component_class) in Registry._MAP_QUEUE:
            assert config_regr_key in Registry.REGISTRY, f'Could not find registration {config_regr_key}. Did you register it?'
            Registry.MAPPINGS[config_regr_key] = component_class

        Registry._MAP_QUEUE.clear()

    # TODO: update? this is a general filter
    @staticmethod
    def get_registered_elements(
            flag_or_filters: Union[str, List[GenericFilter]] = None,
            framework_or_filters: Union[Framework, List[GenericFilter]] = None,
            tags_or_filters: Union[Set[str], List[GenericFilter]] = None,
            namespace_or_filters: Union[str, List[GenericFilter]] = None
    ) -> List[Tuple[RegistrationKey, Any]]:
        filters = []

        flag_or_filters = parse_and_create_filter(data=flag_or_filters, attribute_name='flag')
        framework_or_filters = parse_and_create_filter(data=framework_or_filters,
                                                       attribute_name='framework')
        tags_or_filters = parse_and_create_filter(data=tags_or_filters, attribute_name='tags')
        namespace_or_filters = parse_and_create_filter(data=namespace_or_filters,
                                                       attribute_name='namespace')

        filters += flag_or_filters + framework_or_filters + tags_or_filters + namespace_or_filters

        # We accept everything in case of no filter options
        if not len(filters):
            filters.append(PassFilter())

        filtered_keys = list(filter(lambda element: all(map(lambda f: f(element), filters)), Registry.REGISTRY))
        return [(key, Registry.REGISTRY[key]) for key in filtered_keys]

    @staticmethod
    def show():
        from utility.logging_utility import Logger
        logger = Logger.get_logger(__name__)
        logger.info(f'''
        {os.linesep}
        {"*" * 50}
        {os.linesep}
        {"*" * 50}
        {os.linesep}
        Modules: {os.linesep}{Registry.MODULES}{os.linesep}
        {os.linesep}
        {"*" * 50}
        {os.linesep}
        Registry: {os.linesep}{Registry.REGISTRY}
        {os.linesep}
        {"*" * 50}
        {os.linesep}
        ''')
