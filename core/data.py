import ast
import os
from copy import deepcopy
from typing import Any, List, Optional, Callable, Dict, Type, Set, Union, Iterable, Tuple, Hashable

from dotmap import DotMap
from typeguard import check_type

from utility.logging_utility import Logger


class HashableIndex:
    ATTRIBUTE_SEPARATOR = '--'
    KEY_VALUE_SEPARATOR = ':'

    def __init__(
            self,
            name: Hashable,
    ):
        self.name = name

    def __eq__(
            self,
            other
    ):
        if type(other) == str:
            other = HashableIndex(other)

        return self.name == other.name

    def __hash__(
            self
    ):
        return hash(self.__str__())

    def __repr__(
            self
    ) -> str:
        return self.__str__()

    def __str__(
            self
    ) -> str:
        return f'name{self.KEY_VALUE_SEPARATOR}{self.name}'

    def __lt__(
            self,
            other
    ):
        return self.__str__() < other.__str__()

    def partial_match(
            self,
            other: Union[str, 'HashableIndex']
    ) -> bool:
        if type(other) == str:
            other = type(self)(other)

        name_condition = lambda other: (self.name is None and other.name is None) or self.name == other.name
        return name_condition(other)

    @classmethod
    def from_string(
            cls,
            string_format: str
    ) -> 'HashableIndex':
        attributes = string_format.split(cls.ATTRIBUTE_SEPARATOR)
        attribute_dict = {}
        for attribute in attributes:
            try:
                key, value = attribute.split(cls.KEY_VALUE_SEPARATOR)
                if key == 'tags':
                    value = ast.literal_eval(value)
                attribute_dict[key] = value
            except ValueError:
                Logger.get_logger(__name__).exception(
                    f'Failed parsing registration info from string.. Got: {string_format}')

        return HashableIndex(**attribute_dict)


class Field:

    def __init__(
            self,
            name: Hashable,
            value: Any = None,
            typing: Type = None,
            description: str = None,
            tags: Set[str] = None,
    ):
        self.name = name
        self.value = value
        self.typing = typing
        self.description = description
        self.tags = tags if tags is not None else set()

    def short_repr(
            self
    ) -> str:
        return f'{self.value}'

    def long_repr(
            self
    ) -> str:
        return f'name: {self.name} --{os.linesep}' \
               f'value: {self.value} --{os.linesep}' \
               f'typing: {self.typing if self.typing is not None else "not-specified"} --{os.linesep}' \
               f'description: {self.description} --{os.linesep}' \
               f'tags: {self.tags if self.tags is not None else "not-specified"}'

    def __str__(
            self
    ) -> str:
        return self.short_repr()

    def __repr__(
            self
    ) -> str:
        return self.short_repr()

    def __hash__(
            self
    ) -> int:
        return hash(str(self))

    def __eq__(
            self,
            other: 'Field'
    ) -> bool:
        name_condition = lambda other: self.name == other.name
        value_condition = lambda other: self.value == other.value
        return name_condition(other) and value_condition(other)


class FieldSet(DotMap):

    def __setitem__(
            self,
            key: Union[Hashable, HashableIndex],
            item: Union[Field, Any]
    ):
        if not isinstance(key, HashableIndex):
            key = HashableIndex(name=key)

        if isinstance(item, Field):
            super().__setitem__(k=key, v=item)
        else:
            assert key in self, f'Cannot find or update a non-existing field! Key = {key}'
            self.get_field(key).value = item

    def __getitem__(
            self,
            item: Union[Hashable, HashableIndex, Tuple[Union[Hashable, HashableIndex], bool]]
    ) -> Field:
        return_value = True
        if type(item) == tuple:
            item, return_value = item

        if len(self.search_by_name(name=item)):
            if type(item) != HashableIndex:
                item = HashableIndex(item)
        else:
            return super().__getitem__(item)

        return super().__getitem__(item).value if return_value else super().__getitem__(item)

    def get_field(
            self,
            key: Union[Hashable, HashableIndex],
            default_value: Optional[Any] = None
    ) -> Union[Field, Optional[Any]]:
        return self[key, False] if key in self else default_value

    def __contains__(
            self,
            item: Union[Hashable, HashableIndex]
    ) -> bool:
        if not isinstance(item, HashableIndex):
            item = HashableIndex(name=item)
        return super().__contains__(k=item)

    def __deepcopy__(
            self,
            memodict: Dict = {}
    ):
        copy = type(self)()
        for index in self.keys():
            copy.add(deepcopy(self.get_field(index)))
        return copy

    def add(
            self,
            field: Field
    ):
        field_index = HashableIndex(name=field.name)
        self[field_index] = field

    def add_short(
            self,
            name: Hashable,
            value: Any = None,
            typing: Type = None,
            description: str = None,
            tags: Set[str] = None
    ):
        field = Field(name=name,
                      value=value,
                      typing=typing,
                      description=description,
                      tags=tags)
        self.add(field=field)

    def update_fields(
            self,
            fields: List[Field]
    ):
        for field in fields:
            self.add(field=field)

    def search_by_tag(
            self,
            tags: Optional[Union[Set[str], str]] = None
    ) -> 'FieldSet':
        if not type(tags) == set:
            tags = {tags}
        return type(self)({key: value for key, value in self.items()
                           if value.tags == tags or tags is None})

    def search_by_name(
            self,
            name: Optional[Hashable] = None
    ) -> 'FieldSet':
        return type(self)({key: value for key, value in self.items() if key.name == name or name is None})

    def search(
            self,
            search_key: Optional[Union[Hashable, HashableIndex]] = None,
            exact_match: bool = True
    ) -> 'FieldSet':
        if type(search_key) == str:
            search_key = HashableIndex(name=search_key)

        if exact_match:
            return type(self)({key: value for key, value in self.items() if key == search_key})
        else:
            return type(self)({key: value for key, value in self.items() if key.partial_match(search_key)})


class Parameter:

    def __init__(
            self,
            name: str,
            value: Any = None,
            typing: Type = None,
            description: str = None,
            tags: Set[str] = None,
            allowed_range: Callable[[Any], bool] = None,
            affects_serialization: bool = False,
            is_required: bool = False,
            is_registration: bool = False,
            build_from_registration: bool = True,
            variations: Optional[Iterable] = None,
            **kwargs
    ):
        self.name = name
        self.value = value
        self.typing = typing
        self.description = description
        self.tags = tags if tags is not None else set()
        self.allowed_range = allowed_range
        self.affects_serialization = affects_serialization
        self.is_required = is_required
        self.is_registration = is_registration
        self.build_from_registration = build_from_registration
        self.variations = variations

    def check_value(
            self
    ):
        if self.allowed_range is not None:
            assert self.allowed_range(self.value), f'Parameter value {self.value} not in allowed range'

    def short_repr(
            self
    ) -> str:
        return f'{self.value}'

    def long_repr(
            self
    ) -> str:
        return f'name: {self.name} --{os.linesep}' \
               f'value: {self.value} --{os.linesep}' \
               f'affects_serialization: {self.affects_serialization} --{os.linesep}' \
               f'typing: {self.typing if self.typing is not None else "not-specified"} --{os.linesep}' \
               f'description: {self.description} --{os.linesep}' \
               f'is_required: {self.is_required} --{os.linesep}' \
               f'variations: {self.variations} --{os.linesep}'

    def __str__(
            self
    ) -> str:
        return self.short_repr()

    def __repr__(
            self
    ) -> str:
        return self.short_repr()

    def __hash__(
            self
    ) -> int:
        return hash(str(self))

    def __eq__(
            self,
            other: 'Parameter'
    ) -> bool:
        name_condition = lambda other: self.name == other.name
        value_condition = lambda other: self.value == other.value
        return name_condition(other) and value_condition(other)


# TODO: consider adding a Resolutor: -> condition == False? throw error or resolve
# useful for handling invalid parameter values when you don't know their boundaries
# TODO: distinguish between pre- and post- build conditions
class ParameterCondition:

    def __init__(
            self,
            name: str,
            condition: Callable[[Any], bool],
    ):
        self.name = name
        self.condition = condition

    def validate(
            self,
            parameters: 'ParameterSet'
    ):
        return self.condition(parameters)


class ParameterSet(DotMap):

    def __init__(
            self,
            **kwargs
    ):
        super().__init__(**kwargs)
        self.conditions = dict()

    def __setitem__(
            self,
            key: Union[str, HashableIndex],
            item: Union[Any, Parameter]
    ):
        if key in ['conditions']:
            return super().__setitem__(key, item)

        if not isinstance(key, HashableIndex):
            key = HashableIndex(name=key)

        if isinstance(item, Parameter):
            super().__setitem__(key, item)
        else:
            assert key in self, f'Cannot update the value of a non-existing parameter! Key = {key}'
            self.get_param(key).value = item
        self.get_param(key).check_value()

    def __getitem__(
            self,
            item: Union[str, HashableIndex, Tuple[Union[str, HashableIndex], bool]]
    ) -> Parameter:
        return_value = True
        if type(item) == tuple:
            item, return_value = item

        if type(item) == str:
            if item in ['conditions']:
                return super().__getitem__(item)

            item = HashableIndex(item)
        return super().__getitem__(item).value if return_value else super().__getitem__(item)

    def get_param(
            self,
            key: Union[str, HashableIndex]
    ) -> Parameter:
        return self[key, False]

    def __contains__(
            self,
            item: Union[str, HashableIndex]
    ) -> bool:
        if not isinstance(item, HashableIndex):
            item = HashableIndex(name=item)
        return super().__contains__(item)

    def __deepcopy__(
            self,
            memodict: Dict = {}
    ):
        copy = type(self)()
        for index in self.keys():
            if type(index) == HashableIndex:
                copy.add(deepcopy(self.get_param(index)))
        copy.conditions = self.conditions
        return copy

    def add(
            self,
            param: Parameter
    ):
        param_index = HashableIndex(name=param.name)
        self[param_index] = param

        if param.is_required:
            self.add_condition(ParameterCondition(name=f'{param.name}_is_required',
                                                  condition=lambda p: p[param.name] is not None))

        def typing_condition(
                parameters: ParameterSet
        ) -> bool:
            try:
                found = parameters.get_param(key=param.name)
                check_type(argname=found.name,
                           value=found.value,
                           expected_type=found.typing)
            except TypeError:
                return False
            return True

        # add condition concerning typing
        if not param.is_registration and param.build_from_registration and param.typing is not None:
            self.add_condition(ParameterCondition(name=f'{param.name}_typecheck',
                                                  condition=lambda p: typing_condition(p)))

    def add_short(
            self,
            name: str,
            value: Any = None,
            allowed_range: Callable[[Any], bool] = None,
            affects_serialization: bool = False,
            typing: Type = None,
            description: str = None,
            is_required: bool = False,
            is_registration: bool = False,
            build_from_registration: bool = True,
            variations: Optional[Iterable] = None
    ):
        param = Parameter(name=name,
                          value=value,
                          allowed_range=allowed_range,
                          affects_serialization=affects_serialization,
                          typing=typing,
                          description=description,
                          is_required=is_required,
                          is_registration=is_registration,
                          build_from_registration=build_from_registration,
                          variations=variations)
        return self.add(param=param)

    def update_parameters(
            self,
            parameters: List[Parameter]
    ):
        for param in parameters:
            self.add(param=param)

    def get_serialization_parameters(
            self
    ) -> Dict[str, Parameter]:
        """
        Returns the current Configuration instance fields that can change the data serialization pipeline.

        Returns:
            parameters (dict): {parameter_name: parameter_value} dictionary of current Configuration instance,
            including its children.
        """
        return {key: param for key, param in self.items() if param.affects_serialization}

    def add_condition(
            self,
            condition: ParameterCondition,
    ):
        """
        Adds a condition function to be evaluated concerning an attribute subset.

        Args:
            condition (Callable): a function to be called that evaluates an attribute subset.
        """
        assert condition.name not in self.conditions, \
            f'Cannot overwrite existing condition with the same name={condition.name}'
        self.conditions[condition.name] = condition

    def add_condition_short(
            self,
            name: str,
            condition: Callable[[Any], bool]
    ):
        return self.add_condition(ParameterCondition(name=name, condition=condition))

    def update_condition(
            self,
            condition: ParameterCondition
    ):
        assert condition.name in self.conditions, f"Cannot update a non-existing condition"
        self.conditions[condition.name] = condition

    def update_condition_short(
            self,
            name: str,
            condition: Callable[[Any], bool]
    ):
        return self.update_condition(condition=ParameterCondition(name=name,
                                                                  condition=condition))

    def validate(
            self
    ):
        """
        Calls each stored condition evaluation function with its corresponding set of arguments.
        """
        for condition_name, condition in self.conditions.items():
            assert condition.validate(self), f"Condition {condition_name} failed!"

    def post_build(
            self
    ):
        from core.registry import Registry
        for index in self.keys():
            if type(index) == HashableIndex:
                param = self.get_param(index)
                if param.is_registration and param.build_from_registration:
                    if type(param.value) == list:
                        param.value = [Registry.retrieve_component_from_key(config_regr_key=value)
                                       for value in param.value]
                    else:
                        param.value = Registry.retrieve_component_from_key(config_regr_key=param.value)
