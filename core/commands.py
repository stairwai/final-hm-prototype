"""

TODO: add documentation

"""

import os
from typing import AnyStr, List, Union, cast

from tqdm import tqdm

from shared.components.file_manager import FileManager
from core.registry import RegistrationKey, Framework
from core.registry import Registry
from utility.json_utility import save_json
from utility.logging_utility import Logger
import glob


def stringify_elements(
        elements
):
    elements = list(map(lambda item: str(item), elements))
    elements = sorted(elements)
    return elements


def retrieve_and_save(
        save_folder: AnyStr,
        save_name: str,
        **kwargs
):
    elements = Registry.get_registered_elements(**kwargs)
    elements = [item[0] for item in elements]
    elements = stringify_elements(elements)
    save_json(os.path.join(save_folder, f'{save_name}.json'), elements)


def find_modules(
        root: AnyStr
) -> List[AnyStr]:
    folders = glob.iglob(pathname=root + "/**", recursive=True)
    folders = list(filter(lambda x: os.path.isdir(x) and os.path.split(x)[1] not in ['__pycache__'],
                          folders))
    # Ignore first item (root)
    return folders[1:]


# Commands

def setup_registry(
        directory: AnyStr = None,
        module_directories: List[AnyStr] = None,
        generate_registration: bool = False,
        file_manager_regr_info: Union[RegistrationKey, str] = None
) -> Union[RegistrationKey, str]:
    directory = os.path.normpath(directory)
    module_directories = module_directories if module_directories is not None else [directory]
    for mod_dir in module_directories:
        mod_dir = os.path.normpath(mod_dir)
        for module_name in find_modules(root=mod_dir):
            Registry.load_custom_module(module_path=module_name)

    Registry.finalize_mappings()

    if file_manager_regr_info is None:
        file_manager_regr_info = RegistrationKey(name='file_manager',
                                                 framework=Framework.GENERIC,
                                                 tags={'default'})

    file_manager = Registry.retrieve_component_from_key(config_regr_key=file_manager_regr_info)

    file_manager = cast(FileManager, file_manager)
    file_manager.setup(base_directory=directory)

    Registry.register_built_component(component=file_manager,
                                      framework=Framework.GENERIC,
                                      tags={'default'},
                                      name="file_manager")

    logging_path = file_manager.run(filepath=file_manager.logging_directory)
    logging_path = os.path.join(logging_path, file_manager.logging_filename)
    Logger.set_log_path(log_path=logging_path, name='__main__')

    if generate_registration:
        list_registrations()

    return file_manager_regr_info


def list_registrations(
        namespaces: List[str] = None
):
    file_manager = Registry.retrieve_built_component(framework=Framework.GENERIC,
                                                     name='file_manager',
                                                     tags={'default'})
    file_manager = cast(FileManager, file_manager)

    Logger.get_logger(__name__).info(
        f'Saving registration info to folder: {file_manager.registrations_directory}')

    if namespaces is None:
        Logger.get_logger(__name__).info('No namespace set specified. Retrieving all available namespaces...')
        registered_elements = Registry.get_registered_elements()
        namespaces = set([info.namespace for info, element in registered_elements])

    Logger.get_logger(__name__).info(f'Total namespaces: {len(namespaces)}{os.linesep}'
                                     f'Namespaces: {os.linesep}'
                                     f'{namespaces}')
    registration_directory = file_manager.run(filepath=file_manager.registrations_directory)
    for namespace in tqdm(namespaces):
        namespace_registration_path = os.path.join(registration_directory, namespace)

        if not os.path.isdir(namespace_registration_path):
            os.makedirs(namespace_registration_path)

        retrieve_and_save(save_folder=namespace_registration_path,
                          save_name='configurations',
                          namespace_or_filters=namespace)