"""

TODO: add documentation

"""

from abc import ABC, abstractmethod
from typing import Any, Iterable

from dotmap import DotMap

from core.configuration import Configuration
from core.data import ParameterSet


class Component(ABC):
    """
    Base component class.
    Each atomic entity in a model pipeline is a component.
    Components generally receive data and produce other data as output.
    This is generally defined as 'data transformation' process.

    """

    def __init__(
            self,
            config: Configuration
    ):
        self.config = config
        self.config.post_build()
        self.config.validate()

    def __getattr__(
            self,
            item
    ):
        if item in self.config.parameter_set:
            return self.config.parameter_set[item]
        elif item not in self.__dict__:
            raise AttributeError(f'{self.__class__.__name__} has no attribute {item}')

    def __dir__(
            self
    ) -> Iterable[str]:
        return list(super().__dir__()) + list(self.parameter_set.__dir__())

    @property
    def parameter_set(
            self
    ) -> ParameterSet:
        return self.config.parameter_set

    @property
    def state(
            self
    ) -> DotMap:
        return DotMap(self.__dict__)

    @abstractmethod
    def run(
            self,
            *args,
            **kwargs
    ) -> Any:
        pass
