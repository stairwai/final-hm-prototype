import os
from typing import cast

from core.commands import setup_registry
from core.registry import Registry, RegistrationKey, Framework
from shared.components.data_manager import DataManager
from utility.logging_utility import Logger

if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)
    logger = Logger.get_logger(__name__)

    # ExpertsManager (em)
    em_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'experts', 'ams', 'default'})
    em = Registry.retrieve_component_from_key(config_regr_key=em_config_regr_key)
    em = cast(DataManager, em)
    data = em.build()

    logger.info(f'Experts: {os.linesep}{data}')
