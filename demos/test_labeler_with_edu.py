import os
from typing import cast

from core.commands import setup_registry
from core.registry import Registry, RegistrationKey, Framework
from labeler.components.labeler import TextLabeler
from shared.components.data_manager import DataManager
from shared.components.processor import Processor
from utility.logging_utility import Logger
from core.data import FieldSet
from matchmaking.components.algorithm import MatchmakingAlgorithm


if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)
    logger = Logger.get_logger(__name__)

    # LabelManager (lm)
    lm_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'labels', 'file', 'default'})
    lm = Registry.retrieve_component_from_key(config_regr_key=lm_config_regr_key)
    lm = cast(DataManager, lm)
    labels = lm.run()
    logger.info(f'Labels: {os.linesep}{labels}')

    # QueryManager (qm)
    qm_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'queries', 'file', 'default'})
    qm = Registry.retrieve_component_from_key(config_regr_key=qm_config_regr_key)
    qm = cast(DataManager, qm)
    queries = qm.run()
    logger.info(f'Queries: {os.linesep}{queries}')

    # EduManager (em)
    em_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'edu', 'file', 'default'})
    em = Registry.retrieve_component_from_key(config_regr_key=em_config_regr_key)
    em = cast(DataManager, em)
    edu = em.run()
    logger.info(f'Educational Resources: {os.linesep}{edu}')

    # TextProcessor (tp)
    tp_config_regr_key = RegistrationKey(name='processor',
                                         framework=Framework.GENERIC,
                                         tags={'text', 'default'})
    tp = Registry.retrieve_component_from_key(config_regr_key=tp_config_regr_key)
    tp = cast(Processor, tp)
    labels = tp.run(data=labels)
    queries = tp.run(data=queries)
    edu = tp.run(data=edu)

    # TextLabeler (tl)
    tl_config_regr_key = RegistrationKey(name='labeler',
                                         framework=Framework.GENERIC,
                                         tags={'random'},
                                         namespace='baseline')
    tl = Registry.retrieve_component_from_key(config_regr_key=tl_config_regr_key)
    tl = cast(TextLabeler, tl)
    query_scores = tl.run(data=queries,
                          labels=labels)
    logger.info(query_scores)

    edu = tl.run(data=edu)
    logger.info(edu)

    # MatchmakingAlgorithm (ma)
    query_example = FieldSet()
    for key, value in queries.items():
        query_example.add_short(name=value.name, value=value.value[:1] if value.value is not None else value.value)

    query_example.add_short(name='team_size',
                            value=2)
    query_example.add_short(name='languages',
                            value=['English'])

    ma_config_regr_key = RegistrationKey(name='matchmaking',
                                         framework=Framework.GENERIC,
                                         tags={'cosine', 'online'})
    ma = Registry.retrieve_component_from_key(config_regr_key=ma_config_regr_key)
    ma = cast(MatchmakingAlgorithm, ma)
    ma_result = ma.run(queries=query_example,
                       labels=labels,
                       resources=edu,
                       weights={})
    Logger.get_logger(__name__).info(ma_result)

