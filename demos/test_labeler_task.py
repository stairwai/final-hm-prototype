import os

from core.commands import setup_registry
from core.registry import Registry, RegistrationKey, Framework
from labeler.components.task import LabelerTask
from utility.logging_utility import Logger
from typing import cast

if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)

    # LabelerTask (lt)
    lt_config_regr_key = RegistrationKey(name='labeler_task',
                                         framework=Framework.GENERIC,
                                         tags={'random'},
                                         namespace='baseline')
    lt = Registry.retrieve_component_from_key(config_regr_key=lt_config_regr_key)
    lt = cast(LabelerTask, lt)
    task_result = lt.run()
    Logger.get_logger(__name__).info(task_result)
