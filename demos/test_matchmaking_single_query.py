import os

from service.components.task import IndexerTask
from shared.components.data_manager import DataManager
from matchmaking.components.algorithm import MatchmakingAlgorithm
from core.commands import setup_registry
from core.registry import Registry, RegistrationKey, Framework
from utility.logging_utility import Logger
from core.data import FieldSet
from typing import cast

if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)

    # IndexerTask (it)
    it_config_regr_key = RegistrationKey(name='indexer_task',
                                         framework=Framework.GENERIC,
                                         tags={'file', 'random', 'fake'},
                                         namespace='baseline')
    it = Registry.retrieve_component_from_key(config_regr_key=it_config_regr_key)
    it = cast(IndexerTask, it)
    it_result = it.run()
    Logger.get_logger(__name__).info(it_result)

    # QueryManager (qm)
    query_example = FieldSet()
    query_example.add_short(name='team_size',
                            value=[2],
                            tags={'metadata'})
    query_example.add_short(name='languages',
                            value=[['english']],
                            tags={'metadata'})
    query_example.add_short(name='identifier',
                            value=[1],
                            tags={'ID'})
    query_example.add_short(name='description',
                            value=['fake description here'],
                            tags={'text'})
    query_example = it.labeler.run(data=query_example,
                                   normalize_scores=True)
    Logger.get_logger(__name__).info(query_example)

    # MatchmakingAlgorithm (ma)
    ma_config_regr_key = RegistrationKey(name='matchmaking',
                                         framework=Framework.GENERIC,
                                         tags={'cosine', 'online'})
    ma = Registry.retrieve_component_from_key(config_regr_key=ma_config_regr_key)
    ma = cast(MatchmakingAlgorithm, ma)
    ma_result = ma.run(queries=query_example,
                       labels=it_result.labels,
                       resources=it_result.resources)
    Logger.get_logger(__name__).info(ma_result)
