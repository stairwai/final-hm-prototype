import os

from core.commands import setup_registry
from core.registry import Registry, RegistrationKey, Framework
from labeler.components.task import LabelerClassifierTask
from utility.logging_utility import Logger
from typing import cast

if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)

    # LabelerClassifierTask (lct)
    lct_config_regr_key = RegistrationKey(name='labeler_classifier_task',
                                          framework=Framework.TORCH,
                                          tags={'all-mpnet-base-v2', 'cosine', 'descr2text', 'sbert'},
                                          namespace='unibo')
    lct = Registry.retrieve_component_from_key(config_regr_key=lct_config_regr_key)
    lct = cast(LabelerClassifierTask, lct)
    task_result = lct.run()
    Logger.get_logger(__name__).info(task_result)
