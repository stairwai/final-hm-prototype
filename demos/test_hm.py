import os


from core.commands import setup_registry
from hm import HorizontalMatchmaking
from utility.logging_utility import Logger

if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)
    logger = Logger.get_logger(__name__)

    # Init
    hm = HorizontalMatchmaking(
        label_manager_config_regr_key="name:data_manager--framework:generic--tags:['file', 'default', 'labels']--namespace:default",
        resource_manager_config_regr_key="name:data_manager--framework:generic--tags:['experts', 'default', 'file', 'fake']--namespace:default",
        labeler_config_regr_key="name:labeler--framework:torch--tags:['all-mpnet-base-v2', 'cosine', 'descr2text', 'sbert']--namespace:unibo",
        matchmaking_config_regr_key="name:matchmaking--framework:generic--tags:['cosine', 'online']--namespace:default"
    )

    # Label options
    logger.info(hm.label_options)

    # Labeling
    labeling_query = "yadda yadda yadda. yadda yadda."
    labeling_result = hm.run_labeling(query=labeling_query)

    # Matchmaking
    matchmaking_query = "yadda yadda yadda. yadda yadda."
    mm_result = hm.run_matchmaking(query=matchmaking_query,
                                   return_explanations=True)
    logger.info(mm_result)
