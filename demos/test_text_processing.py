from core.registry import Registry, RegistrationKey, Framework
from shared.components.data_manager import DataManager
from shared.components.processor import Processor
from core.commands import setup_registry
from utility.logging_utility import Logger
from typing import cast
import os


if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)
    logger = Logger.get_logger(__name__)

    # LabelManager (lm)
    lm_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'labels', 'file', 'default'})
    lm = Registry.retrieve_component_from_key(config_regr_key=lm_config_regr_key)
    lm = cast(DataManager, lm)
    labels = lm.run()
    logger.info(f'Labels: {os.linesep}{labels}')

    # TextProcessor (tp)
    tp_config_regr_key = RegistrationKey(name='processor',
                                         framework=Framework.GENERIC,
                                         tags={'default', 'text'})
    tp = Registry.retrieve_component_from_key(config_regr_key=tp_config_regr_key)
    tp = cast(Processor, tp)
    labels = tp.run(data=labels)
    logger.info(f'Labels: {os.linesep}{labels}')