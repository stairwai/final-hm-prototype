import os

from service.components.task import IndexerTask
from core.commands import setup_registry
from core.registry import Registry, RegistrationKey, Framework
from utility.logging_utility import Logger
from typing import cast
from subprocess import Popen


if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)

    # IndexerTask (it)
    it_config_regr_key = RegistrationKey(name='indexer_task',
                                         framework=Framework.GENERIC,
                                         tags={'random', 'ams', 'experts'},
                                         namespace='baseline')
    it = Registry.retrieve_component_from_key(config_regr_key=it_config_regr_key)
    it = cast(IndexerTask, it)

    # Reset DB for debugging purposes
    # it.output_resource_manager.driver.hard_db_reset(False)
    # cmd = ' '.join([f'python {os.path.normpath(os.path.join(os.getcwd(), os.pardir, "db-initializer", "setup.py"))}'])
    # process = Popen(cmd, shell=True)
    # process.wait()

    task_result = it.run()
    Logger.get_logger(__name__).info(task_result)
