import os
from typing import cast

from core.commands import setup_registry
from core.registry import Registry, RegistrationKey, Framework
from labeler.components.labeler import TextLabeler
from shared.components.data_manager import DataManager
from shared.components.processor import Processor
from utility.logging_utility import Logger
from core.data import FieldSet
from matchmaking.components.algorithm import MatchmakingAlgorithm

if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)
    logger = Logger.get_logger(__name__)

    # LabelManager (lm)
    lm_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'labels', 'file', 'default'})
    lm = Registry.retrieve_component_from_key(config_regr_key=lm_config_regr_key)
    lm = cast(DataManager, lm)
    labels = lm.run()
    logger.info(f'Labels: {os.linesep}{labels}')

    # EduManagerFile (em_file)
    em_file_key = RegistrationKey(name='data_manager',
                                  framework=Framework.GENERIC,
                                  tags={'edu', 'file', 'default'})
    em_file = Registry.retrieve_component_from_key(config_regr_key=em_file_key)
    em_file = cast(DataManager, em_file)
    edu = em_file.run()
    logger.info(f'Educational Resources: {os.linesep}{edu}')

    # TextProcessor (tp)
    tp_config_regr_key = RegistrationKey(name='processor',
                                         framework=Framework.GENERIC,
                                         tags={'text', 'default'})
    tp = Registry.retrieve_component_from_key(config_regr_key=tp_config_regr_key)
    tp = cast(Processor, tp)
    labels = tp.run(data=labels)
    edu = tp.run(data=edu)

    # TextLabeler (tl)
    tl_config_regr_key = RegistrationKey(name='labeler',
                                         framework=Framework.GENERIC,
                                         tags={'random'},
                                         namespace='baseline')
    tl = Registry.retrieve_component_from_key(config_regr_key=tl_config_regr_key)
    tl = cast(TextLabeler, tl)
    edu = tl.run(data=edu, labels=labels)
    logger.info(edu)

    # EduManagerAMS (em_ams)
    em_ams_key = RegistrationKey(name='data_manager',
                                 framework=Framework.GENERIC,
                                 tags={'edu', 'ams', 'default'})
    em_ams = Registry.retrieve_component_from_key(config_regr_key=em_ams_key)
    em_ams = cast(DataManager, em_ams)
    edu = em_ams.run(data=edu)
    logger.info(f'Educational Resources: {os.linesep}{edu}')

    edu_loaded = em_ams.build()
    logger.info(f'Educational Resources from AMS: {os.linesep}{edu_loaded}')