from core.registry import Registry, RegistrationKey, Framework
from shared.components.data_manager import DataManager
from core.commands import setup_registry
from utility.logging_utility import Logger
import os
from typing import cast


if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)
    logger = Logger.get_logger(__name__)

    # LabelManager (lm)
    lm_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'labels', 'file', 'default'})
    lm = Registry.retrieve_component_from_key(config_regr_key=lm_config_regr_key)
    lm = cast(DataManager, lm)
    labels = lm.run()
    logger.info(f'Labels: {os.linesep}{labels}')

    # QueryManager (qm)
    qm_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'queries', 'file', 'default'})
    qm = Registry.retrieve_component_from_key(config_regr_key=qm_config_regr_key)
    qm = cast(DataManager, qm)
    queries = qm.run()
    logger.info(f'Queries: {os.linesep}{queries}')

    # AssetsManager (am)
    am_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'assets', 'file', 'default'})
    am = Registry.retrieve_component_from_key(config_regr_key=am_config_regr_key)
    am = cast(DataManager, am)
    assets = am.run()
    logger.info(f'Assets: {os.linesep}{assets}')

    # ExpertsManager (em)
    em_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'experts', 'fake', 'file', 'default'})
    em = Registry.retrieve_component_from_key(config_regr_key=em_config_regr_key)
    em = cast(DataManager, em)
    experts = em.run()
    logger.info(f'Experts: {os.linesep}{experts}')