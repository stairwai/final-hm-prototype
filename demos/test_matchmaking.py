import os

from service.components.task import IndexerTask
from shared.components.data_manager import DataManager
from matchmaking.components.algorithm import MatchmakingAlgorithm
from core.commands import setup_registry
from core.registry import Registry, RegistrationKey, Framework
from utility.logging_utility import Logger
from core.data import FieldSet
from typing import cast

if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)

    # IndexerTask (it)
    it_config_regr_key = RegistrationKey(name='indexer_task',
                                         framework=Framework.GENERIC,
                                         tags={'file', 'random', 'fake'},
                                         namespace='baseline')
    it = Registry.retrieve_component_from_key(config_regr_key=it_config_regr_key)
    it = cast(IndexerTask, it)
    it_result = it.run()
    Logger.get_logger(__name__).info(it_result)

    # QueryManager (qm)
    qm_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'default', 'queries', 'file'})
    qm = Registry.retrieve_component_from_key(config_regr_key=qm_config_regr_key)
    qm = cast(DataManager, qm)
    queries = qm.run()
    queries = it.labeler.run(data=queries, normalize_scores=True)
    Logger.get_logger(__name__).info(queries)

    # MatchmakingAlgorithm (ma)
    query_example = FieldSet()
    for key, value in queries.items():
        query_example.add_short(name=value.name, value=value.value[:1] if value.value is not None else value.value)

    query_example.add_short(name='team_size',
                            value=2)
    query_example.add_short(name='languages',
                            value=['English'])

    ma_config_regr_key = RegistrationKey(name='matchmaking',
                                         framework=Framework.GENERIC,
                                         tags={'cosine', 'online'})
    ma = Registry.retrieve_component_from_key(config_regr_key=ma_config_regr_key)
    ma = cast(MatchmakingAlgorithm, ma)
    ma_result = ma.run(queries=query_example,
                       labels=it_result.labels,
                       resources=it_result.resources,
                       weights={'languages': 0.5})
    Logger.get_logger(__name__).info(ma_result)
