import os
from typing import cast

from core.commands import setup_registry
from core.registry import Registry, RegistrationKey, Framework
from labeler.components.labeler import TextLabeler
from shared.components.data_manager import DataManager
from shared.components.processor import Processor
from utility.logging_utility import Logger

if __name__ == '__main__':
    setup_registry(directory=os.path.join(os.getcwd(), os.pardir),
                   module_directories=[os.path.join(os.getcwd(),
                                                    os.pardir)],
                   generate_registration=True)
    logger = Logger.get_logger(__name__)

    # LabelManager (lm)
    lm_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'labels', 'file', 'default'})
    lm = Registry.retrieve_component_from_key(config_regr_key=lm_config_regr_key)
    lm = cast(DataManager, lm)
    labels = lm.run()
    logger.info(f'Labels: {os.linesep}{labels}')

    # QueryManager (qm)
    qm_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'queries', 'file', 'default'})
    qm = Registry.retrieve_component_from_key(config_regr_key=qm_config_regr_key)
    qm = cast(DataManager, qm)
    queries = qm.run()
    logger.info(f'Queries: {os.linesep}{queries}')

    # AssetsManager (am)
    am_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'assets', 'file', 'default'})
    am = Registry.retrieve_component_from_key(config_regr_key=am_config_regr_key)
    am = cast(DataManager, am)
    assets = am.run()
    logger.info(f'Assets: {os.linesep}{assets}')

    # ExpertsManager (em)
    em_config_regr_key = RegistrationKey(name='data_manager',
                                         framework=Framework.GENERIC,
                                         tags={'experts', 'file', 'default'})
    em = Registry.retrieve_component_from_key(config_regr_key=em_config_regr_key)
    em = cast(DataManager, em)
    experts = em.run()
    logger.info(f'Experts: {os.linesep}{experts}')

    # TextProcessor (tp)
    tp_config_regr_key = RegistrationKey(name='processor',
                                         framework=Framework.GENERIC,
                                         tags={'text', 'default'})
    tp = Registry.retrieve_component_from_key(config_regr_key=tp_config_regr_key)
    tp = cast(Processor, tp)
    labels = tp.run(data=labels)
    queries = tp.run(data=queries)
    assets = tp.run(data=assets)
    experts = tp.run(data=experts)

    # TextLabeler (tl)
    tl_config_regr_key = RegistrationKey(name='labeler',
                                         framework=Framework.GENERIC,
                                         tags={'random'},
                                         namespace='baseline')
    tl = Registry.retrieve_component_from_key(config_regr_key=tl_config_regr_key)
    tl = cast(TextLabeler, tl)
    query_scores = tl.run(data=queries,
                          labels=labels)
    logger.info(query_scores)

    asset_scores = tl.run(data=assets)
    logger.info(asset_scores)

    expert_scores = tl.run(data=experts, labels=labels, normalize_scores=True)
    logger.info(expert_scores)
