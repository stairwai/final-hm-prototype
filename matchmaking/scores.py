import abc

import numpy as np


class MatchmakingMetric:

    @abc.abstractmethod
    def __call__(
            self,
            x: np.ndarray,
            y: np.ndarray,
            return_explanations: bool = False
    ):
        """
        Compute the relavance between two score matrices

        Parameters
        ----------
        x: numpy array or equivalent
            First score matrix (n x k, where k is the number of scores)
        y: numpy array or equivalent
            Second score matrix (m x k, where k is the number of scores)

        Returns
        -------
        numpy array:
            the score vector
        list:
            explanations
        """
        pass


class ScalProdMetric(MatchmakingMetric):

    def __call__(
            self,
            x: np.ndarray,
            y: np.ndarray,
            return_explanations: bool = False
    ):
        if not return_explanations:
            # Compute scores
            scores = x.dot(y.T)
            return scores, None
        else:
            # Compute individual terms of the dot product
            pre_dot = x[:, None, :] * y[None, :, :]
            # Compute the scores
            scores = pre_dot.sum(axis=2)
            return scores, pre_dot


class CosineSimilarityMetric(MatchmakingMetric):

    def __call__(
            self,
            x: np.ndarray,
            y: np.ndarray,
            return_explanations: bool = False
    ):
        if not return_explanations:
            # Compute dot product
            scores_raw = x.dot(y.T)

            # Compute norms
            normx = np.linalg.norm(x, ord=2, axis=1).reshape(1, -1)
            normy = np.linalg.norm(y, ord=2, axis=1).reshape(1, -1)

            # Compute norm product
            normp = normx.T.dot(normy)

            # Compute actual scores
            scores = np.divide(scores_raw, normp,
                               out=np.zeros_like(scores_raw), where=normp!=0)
            # scores = np.nan_to_num(scores_raw / normp)

            # Return normalized scores
            return scores, None
        else:
            # Compute individual alignment scores
            pre_dot_raw = x[:, None, :] * y[None, :, :]

            # Compute norms
            normx = np.linalg.norm(x, ord=2, axis=1).reshape(1, -1)
            normy = np.linalg.norm(y, ord=2, axis=1).reshape(1, -1)

            # Compute norm product
            normp = normx.T.dot(normy)

            # Normalize the individual scores
            # pre_dot = pre_dot_raw / normp[:, :, None]
            num, den = pre_dot_raw, normp[:, :, None]
            pre_dot = np.divide(num, den, out=np.zeros_like(num), where=den!=0)

            # Compute the individual terms of the dot product
            scores = pre_dot.sum(axis=2)

            return scores, pre_dot
