import abc
from typing import Iterable, Dict

import numpy as np

from core.component import Component
from core.data import FieldSet


class MatchmakingAlgorithm(Component):

    def __init__(
            self,
            **kwargs
    ):
        super().__init__(**kwargs)
        self.resources = None
        self.labels = None

    @abc.abstractmethod
    def match_one(
            self,
            query: FieldSet,
            resources: FieldSet,
            labels: FieldSet,
            weights: Dict[str, float]
    ) -> FieldSet:
        pass

    @abc.abstractmethod
    def match_many(
            self,
            queries: FieldSet,
            resources: FieldSet,
            labels: FieldSet,
            weights: Dict[str, float]
    ) -> FieldSet:
        pass

    def run(
            self,
            queries: FieldSet,
            weights: Dict[str, float],
            resources: FieldSet = None,
            labels: FieldSet = None
    ) -> FieldSet:
        if resources is None:
            assert self.resources is not None, f'Expected some resources for matchmaking'
        elif self.resources is None:
            self.resources = resources

        if labels is None:
            assert self.labels is not None, f'Expected some labels for matchmaking'
        elif self.labels is None:
            self.labels = labels

        if self.strategy == 'one':
            assert len(queries.labeler_score) == 1, 'Match one expects a single query'
            return self.match_one(query=queries,
                                  resources=self.resources,
                                  labels=labels,
                                  weights=weights)
        else:
            return self.match_many(queries=queries,
                                   resources=resources,
                                   labels=self.labels,
                                   weights=weights)


class OnlineMatchmaking(MatchmakingAlgorithm):

    def _compute_labeler_score(
            self,
            query: FieldSet,
            resources: FieldSet
    ):
        if resources.labeler_score is None or query.labeler_score is None:
            raise AttributeError('Expected labeler_score field to be available in resources and query.')

        # Transform resource and query into array format
        resource_matrix = self.vectorizer.fit_transform(resources.labeler_score).toarray()
        query_vector = self.vectorizer.transform(query.labeler_score).toarray()

        # Compute matching scores
        labeler_matching_score, matching_explanation = self.metric(query_vector,
                                                                   resource_matrix,
                                                                   return_explanations=self.return_explanations)
        labeler_matching_score = labeler_matching_score[0]
        matching_explanation = matching_explanation[0] if matching_explanation is not None else matching_explanation
        return labeler_matching_score, matching_explanation

    def _compute_team_size_score(
            self,
            query: FieldSet,
            resources: FieldSet
    ):
        # TODO: add warning
        if resources.get_field('team_size', None) is None or query.get_field('team_size', None) is None:
            return np.zeros((len(resources.identifier)))

        # Transform resource and query into array format
        resource_team_size = np.array(resources.team_size, dtype=float)
        resource_team_size = np.nan_to_num(resource_team_size, nan=0)
        query_team_size = np.array(query.team_size, dtype=float)

        # [# resources]
        team_size_diff = np.maximum(0, query_team_size - resource_team_size)
        team_size_scores = 1 - (1 / (1 + np.exp(-team_size_diff)))

        return team_size_scores

    def _compute_languages_score(
            self,
            query: FieldSet,
            resources: FieldSet
    ):
        # TODO: add warning
        if resources.get_field('languages', None) is None or \
                query.get_field('languages', None) is None or \
                len(query.languages[0]) == 0:
            return np.zeros((len(resources.identifier)))

        # Transform resource and query into one_hot encoding
        query_languages = query.languages[0]
        language_map = {query_languages[i]: i for i in range(len(query_languages))}

        resource_languages = np.zeros((len(resources.languages), len(language_map)))
        for i, languages in enumerate(resources.languages):
            if languages is not None:
                languages_as_list = [languages] if isinstance(languages, str) else languages
                for l in languages_as_list:
                    idx = language_map.get(l, None)
                    if idx is not None:
                        resource_languages[i][idx] = 1.

        query_lang_mask = np.ones((1, len(query_languages)))

        languages_scores, _ = self.metric(query_lang_mask,
                                          resource_languages,
                                          return_explanations=False)
        return languages_scores.ravel()

    def match_one(
            self,
            query: FieldSet,
            resources: FieldSet,
            labels: FieldSet,
            weights: Dict[str, float]
    ) -> FieldSet:
        assert len(resources) > 0, "No resources found."

        # Matching score on the labeler_score
        labeler_matching_score, matching_explanation = self._compute_labeler_score(query=query,
                                                                                   resources=resources)
        # Matching score on team_size
        team_size_scores = self._compute_team_size_score(query=query,
                                                         resources=resources)

        # Matching score on languages
        languages_scores = self._compute_languages_score(query=query,
                                                         resources=resources)

        # Combine scores
        matching_score = weights.get('labeler', 1.0) * labeler_matching_score +\
                         weights.get('team_size', 1.0) * team_size_scores +\
                         weights.get('languages', 1.0) * languages_scores

        # Sort resources and explanations
        resource_indexes = np.argsort(-matching_score)

        for key, value in resources.items():
            if value.value is not None:
                value.value = [value.value[idx] for idx in resource_indexes]

        resources.add_short(name='matchmaking_score',
                            value=matching_score[resource_indexes],
                            typing=Iterable[float],
                            description='Matchmaking score')
        if matching_explanation is not None:
            # Concatenate individual scores' explanations (labeler, team size, and languages)
            matching_explanation = np.concatenate((weights.get('labeler', 1.0) * matching_explanation,
                                                   weights.get('team_size', 1.0) * team_size_scores[:, np.newaxis],
                                                   weights.get('languages', 1.0) * languages_scores[:, np.newaxis]), axis=-1)

            # Define the column names for the explanation data
            vocabulary = list(self.vectorizer.vocabulary_.items())
            vocabulary.append(('Team Size', len(vocabulary)))
            vocabulary.append(('Languages', len(vocabulary)))

            # resources.add_short(name='matchmaking_explanation',
            #                     value=list(map(
            #                         lambda x: {key: x[value] for key, value in self.vectorizer.vocabulary_.items()},
            #                         matching_explanation[resource_indexes])))
            resources.add_short(name='matchmaking_explanation',
                                value=list(map(
                                    lambda x: {key: x[value] for key, value in vocabulary},
                                    matching_explanation[resource_indexes])))

        return resources

    # TODO: implement
    def match_many(
            self,
            query: FieldSet,
            resources: FieldSet,
            labels: FieldSet,
            weights: Dict[str, float]
    ) -> FieldSet:
        pass
