from sklearn.feature_extraction import DictVectorizer

from core.configuration import Configuration
from core.data import ParameterSet
from core.registry import Registry, Framework
from matchmaking.components.algorithm import OnlineMatchmaking
from matchmaking.scores import MatchmakingMetric, ScalProdMetric, CosineSimilarityMetric


class OnlineMatchmakingConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = ParameterSet()

        parameter_set.add_short(name='metric',
                                is_required=True,
                                typing=MatchmakingMetric,
                                description='Matchmaking metric for numerical comparison')
        parameter_set.add_short(name='strategy',
                                value='one',
                                allowed_range=lambda value: value in ['one', 'many'],
                                is_required=True,
                                typing=str,
                                description='Matchmaking strategy to adopt')
        parameter_set.add_short(name='return_explanations',
                                value=True,
                                typing=bool,
                                description='If enabled, the matchmaking strategy also returns score explanations')
        parameter_set.add_short(name='vectorizer',
                                value=DictVectorizer(),
                                typing=DictVectorizer,
                                description='Vectorizer for numerical transformation')
        return parameter_set


def register_matchmaking_algorithms():
    # Cosine
    Registry.register_and_map(configuration=OnlineMatchmakingConfig.get_delta_copy_by_dict(
        params={
            'metric': CosineSimilarityMetric()
        }
    ),
        component_class=OnlineMatchmaking,
        framework=Framework.GENERIC,
        name='matchmaking',
        tags={'online', 'cosine'})

    # ScalProd
    Registry.register_and_map(configuration=OnlineMatchmakingConfig.get_delta_copy_by_dict(
        params={
            'metric': ScalProdMetric()
        }
    ),
        component_class=OnlineMatchmaking,
        framework=Framework.GENERIC,
        name='matchmaking',
        tags={'online', 'scalprod'})
