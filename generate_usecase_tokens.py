import os
from util import ssac
from doc_api.ams_api.StairwAIDriver import Driver
import pandas as pd
from argparse import ArgumentParser

# reset = False
uc_dir = os.path.join('data', 'ssac', 'matchmaking_usecase')
default_fname = os.path.join('data', 'queries', 'use-cases-3rd-open-call.csv')

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--reset', '-r', default=False, type=bool)
    parser.add_argument('--file', '-f', default=default_fname, type=str)
    args = parser.parse_args()

    reset = args.reset
    fname = args.file

    # If requested, clean all existing token files
    if reset:
        ssac.clean_files(uc_dir, only_test_files=False)

    # Load usecase metadata
    # fname = os.path.join('data', 'queries', 'use-cases-2nd-open-call.csv')
    # fname = os.path.join('data', 'queries', 'use-cases-3rd-open-call.csv')
    usecase_metadata_df = pd.read_csv(fname)

    # Convert usecase metadata format
    # usecase_metadata = [{'name': uc['name'], 'email': uc['email']} for idx, uc in usecase_metadata_df.iterrows()]
    usecase_metadata = [{'name': uc['name'], 
                         'email': uc['email'],
                         'description': uc['description'] if 'description' in uc.index else '',
                         'uuid identifier': uc['uuid identifier']}
                        for idx, uc in usecase_metadata_df.iterrows()]

    # Generate a few testing tokens
    generated_ids = ssac.generate_storage_files(metadata=usecase_metadata, directory=uc_dir, testing=False)

    # Write token links
    for idx in generated_ids:
        print(f'http://127.0.0.1:5000/hm_query_usecase_gui?token={str(idx)}')

    # write all the data to a file
    lines = []
    for idx, uc in zip(generated_ids, usecase_metadata):
        lines.append(f'{uc["name"]},http://127.0.0.1:5000/hm_query_usecase_gui?token={str(idx)}\n')

    mode = 'w' if reset else 'a'
    fname = os.path.join('data', 'usecase_tokens.csv')
    with open(fname, mode) as fp:
        fp.writelines(lines)
