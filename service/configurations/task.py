from core.configuration import Configuration
from core.data import ParameterSet
from core.registry import Registry, Framework, RegistrationKey
from labeler.components.labeler import TextLabeler
from service.components.task import IndexerTask
from shared.components.data_manager import DataManager
from shared.components.processor import Processor


class IndexerTaskConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = ParameterSet()

        parameter_set.add_short(name='name',
                                typing=str,
                                is_required=True,
                                description="Task unique identifier.")
        parameter_set.add_short(name='label_manager',
                                typing=DataManager,
                                is_required=True,
                                is_registration=True,
                                description='Data manager component for loading labels.')
        parameter_set.add_short(name='label_processor',
                                typing=Processor,
                                value=RegistrationKey(name='processor',
                                                      framework=Framework.GENERIC,
                                                      tags={'pipeline', 'default'}),
                                is_required=True,
                                is_registration=True,
                                description='Processor for pre-processing labels.')
        parameter_set.add_short(name='input_resource_manager',
                                typing=DataManager,
                                is_required=True,
                                is_registration=True,
                                description='Data manager component for loading input resources'
                                            ' (e.g., experts, assets).')
        parameter_set.add_short(name='output_resource_manager',
                                typing=DataManager,
                                is_required=True,
                                is_registration=True,
                                description='Data manager component for storing output resources'
                                            ' (e.g., experts, assets).')
        parameter_set.add_short(name='resource_processor',
                                typing=Processor,
                                value=RegistrationKey(name='processor',
                                                      framework=Framework.GENERIC,
                                                      tags={'pipeline', 'default'}),
                                is_required=True,
                                is_registration=True,
                                description='Processor for pre-processing resources.')
        parameter_set.add_short(name='labeler',
                                typing=TextLabeler,
                                is_required=True,
                                is_registration=True,
                                description='Text labeler for ontology matching')
        return parameter_set


class ExpertsFileIndexerTaskConfig(IndexerTaskConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.label_manager = RegistrationKey(name='data_manager',
                                                      framework=Framework.GENERIC,
                                                      tags={'file', 'default', 'labels'})
        parameter_set.input_resource_manager = RegistrationKey(name='data_manager',
                                                               framework=Framework.GENERIC,
                                                               tags={'file', 'default', 'experts'})
        parameter_set.output_resource_manager = RegistrationKey(name='data_manager',
                                                                framework=Framework.GENERIC,
                                                                tags={'file', 'default', 'experts'})
        return parameter_set


class EduFileIndexerTaskConfig(IndexerTaskConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.label_manager = RegistrationKey(name='data_manager',
                                                      framework=Framework.GENERIC,
                                                      tags={'file', 'default', 'labels'})
        parameter_set.input_resource_manager = RegistrationKey(name='data_manager',
                                                               framework=Framework.GENERIC,
                                                               tags={'file', 'default', 'edu'})
        parameter_set.output_resource_manager = RegistrationKey(name='data_manager',
                                                                framework=Framework.GENERIC,
                                                                tags={'file', 'default', 'edu'})
        return parameter_set


class PaperFileIndexerTaskConfig(IndexerTaskConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.label_manager = RegistrationKey(name='data_manager',
                                                      framework=Framework.GENERIC,
                                                      tags={'file', 'default', 'labels'})
        parameter_set.input_resource_manager = RegistrationKey(name='data_manager',
                                                               framework=Framework.GENERIC,
                                                               tags={'file', 'default', 'paper'})
        parameter_set.output_resource_manager = RegistrationKey(name='data_manager',
                                                                framework=Framework.GENERIC,
                                                                tags={'file', 'default', 'paper'})
        return parameter_set


class ExpertsAMSIndexerTaskConfig(IndexerTaskConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.label_manager = RegistrationKey(name='data_manager',
                                                      framework=Framework.GENERIC,
                                                      tags={'ams', 'default', 'labels'})
        parameter_set.input_resource_manager = RegistrationKey(name='data_manager',
                                                               framework=Framework.GENERIC,
                                                               tags={'file', 'default', 'experts'})
        parameter_set.output_resource_manager = RegistrationKey(name='data_manager',
                                                                framework=Framework.GENERIC,
                                                                tags={'ams', 'default', 'experts'})
        return parameter_set


class EduAMSIndexerTaskConfig(IndexerTaskConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.label_manager = RegistrationKey(name='data_manager',
                                                      framework=Framework.GENERIC,
                                                      tags={'ams', 'default', 'labels'})
        parameter_set.input_resource_manager = RegistrationKey(name='data_manager',
                                                               framework=Framework.GENERIC,
                                                               tags={'file', 'default', 'edu'})
        parameter_set.output_resource_manager = RegistrationKey(name='data_manager',
                                                                framework=Framework.GENERIC,
                                                                tags={'ams', 'default', 'edu'})
        return parameter_set


class PaperAMSIndexerTaskConfig(IndexerTaskConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.label_manager = RegistrationKey(name='data_manager',
                                                      framework=Framework.GENERIC,
                                                      tags={'ams', 'default', 'labels'})
        parameter_set.input_resource_manager = RegistrationKey(name='data_manager',
                                                               framework=Framework.GENERIC,
                                                               tags={'file', 'default', 'paper'})
        parameter_set.output_resource_manager = RegistrationKey(name='data_manager',
                                                                framework=Framework.GENERIC,
                                                                tags={'ams', 'default', 'paper'})
        return parameter_set


def register_tasks():
    # FileIndexerTask

    # Random
    Registry.register_and_map(configuration=ExpertsFileIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': 'random_indexer',
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.GENERIC,
                                       tags={'random'},
                                       namespace='baseline')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.GENERIC,
        name='indexer_task',
        tags={'random', 'file', 'experts'},
        namespace='baseline')

    Registry.register_and_map(configuration=EduFileIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': 'random_indexer',
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.GENERIC,
                                       tags={'random'},
                                       namespace='baseline')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.GENERIC,
        name='indexer_task',
        tags={'random', 'file', 'edu'},
        namespace='baseline')

    Registry.register_and_map(configuration=PaperFileIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': 'random_indexer',
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.GENERIC,
                                       tags={'random'},
                                       namespace='baseline')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.GENERIC,
        name='indexer_task',
        tags={'random', 'file', 'paper'},
        namespace='baseline')

    # SBERT
    Registry.register_and_map(configuration=ExpertsFileIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': "sbert-cosine_indexer",
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.TORCH,
                                       tags={'descr2text', 'sbert', 'all-mpnet-base-v2', 'cosine'},
                                       namespace='unibo')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.TORCH,
        name='indexer_task',
        tags={'sbert', 'all-mpnet-base-v2', 'cosine', 'descr2text', 'file', 'experts'},
        namespace='unibo')

    Registry.register_and_map(configuration=EduFileIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': "sbert-cosine_indexer",
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.TORCH,
                                       tags={'descr2text', 'sbert', 'all-mpnet-base-v2', 'cosine'},
                                       namespace='unibo')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.TORCH,
        name='indexer_task',
        tags={'sbert', 'all-mpnet-base-v2', 'cosine', 'descr2text', 'file', 'edu'},
        namespace='unibo')

    Registry.register_and_map(configuration=PaperFileIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': "sbert-cosine_indexer",
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.TORCH,
                                       tags={'descr2text', 'sbert', 'all-mpnet-base-v2', 'cosine'},
                                       namespace='unibo')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.TORCH,
        name='indexer_task',
        tags={'sbert', 'all-mpnet-base-v2', 'cosine', 'descr2text', 'file', 'paper'},
        namespace='unibo')

    # AMSIndexerTask

    # Random
    Registry.register_and_map(configuration=ExpertsAMSIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': 'random_indexer',
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.GENERIC,
                                       tags={'random'},
                                       namespace='baseline')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.GENERIC,
        name='indexer_task',
        tags={'random', 'ams', 'experts'},
        namespace='baseline')

    Registry.register_and_map(configuration=EduAMSIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': 'random_indexer',
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.GENERIC,
                                       tags={'random'},
                                       namespace='baseline')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.GENERIC,
        name='indexer_task',
        tags={'random', 'ams', 'edu'},
        namespace='baseline')

    Registry.register_and_map(configuration=PaperAMSIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': 'random_indexer',
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.GENERIC,
                                       tags={'random'},
                                       namespace='baseline')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.GENERIC,
        name='indexer_task',
        tags={'random', 'ams', 'paper'},
        namespace='baseline')

    # SBERT
    Registry.register_and_map(configuration=ExpertsAMSIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': "sbert-cosine_indexer",
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.TORCH,
                                       tags={'descr2text', 'sbert', 'all-mpnet-base-v2', 'cosine'},
                                       namespace='unibo')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.TORCH,
        name='indexer_task',
        tags={'sbert', 'all-mpnet-base-v2', 'cosine', 'descr2text', 'ams', 'experts'},
        namespace='unibo')

    Registry.register_and_map(configuration=EduAMSIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': "sbert-cosine_indexer",
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.TORCH,
                                       tags={'descr2text', 'sbert', 'all-mpnet-base-v2', 'cosine'},
                                       namespace='unibo')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.TORCH,
        name='indexer_task',
        tags={'sbert', 'all-mpnet-base-v2', 'cosine', 'descr2text', 'ams', 'edu'},
        namespace='unibo')

    Registry.register_and_map(configuration=PaperAMSIndexerTaskConfig.get_delta_copy_by_dict(
        params={
            'name': "sbert-cosine_indexer",
            'labeler': RegistrationKey(name='labeler',
                                       framework=Framework.TORCH,
                                       tags={'descr2text', 'sbert', 'all-mpnet-base-v2', 'cosine'},
                                       namespace='unibo')
        }
    ),
        component_class=IndexerTask,
        framework=Framework.TORCH,
        name='indexer_task',
        tags={'sbert', 'all-mpnet-base-v2', 'cosine', 'descr2text', 'ams', 'paper'},
        namespace='unibo')

