from core.data import FieldSet
from shared.components.task import Task

class IndexerTask(Task):

    def run(
            self,
    ) -> FieldSet:
        return_field = super().run()

        # Labels
        labels = self.label_manager.run()
        labels = self.label_processor.run(data=labels)
        return_field.add_short(name='labels',
                               value=labels,
                               description='Ontology labels')

        # Resources (e.g., AI experts)
        resources = self.input_resource_manager.run()
        resources = self.resource_processor.run(data=resources)

        # Labeler (memorize labels at first run)
        resources = self.labeler.run(data=resources,
                                     labels=labels)

        # Update resources
        resources = self.output_resource_manager.run(data=resources)
        return_field.add_short(name='resources',
                               value=resources,
                               description='Matchmaking resources')
        return return_field
