import os

from service.components.task import IndexerTask
from core.commands import setup_registry
from core.registry import Registry
from utility.logging_utility import Logger
from typing import cast
from subprocess import Popen
from argparse import ArgumentParser


if __name__ == '__main__':
    setup_registry(directory=os.getcwd())

    parser = ArgumentParser()
    parser.add_argument('--framework', '-f', default='torch', type=str)
    parser.add_argument('--tags', '-t', nargs='+', default=['all-mpnet-base-v2', 'ams', 'cosine', 'descr2text', 'experts', 'sbert'])
    # parser.add_argument('--tags', '-t', nargs='+', default={})
    parser.add_argument('--namespace', '-n', default='unibo', type=str)
    parser.add_argument('--reset_db', '-r', default=False, type=bool)
    args = parser.parse_args()

    # print('=' * 78)
    # print(args)
    # print('=' * 78)
    # raise Exception('KABOOM')

    # IndexerTask key
    # it_config_regr_key = f"name:indexer_task--framework:torch--tags:['all-mpnet-base-v2', 'ams', 'cosine', 'descr2text', 'experts', 'sbert']--namespace:unibo"
    it_config_regr_key = f"name:indexer_task--framework:{args.framework}--tags:{args.tags}--namespace:{args.namespace}"
    # it_config_regr_key = RegistrationKey(name='indexer_task',
    #                                      framework=Framework.GENERIC,
    #                                      tags={'random', 'ams', 'experts'},
    #                                      namespace='baseline')

    # Retrieve indexer task
    it = Registry.retrieve_component_from_key(config_regr_key=it_config_regr_key)
    it = cast(IndexerTask, it)

    # Reset DB for debugging purposes
    if args.reset_db:
        it.output_resource_manager.driver.hard_db_reset(False)
        # cmd = ' '.join([f'python3 {os.path.normpath(os.path.join(os.getcwd(), "db-initializer", "setup.py"))}'])
        # process = Popen(cmd, shell=True)
        # process.wait()

    task_result = it.run()
    Logger.get_logger(__name__).info(task_result)
