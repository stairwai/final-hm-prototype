#!/bin/python3
from flask import Flask
from flask import jsonify
from flask import render_template
from flask import request

import os
from core.commands import setup_registry
# from hm import HorizontalMatchmaking
from hm import UseCaseExpertHorizontalMatchmaking
from hm import EduResourceHorizontalMatchmaking
from hm import PaperHorizontalMatchmaking
from utility.logging_utility import Logger
import py2neo
import time

# ==============================================================================
# Service setup
# ==============================================================================

# Build the Flask app
setup_registry(directory=os.getcwd())

logger = Logger.get_logger(__name__)
logger.info('>>> Creating app')
app = Flask(__name__)

logger.info('>>> Setting registry')
# Setup components/configurations


# Initialize HM

init_flag = False
n_attempts = 5
waiting_time = 5
for idx in range(n_attempts):
    try:
        logger.info('>>> Initializing the Horizontal Matchmaking services')
        hm = UseCaseExpertHorizontalMatchmaking(
            label_manager_config_regr_key="name:data_manager--framework:generic--tags:['ams', 'default', 'labels']--namespace:default",
            resource_manager_config_regr_key="name:data_manager--framework:generic--tags:['experts', 'default', 'ams']--namespace:default",
            labeler_config_regr_key="name:labeler--framework:torch--tags:['all-mpnet-base-v2', 'cosine', 'descr2text', 'sbert']--namespace:unibo",
            matchmaking_config_regr_key="name:matchmaking--framework:generic--tags:['cosine', 'online']--namespace:default"
        )
        hm_edu = EduResourceHorizontalMatchmaking(
            label_manager_config_regr_key="name:data_manager--framework:generic--tags:['ams', 'default', 'labels']--namespace:default",
            resource_manager_config_regr_key="name:data_manager--framework:generic--tags:['edu', 'default', 'ams']--namespace:default",
            labeler_config_regr_key="name:labeler--framework:torch--tags:['all-mpnet-base-v2', 'cosine', 'descr2text', 'sbert']--namespace:unibo",
            matchmaking_config_regr_key="name:matchmaking--framework:generic--tags:['cosine', 'online']--namespace:default"
        )
        hm_paper = PaperHorizontalMatchmaking(
            label_manager_config_regr_key="name:data_manager--framework:generic--tags:['ams', 'default', 'labels']--namespace:default",
            resource_manager_config_regr_key="name:data_manager--framework:generic--tags:['paper', 'default', 'ams']--namespace:default",
            labeler_config_regr_key="name:labeler--framework:torch--tags:['all-mpnet-base-v2', 'cosine', 'descr2text', 'sbert']--namespace:unibo",
            matchmaking_config_regr_key="name:matchmaking--framework:generic--tags:['cosine', 'online']--namespace:default"
        )
        logger.info('<<< Done')

        init_flag = True
        break
    except py2neo.errors.ConnectionUnavailable as e:
        print(e)
        time.sleep(waiting_time)
        continue

if not init_flag:
    raise Exception('Error: failed to establish connection to the AMS')


# ==============================================================================
# Routes (HM)
# ==============================================================================

# Define the main route
# NOTE do not change this route name (it would break Tilde's stuff)
@app.route('/matchmaking', methods=['POST'])
def matchmaking():
    return hm.run_matchmaking(request.form['query'])


# Define a route to display results
# NOTE do not change this route name (it would break Tilde's stuff)
@app.route('/matchmaking_gui', methods=['GET', 'POST'])
def matchmaking_gui():
    resource_list = None
    query = None
    if request.method == 'POST':
        query = request.form['query']
        resource_list = hm.run_matchmaking(query)
        resource_list = resource_list['assets']
    return render_template('hm_matchmaking_gui.html',
                           resource_list=resource_list, query=query)


# A route to run the labeling service
# NOTE do not change this route name (it would break Tilde's stuff)
@app.route('/labeling', methods=['POST'])
def labeling():
    return hm.run_labeling(request.form['query'])


# Define a route to display labels
# NOTE do not change this route name (it would break Tilde's stuff)
@app.route('/labeling_gui', methods=['GET', 'POST'])
def labeling_gui():
    label_list = None
    query = None
    if request.method == 'POST':
        query = request.form['query']
        label_list = hm.run_labeling(query)
        label_list = label_list['labels']
    return render_template('hm_labeling_gui.html',
                           label_list=label_list, query=query)


@app.route('/hm_asset_expert_gui', methods=['GET', 'POST'])
def hm_asset_expert_gui():
    # Access control ========================================================
    token = request.args.get('token', default=None, type=str)
    # If no token is passed, deny access
    if token is None:
        return render_template('hm_access_denied.html')
    # Determine whether the token is valid
    access_granted, content = hm.asset_expert_access_control(token)
    # If the token is invalid, deny access
    if not access_granted:
        return render_template('hm_access_denied.html')
    # Retrieve the expert ID
    expert_id = content['uuid identifier']

    # Handle requests =======================================================
    # Add a new expert of update the AMS entry
    if request.method == 'POST':
        res = hm.asset_expert_post(expert_id, request, ssac_expert_id=token, ssac_content=content)
    else:
        res = hm.asset_expert_get(expert_id, request, ssac_expert_id=token, ssac_content=content)

    # Display the GUI =======================================================
    # Get available country options from the AMS
    country_options = sorted(hm.get_country_options())
    # Get available language options from the AMS
    # language_options = [[t['desc'], t['label']] for t in hm.get_language_options()]
    # language_options = sorted(language_options, key=lambda t: t[1])
    language_options = sorted(hm.get_language_options())
    # Determine the action link
    action = '/hm_asset_expert_gui' + f'?token={token}' if token is not None else ''

    # print('=' * 78)
    # print(res['explanation_list'])
    # print('=' * 78)

    # Return the rendered template
    return render_template('hm_asset_expert_gui.html',
                           action=action,
                           country_options=country_options,
                           language_options=language_options,
                           expert_id=expert_id,
                           name=res['name'],
                           price=res['price'],
                           description=res['description'],
                           country=res['country'],
                           languages=res['languages'],
                           team_size=res['team_size'],
                           max_length=500,
                           label_list=res['label_list'],
                           explanation_list=res['explanation_list']
                           )


@app.route('/hm_asset_expert', methods=['GET', 'POST'])
def hm_asset_expert():
    # Access control ========================================================
    token = request.args.get('token', default=None, type=str)
    # If no token is passed, deny access
    if token is None:
        return render_template('hm_access_denied.html')
    # Determine whether the token is valid
    access_granted, content = hm.asset_expert_access_control(token)
    # If the token is invalid, deny access
    if not access_granted:
        return jsonify({'msg', 'Invalid of missing access token'}), 403

    # Handle request ========================================================
    if request.method == 'POST':
        # Add a new expert of update the AMS entry
        res = hm.asset_expert_post(request)
    else:
        # Retrieve data about the expert
        res = hm.asset_expert_get(request)
    # Return results
    return jsonify(res), 200


# use case to expert matchmaking (GUI version)
@app.route('/hm_query_usecase_gui', methods=['GET', 'POST'])
def hm_query_usecase_gui():
    # Access control ========================================================
    token = request.args.get('token', default=None, type=str)
    # If no token is passed, deny access
    if token is None:
        return render_template('hm_access_denied.html')
    # Determine whether the token is valid
    access_granted, content = hm.query_usecase_access_control(token)
    # If the token is invalid, deny access
    if not access_granted:
        return render_template('hm_access_denied.html')
    # Retrieve the usecase ID
    usecase_id = token

    # Handle requests =======================================================
    # Retrieve or store use case data
    if request.method == 'POST':
        res = hm.query_usecase_post(usecase_id, request)
    else:
        res = hm.query_usecase_get(usecase_id, request)

    # Display the GUI =======================================================
    # Get available country options from the AMS
    country_options = sorted(hm.get_country_options())
    # Get available language options from the AMS
    # language_options = [[t['desc'], t['label']] for t in hm.get_language_options()]
    # language_options = sorted(language_options, key=lambda t: t[1])
    language_options = sorted(hm.get_language_options())
    # Determine the action link
    action = '/hm_query_usecase_gui' + f'?token={token}' if token is not None else ''

    # Convert the explanation lists into a sorterd lists of tuples
    explanation_list = []
    using_explanations = False
    if res['explanation_list'] is not None:
        using_explanations = True
        explanation_list = [sorted([t for t in d.items()], key=lambda t: t[1], reverse=True)
                            for d in res['explanation_list']]

    # Prioritize selected assets
    asset_list = []
    if res['asset_list'] is not None:
        sel_ids = set(res['selected_assets'])
        tmp_selected_assets, tmp_selected_assets_xpl = [], []
        tmp_other_assets, tmp_other_assets_xpl = [], []
        for pos, asset in enumerate(res['asset_list']):
            if asset['id'] in sel_ids:
                tmp_selected_assets.append(asset)
                if using_explanations:
                    tmp_selected_assets_xpl.append(explanation_list[pos])
                asset['selected'] = True
            else:
                tmp_other_assets.append(asset)
                if using_explanations:
                    tmp_other_assets_xpl.append(explanation_list[pos])
                asset['selected'] = False
        asset_list = tmp_selected_assets + tmp_other_assets
        if using_explanations:
            explanation_list = tmp_selected_assets_xpl + tmp_other_assets_xpl

    return render_template('hm_query_usecase_gui.html',
                           action=action,
                           language_options=language_options,
                           label_options=[(idx, name) for idx, name in hm.label_options.items()],
                           name=res['name'],
                           description=res['description'] if res['description'] is not None else '',
                           team_size_min=res['team_size_min'] if res['team_size_min'] is not None else 0,
                           team_size_min_importance=res['team_size_min_importance'] if res['team_size_min_importance'] is not None else 0,
                           ai_readiness=res['ai_readiness'] if res['ai_readiness'] is not None else 0,
                           languages=res['languages'] if res['languages'] is not None else [],
                           languages_importance=res['languages_importance'] if res['languages_importance'] is not None else [],
                           emph_labels=res['emph_labels'] if res['emph_labels'] is not None else [],
                           max_length=500,
                           asset_list=asset_list,
                           explanation_list=explanation_list,
                           feedback=res['feedback'])

# use case to expert matchmaking (GUI version)
@app.route('/hm_edu_gui', methods=['GET', 'POST'])
def hm_edu_gui():
    # If called in POST mode, retrieve a list of ranked assets
    res = {'description': 'write your description here'}
    if request.method == 'POST':
        res = hm_edu.edu_post(request)

    # Display the GUI =======================================================
    # Determine the action link
    action = '/hm_edu_gui'

    # Retrieve the list of assets
    asset_list = []
    if 'asset_list' in res:
        asset_list = res['asset_list']
    
    # Retrive the list of explanations
    using_explanations = False
    explanation_list = []
    if 'explanation_list' in res and res['explanation_list'] is not None:
        using_explanations = True
        explanation_list = [sorted([t for t in d.items()], key=lambda t: t[1], reverse=True)
                            for d in res['explanation_list']]

    return render_template('hm_edu_gui.html',
                           action=action,
                           description=res['description'] if res['description'] is not None else '',
                           max_length=500,
                           asset_list=asset_list,
                           explanation_list=explanation_list)


# use case to expert matchmaking (GUI version)
@app.route('/hm_paper_gui', methods=['GET', 'POST'])
def hm_paper_gui():
    # If called in POST mode, retrieve a list of ranked assets
    res = {'description': 'write your description here'}
    if request.method == 'POST':
        res = hm_paper.paper_post(request)

    # Display the GUI =======================================================
    # Determine the action link
    action = '/hm_paper_gui'

    # Retrieve the list of assets
    asset_list = []
    if 'asset_list' in res:
        asset_list = res['asset_list']
    
    # Retrive the list of explanations
    using_explanations = False
    explanation_list = []
    if 'explanation_list' in res and res['explanation_list'] is not None:
        using_explanations = True
        explanation_list = [sorted([t for t in d.items()], key=lambda t: t[1], reverse=True)
                            for d in res['explanation_list']]

    return render_template('hm_paper_gui.html',
                           action=action,
                           description=res['description'] if res['description'] is not None else '',
                           max_length=500,
                           asset_list=asset_list,
                           explanation_list=explanation_list)


if __name__ == "__main__":
    app.run()
