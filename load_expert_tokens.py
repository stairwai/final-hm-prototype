import os
from util import ssac
from doc_api.ams_api.StairwAIDriver import Driver
from argparse import ArgumentParser
from core.commands import setup_registry
import glob
import time
import json
import shutil
import ntpath

if __name__ == '__main__':
    setup_registry(directory=os.getcwd())

    parser = ArgumentParser()
    parser.add_argument('--source', '-s', default='ai-experts-2024-02-tokens', type=str)
    args = parser.parse_args()

    ep_dir = os.path.join('data', 'ssac', 'asset_expert')

    # Retrieve list of experts
    driver = Driver()
    ai_experts = driver.get_aiexperts()
    uuid_map = {}
    for edata in ai_experts:
        if 'name' in edata and 'uuid identifier' in edata:
            uuid_map[edata['name']] = edata['uuid identifier']

    # Loop over all the stored expert SSAC files
    ep_dir_glob = os.path.join('data', 'resources', args.source, '*.json')
    lines = []
    for fname in glob.glob(ep_dir_glob):
        # Retrieve expert data from the SSAC file
        to_load = False
        with open(fname) as fp:
            # Retrive SSAC data for this expert
            edata = json.load(fp)
            # Retrive name
            ename = edata['name'] if 'name' in edata else None
            if ename in uuid_map:
                # Clear the UUID field
                if 'uuid identifier' in edata:
                    del edata['uuid identifier']
                elif 'swai__uuid_identifier' in edata:
                    del edata['swai__uuid_identifier']
                # Write the new UUID
                edata['uuid identifier'] = uuid_map[ename]
                # Prepare a line to write to the token file
                if 'uuid identifier' in edata:
                    euuid = edata['uuid identifier']
                    token, _ = os.path.splitext(ntpath.basename(fname))
                    lines.append(f'{ename},{euuid},{token},http://127.0.0.1:5000/hm_asset_expert_gui?token={token}\n')
                    # Mark the token as to load
                    to_load = True
        if to_load:
            # Write back the changes
            ofname = os.path.join('data', 'ssac', 'asset_expert', ntpath.basename(fname))
            with open(ofname, 'w') as fp:
                json.dump(edata, fp)

    # Write all tokens to the token file
    token_fname = os.path.join('data', 'expert_tokens.csv')
    with open(token_fname, 'w') as fp:
        fp.writelines(lines)
