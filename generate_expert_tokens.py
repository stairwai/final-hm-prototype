import os
from util import ssac
from doc_api.ams_api.StairwAIDriver import Driver
from argparse import ArgumentParser
from core.commands import setup_registry

if __name__ == '__main__':
    setup_registry(directory=os.getcwd())

    parser = ArgumentParser()
    parser.add_argument('--reset', '-r', default=False, type=bool)
    args = parser.parse_args()

    reset = args.reset

    ep_dir = os.path.join('data', 'ssac', 'asset_expert')

    # If requested, clean all existing token files
    if reset:
        ssac.clean_files(ep_dir, only_test_files=False)

    # Retrieve list of experts
    driver = Driver()
    ai_experts = driver.get_aiexperts()
    expert_data = [edata for edata in ai_experts]

    # Generate tokens and related files
    expert_metadata = [{'uuid identifier': edata['uuid identifier']} for edata in expert_data]
    generated_ids = ssac.generate_storage_files(metadata=expert_metadata, directory=ep_dir, testing=False)

    # Write token links
    for idx in generated_ids:
        print(f'http://127.0.0.1:5000/hm_asset_expert_gui?token={str(idx)}')

    # write all the data to a file
    lines = []
    for edata, idx in zip(expert_data, generated_ids):
        lines.append(f'{edata["name"]},{edata["uuid identifier"]},{idx},http://127.0.0.1:5000/hm_asset_expert_gui?token={str(idx)}\n')

    mode = 'w' if reset else 'a'
    fname = os.path.join('data', 'expert_tokens.csv')
    with open(fname, mode) as fp:
        fp.writelines(lines)
