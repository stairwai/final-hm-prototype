#!/bin/sh

# Indet the list of experts
# python3 indexer_ams.py

# Index the all the list of resources
python3 indexer_ams.py --tags all-mpnet-base-v2 ams cosine descr2text experts sbert
python3 indexer_ams.py --tags all-mpnet-base-v2 ams cosine descr2text edu sbert
python3 indexer_ams.py --tags all-mpnet-base-v2 ams cosine descr2text paper sbert
