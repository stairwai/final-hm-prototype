import os
import errno
from datetime import datetime, timedelta
from threading import Thread
from time import sleep
from .StairwAIDriver import Driver


def get_env_var(name, default=None):
    if name in os.environ:
        return os.environ[name]
    else:
        return default


class BackupTemporizer(Thread):
    def __init__(self, hora, delay, path):
        super(BackupTemporizer, self).__init__()
        self._state = True
        self.hour = hora
        self.delay = delay
        self.path = path
        self.api = Driver()

    def stop(self):
        self._state = False

    def run(self):
        aux = datetime.strptime(self.hour, '%H:%M:%S')
        hour = datetime.now()
        hour = hour.replace(hour=aux.hour, minute=aux.minute, second=aux.second, microsecond=0)
        if hour <= datetime.now():
            hour += timedelta(days=1)

        while self._state:
            if hour <= datetime.now():
                path2 = self.path + hour.strftime("%Y-%m-%d-%H:%M")
                try:
                    os.mkdir(path2)
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        raise
                # call the backup function
                self.api.create_backup(path=path2)
                print('Backup scheduled executed, {0} at {1}'.format(hour.date(), hour.time()))
                hour += timedelta(days=1)
                print('Next backup scheduled to {0} at {1}'.format(hour.date(), hour.time()))
            sleep(self.delay)
