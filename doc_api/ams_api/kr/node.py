from .getters import *
import uuid
from py2neo import Relationship
from .NeoDriver import NeoDriver


def insert_node(driver: NeoDriver, name, node_class):
    """
    Method to insert nodes in the AMS.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param name: name of the instance which you want to add.
    :param node_class: class of the node you want to insert
    """
    swaiPrefix = "swai__"

    # ERROR CONTROL #
    # if is not instantiable
    if node_class not in get_instantiable_classes(driver):
        raise ValueError(
            "{0} class instances cannot be instantiated or deleted, only instantiable classes can.".format(
                node_class))
    # if already exist
    if if_node_exist_by_name(driver, name, node_class):
        raise ValueError(
            'Insertion cancelled, the following instance is already in the Database: ' + node_class)

    # NODE INSERTION #
    # get the database name of the class specified as a parameter
    db_class_node = driver.class_owl.loc[driver.class_owl["label"] == node_class].values[0][1]
    # generate the uuid for the node
    str_uuid = str(uuid.uuid1())
    # run the command to add the new instance
    node_label = f"Resource:owl__NamedIndividual:{swaiPrefix}{db_class_node}"
    query = f"""
    CREATE (n:{node_label} {{rdfs__label: $name, swai__uri: $uri}})
    SET n.`{swaiPrefix}name` = $name, n.swai__uuidIdentifier = $suuid
    RETURN n
    """
    node = driver.graph.run(query, {"name": name, "uri": driver.uri + db_class_node + '%' + name.replace(' ', ''), "suuid": str_uuid}).data()[0]['n']
    # insert identifier parameter
    node["swai__identifier"] = node.identity
    driver.graph.push(node)

    # set the relation "instance of" between the new node and its class
    class_nodes = driver.graph.nodes.match('owl__Class', rdfs__label=node_class).all()
    class_node = class_nodes.pop(0)
    if len(class_nodes) > 0:
        for cls_node in class_nodes:
            if driver.prefixes['swai'] in cls_node['uri']:
                class_node = cls_node

    driver.graph.create(Relationship(node, "owl__instanceOf", class_node))

    # IDENTIFIER FAIL CONTROL#
    if node.identity == 0:
        delete_node(driver, node_id=node.identity)
        return insert_node(driver, name, node_class)

    return node


def delete_node(driver: NeoDriver, node_id: int):
    """
    Method to delete nodes in the AMS.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param node_id: identifier for the node
    """
    # ERROR CONTROL #
    # if already exist
    if not if_node_exist_by_id(driver, node_id):
        raise ValueError(
            'Deletion cancelled, the following instance ID does not belongs to any instance in the Database: ' + str(
                node_id))
    # if node it is not an instance
    node_var = get_node_by_id(driver, node_id)
    if 'owl__NamedIndividual' not in str(node_var.labels):
        raise ValueError("The node ID {0} does not belong to an instance node.".format(str(node_id)))
    # if is not instantiable
    node_class = driver.graph.match([node_var], r_type='owl__instanceOf').first().end_node['rdfs__label']
    if node_class not in get_instantiable_classes(driver):
        raise ValueError(
            "{0} class instances cannot be instantiated or deleted, only instantiable classes can.".format(
                node_class))

    # Delete the node and all the related relationships
    driver.graph.run("MATCH (n) WHERE ID(n)={0} DETACH DELETE n".format(str(node_id)))


def if_node_exist_by_name(driver: NeoDriver, name, db_class):
    """
    Method that returns True if a node exists and false if not. Using the node name and class
    :param driver: Driver object which contains the connection to the database and the static variables
    :param name: name of the instance which you want to add.
    :param db_class: class of the node you want to insert
    """
    # get the node by its id, if it exists, raise and exception
    return True if get_node_by_name(driver, name, db_class) is not None else False


def if_node_exist_by_id(driver: NeoDriver, node_id):
    """
    Method that returns True if a node exists and false if not. Using the node name and class
    :param driver: :param driver: Driver object which contains the connection to the database and the static variables
    :param node_id: node Identifier
    """
    # get the node by its id, if it exists, raise and exception
    return True if get_node_by_id(driver, node_id) is not None else False
