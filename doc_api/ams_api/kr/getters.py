from .miscellaneous import *
from .NeoDriver import NeoDriver


# GETTERS
def get_classes(driver: NeoDriver, with_descriptions=False):
    """
    Getter of the ontology defined classes, it returns all the defined owl__Class in the DB including
    non-instantiable and extra classes.
    :param driver: Graph database connection using py2neo library.
    :param with_descriptions: Returns the instances with its descriptions if True
    :return: a list of the defined classes.
    """
    if with_descriptions:
        return {label: driver.graph.nodes.match('owl__Class', rdfs__label=label).first()['rdfs__comment'] for label in
                driver.class_owl["label"].to_list()}
    return driver.class_owl["label"].to_list()


def get_instantiable_classes(driver: NeoDriver, with_descriptions: bool = False):
    """
    Getter of the ontology instantiable classes with its definitions if is necessary
    :param driver:Graph database connection using py2neo library.
    :param with_descriptions: the output has the description associated labels included
    :return: a list, or dictionary, of the available classes
    """
    if with_descriptions:
        return {label: driver.graph.nodes.match('owl__Class', rdfs__label=label).first()['rdfs__comment'] for label in
                driver.insta_classes}
    return driver.insta_classes


def get_dbclass_prefix(driver: NeoDriver, class_label: str):
    """
    Getter of the database name and the prefix associated to it
    :param driver:Graph database connection using py2neo library.
    :param class_label: class name.
    :return: database name and the prefix associated
    """
    if class_label not in get_classes(driver):
        raise ValueError("{0} class its not in the database.".format(class_label))

    db_name = driver.class_owl.loc[driver.class_owl["label"] == class_label].values[0][1]
    nodes = driver.graph.nodes.match('owl__Class', rdfs__label=class_label).all()
    if len(nodes) == 0:
        raise NameError("Trying to find the prefix of a non existent class")
    elif len(nodes) == 1:
        node = nodes[0]
    else:
        # if there are more than one class with the same exact name, prioritize StairwAI specified term
        node = None
        for n in nodes:
            if driver.prefixes['swai'] in n['uri']:
                node = n
    inv_prefix_dict = {uri: prefix for prefix, uri in driver.prefixes.items()}
    try:
        prefix = inv_prefix_dict[str(node['uri']).split('#')[0] + '#']
    except Exception:
        return db_name, None
    return db_name, prefix


def get_all_instances_by_class(driver: NeoDriver, class_label: str, only_labels: bool = True):
    """
    This query returns all the instances of a specific class.
    :param driver: Graph database connection using py2neo library.
    :param class_label: class name.
    :param only_labels: If True, the result would be the list of all the instances labels. If False, the result would be
     the list of all the nodes of the class
    """
    cls, prx = get_dbclass_prefix(driver, class_label)
    onto_name = prx+'__'+cls
    query = f"MATCH (n:owl__Class)<-[:owl__instanceOf]-(target:owl__NamedIndividual:{onto_name}) WHERE n.rdfs__label = '{class_label}' RETURN target"
    # get all the instances by its class, and flatten them as a plain list of nodes
    nodes = [node_dict['target'] for node_dict in driver.graph.run(query).data()]
    # if to_dict is True, return the node as a dictionary
    return [node["rdfs__label"] if only_labels else node for node in nodes]


def get_node_by_uuid(driver: NeoDriver, uuid: str, class_label: str, to_dict: bool = False):
    """
    This query returns an instance, if it exists, based on its name and the class.
    :param driver: Graph database connection using py2neo library.
    :param uuid: UUID identifier of the instance searched.
    :param class_label: class of the instance searched.
    :param to_dict: whether the result must be returned as a dictionary (True) or py2neo.Node instance (False).
    :return: Instance data or instance node, if it exists.
    """
    cls, prx = get_dbclass_prefix(driver, class_label)
    onto_name = prx + '__' + cls
    query = f"MATCH (target:owl__NamedIndividual:{onto_name})-[:owl__instanceOf]->(n:owl__Class) WHERE n.rdfs__label = '{class_label}' AND target.swai__uuidIdentifier = '{uuid}' RETURN target"
    # get all the instances by its class, and flatten them as a plain list of nodes
    try:
        node = driver.graph.run(query).data()[0]['target']
    except Exception:
        node = None
    # if to_dict is True, return the node as a dictionary
    return node_data_to_dict(driver, node) if to_dict else node


def get_node_by_name(driver: NeoDriver, instance_name: str, class_label: str, to_dict: bool = False):
    """
    This query returns an instance, if it exists, based on its name and the class.
    :param driver: Graph database connection using py2neo library.
    :param instance_name: Name/label of the instance searched.
    :param class_label: class of the instance searched.
    :param to_dict: whether the result must be returned as a dictionary (True) or py2neo.Node instance (False).
    :return: Instance data or instance node, if it exists.
    """
    # if instance_name is an AI technique or business area, search the specific class node
    if instance_name in driver.technique_hierarchy + driver.business_hierarchy:
        cls, prx = get_dbclass_prefix(driver, instance_name)
    else:
        cls, prx = get_dbclass_prefix(driver, class_label)
    # get all the instances by its class, and flatten them as a plain list of nodes
    label = prx + '__' + cls
    query = f"MATCH (target:owl__NamedIndividual:`{label}`)-[:owl__instanceOf]->(n:owl__Class) " \
            f"WHERE n.rdfs__label = $c AND target.rdfs__label = $u RETURN target"
    try:
        node = driver.graph.run(query, {'c': class_label, 'u': instance_name}).data()[0]['target']
    except Exception:
        node = None
    # if to_dict is True, return the node as a dictionary
    return node_data_to_dict(driver, node) if to_dict else node


def get_node_by_id(driver: NeoDriver, neo_id: int, to_dict=False):
    """
    This query returns the node, if it exists, given its identifier.
    :param driver: Graph database connection using py2neo library.
    :param neo_id: id of the instance searched.
    :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
    :return: Instance data or instance node, if it exists.
    """
    query = """MATCH (n) WHERE ID(n) = {neo_id} AND  ('owl__Class' IN labels(n) OR 'owl__NamedIndividual' IN labels(n)) RETURN n""".format(
        neo_id=str(neo_id))
    # query an instance to the database using its identifier
    try:
        node = driver.graph.run(query).data()[0]['n']
    except Exception:
        node = None
    return node_data_to_dict(driver, node) if to_dict else node


def get_id_from_label_relation(driver: NeoDriver, label, relation):
    """
    This method returns the id of a node given its label, if it exists in the range of a specific relation.
    :param driver: Graph database connection using py2neo library.
    :param label: name of the instance.
    :param relation: relation in which range there have to be the instance with the name specified in the label.
    :return: The id of the instance searched, if exists.
    """
    # get the classes in the range of rel
    if relation in ['applicable to', 'apply', 'experience in', 'expertise in', 'have interest in', 'operates in', 'related to', 'scored in', 'useful for']:
        range_cls = [label]
    else:
        range_cls = driver.properties.loc[driver.properties["label"] == relation]["range"].values[0]

    # for each class range search if the instance exists
    id_res = None
    for rg_cls in range_cls:
        node = get_node_by_name(driver=driver, instance_name=label, class_label=rg_cls)
        if node is not None:
            id_res = node['swai__identifier']
    return id_res
