import py2neo

from .NeoDriver import NeoDriver


# MISCELLANEOUS FUNCTIONS
def node_data_to_dict(driver: NeoDriver, node: py2neo.Node):
    """
    This query returns the node data as a dictionary.
    :param driver:Graph database connection using py2neo library.
    :param node: py2neo.Node instance.
    :return: Node data as a dictionary, if node = None, returns None.
    """
    if node is None:
        return node
    # Translate directly the node attributes
    res = dict(node)
    # Prepare data structures for some special relations
    scores = {}
    sc_rel = ''
    # check if it is a dirty node
    try:
        subgraph = driver.graph.match(nodes=(node,)).all()
    except:
        raise ValueError("The node does not belong to this graph.")
    # Translate all relations
    for r in subgraph:
        if str(r.__class__.__name__) in ['swai__score', 'swai__scoredIn']:
            scores[r.end_node["rdfs__label"]] = r['score']
            sc_rel = str(r.__class__.__name__)
            # scores_id[r.end_node["swai__identifier"]] = r['score']
        elif str(r.__class__.__name__) == 'owl__instanceOf':
            res['owl__instanceOf'] = r.end_node["rdfs__label"]
        else:
            field_name = str(r.__class__.__name__)
            # if new label, insert
            if field_name not in res.keys():
                res[field_name] = r.end_node["rdfs__label"]
            else:
                if isinstance(res[field_name], list):
                    res[field_name].append(r.end_node["rdfs__label"])
                else:
                    aux = res[field_name]
                    res[field_name] = [aux, r.end_node["rdfs__label"]]
    # Store the special relations if there are any, the label can be 'swai__score' or 'swai__scoredIn'
    if len(scores.keys()) > 0:
        res[sc_rel] = scores
    res2 = {}
    for db_k in res.keys():
        onto_name = db_k if db_k == 'uri' else str(db_k).split('__')[1]
        # extract database internal properties from the result
        if onto_name in ['label', 'ontologyName', 'comment', 'subClassOf', 'uri']:
            pass
        elif onto_name == 'instanceOf':
            res2['class'] = res[db_k]
        else:
            res2[driver.properties.loc[driver.properties["db_name"] == onto_name]['label'].values[0]] = res[db_k]
    return res2


def extract_class_properties(driver: NeoDriver, name_class: str):
    """
    This method returns a dictionary with the properties, and its definitions, of a specific type of class.
    :param driver:Graph database connection using py2neo library.
    :param name_class: the name (label) of the class from which to extract the properties.
    :return: A dictionary with the properties available for a specific class and its requirements.
    """
    if name_class not in driver.class_owl["label"].to_list():
        raise ValueError("Trying to extract properties from non-existent class.")
    # set the result array
    properties = {'Attributes': {'def': '', 'properties': []}, 'Relations': {'def': '', 'properties': []}}
    # property definitions insertion
    properties['Attributes'][
        'def'] = "Inherent features of each instance. Only one attribute of each type can exist per instance and the data type of each attribute is defined in its 'range' parameter."
    properties['Relations'][
        'def'] = "Connections between already existing instances in the database. More than one relation of each type can exist per instance and the 'range' parameter of each relation defines the class which the relation is used to connect with."
    for idx, proper in driver.properties.iterrows():
        # Attributes
        if proper['type'] == 'DatatypeProperty' and name_class in proper['domain']:
            rel_node = driver.graph.nodes.match('owl__DatatypeProperty', rdfs__label=proper['label']).first()
            rng = driver.graph.match((rel_node,), r_type='rdfs__range').first().nodes[1]['uri'].split('#')[1]
            properties['Attributes']['properties'].append(
                {'property': proper['label'], 'range': rng,
                 'def': rel_node['rdfs__comment']})
        # Relations
        elif proper['type'] == 'ObjectProperty' and name_class in proper['domain']:
            rel_node = driver.graph.nodes.match('owl__ObjectProperty', rdfs__label=proper['label']).first()
            rel = driver.graph.match((rel_node,), r_type='rdfs__range').first()
            # avoid hasPart and isPartOf general relations
            if rel is not None:
                properties['Relations']['properties'].append(
                    {'property': proper['label'], 'range': proper['range'],
                     'def': rel_node['rdfs__comment']})
    return properties


def all_database_attributes(driver: NeoDriver, with_conditions: bool):
    """
    Getter of the ontology defined attributes.
    :param driver: Graph database connection using py2neo library.
    :param with_conditions: whether the method must return the domain of each property.
    :return: If with_conditions is True, a pandas data frame with the properties and its domain. Otherwise, it will
    return the list of available attributes.
    """
    # get the list of all the attributes in the database
    prop = driver.properties.loc[driver.properties["type"] == 'DatatypeProperty'].reset_index()
    # returning a list of names if with_conditions false and returning more properties if it is true
    return prop[["label", "domain"]] if with_conditions else prop['label'].to_list()


def all_database_relations(driver: NeoDriver, with_conditions: bool):
    """
    Getter of the ontology defined relations.
    :param driver: Graph database connection using py2neo library.
    :param with_conditions: whether the method must return the domain/range of relation.
    :return: If with_conditions is True, a pandas' data frame with the relations and its domain/range. Otherwise, it
    will return the list of available relations.
    """
    # get the list of all the relations in the database
    rel = driver.properties.loc[driver.properties["type"] == 'ObjectProperty'].reset_index()
    # returning a list of names if with_conditions false and returning more properties if it is true
    return rel[["label", "domain", "range"]] if with_conditions else rel['label'].to_list()


def inference_class_subclasses(driver: NeoDriver, class_label: str):
    """
    This method is used to do a recursive search through the ontology hierarchy with the aim of finding the children
    classes of each class in a set of classes.
    :param driver:Graph database connection using py2neo library.
    :param class_label: label of the class from which we want the subclasses.
    :return: a set of classes composed by the original classes and the new ones.
    """
    classes = driver.graph.run(
        "MATCH (n:owl__Class {rdfs__label: $topic_name})<-[r:rdfs__subClassOf*]-(child:owl__Class)"
        "RETURN child.rdfs__label", topic_name=str(class_label)).data()
    if len(classes) > 0:
        res = [elem['child.rdfs__label'] for elem in classes]
        res.append(class_label)
        return res
    return [class_label]


def id_description_by_class(driver: NeoDriver, class_label: str):
    """
    This method is used to get the description and the id from a class
    :param driver: Graph database connection using py2neo library.
    :param class_label: label of the class.
    :return: class identifier and class description
    """
    node = driver.graph.nodes.match('owl__Class', rdfs__label=class_label).first()
    return {'label': node["rdfs__label"], 'desc': node['rdfs__comment'], 'id': node["swai__identifier"]}
