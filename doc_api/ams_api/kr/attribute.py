from .NeoDriver import NeoDriver
from .getters import *
from .node import *


def insert_attribute(driver: NeoDriver, node_id, attribute: str, value):
    """
    Insert a node attribute from the AMS.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param node_id: Identifier of the instance (node) from which you want to insert an attribute.
    :param attribute: attribute type. Must be a relation defined in the ontology.
    :param value: value to insert in the specified attribute
    """
    # ERROR CONTROL #
    # if the node from which the attribute wants to be removed does not exist
    if not if_node_exist_by_id(driver, node_id):
        raise ValueError(
            'Attribute insertion is cancelled, the following instance ID does not belongs to any instance in the Database: ' + str(
                node_id))

    # if is not instantiable
    start_node = (get_node_by_id(driver, node_id))
    node_class = driver.graph.match([start_node], r_type='owl__instanceOf').first().end_node['rdfs__label']
    if node_class not in get_instantiable_classes(driver):
        raise Exception(
            "{0} class instances cannot be modified, only instantiable classes can.".format(
                node_class))

    # check if the attribute type exist
    if attribute not in driver.properties["label"].values:
        raise ValueError("The attribute type: " + attribute + " does not exist in the AMS")

    # if the attribute is in the node class domain
    df_row = driver.properties.loc[driver.properties["label"] == attribute]
    if node_class not in df_row["domain"].to_list()[0]:
        raise ValueError(node_class + " class it's not in the domain of the attribute: " + attribute)

    # if id or uuid wanted to be deleted
    if attribute in ['identifier']:
        raise ValueError("{0} is a primary attribute it can't be modified.".format(attribute))

    # if name is the attribute that is going to be modified, check if it already exists an instance with the same name and class.
    if attribute == 'name' and if_node_exist_by_name(driver, value, node_class):
        raise ValueError("{0} is already a name for an {1} instance. The renaming can't be done".format(value, node_class))

    # if value is None
    if value is None:
        raise ValueError("The attribute {0} from {1} requires a value".format(attribute, start_node['rdfs__label']))

    # ATTRIBUTE INSERTION #
    start_node["swai__" + df_row["db_name"].values[0]] = value
    driver.graph.push(start_node)


def delete_attribute(driver: NeoDriver, node_id, attribute: str):
    """
    Delete a node attribute from the AMS.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param node_id: Identifier of the instance (node) from which you want to delete an attribute.
    :param attribute: attribute type. Must be a relation defined in the ontology.
    """
    # ERROR CONTROL #
    # if the node from which the attribute wants to be removed does not exist
    if not if_node_exist_by_id(driver, node_id):
        raise ValueError(
            'Attribute deletion cancelled, the following instance ID does not belongs to any instance in the Database: ' + str(
                node_id))

    # if is not instantiable
    start_node = (get_node_by_id(driver, node_id))
    node_class = driver.graph.match([start_node], r_type='owl__instanceOf').first().end_node['rdfs__label']
    if node_class not in get_instantiable_classes(driver):
        raise Exception(
            "{0} class instances cannot be modified, only instantiable classes can.".format(
                node_class))

    # check if the attribute type exist
    if attribute not in driver.properties["label"].values:
        raise ValueError("The attribute type: " + attribute + " does not exist in the AMS")

    # if id, uuid or name wants to be deleted
    if attribute in ['name', 'identifier', 'uuid_identifier']:
        raise ValueError("{0} is a primary attribute it can't be deleted.".format(attribute))

    # check if the attribute is already specified in order to be deleted
    df_row = driver.properties.loc[driver.properties["label"] == attribute]
    if 'swai__' + df_row["db_name"].values[0] not in start_node.keys():
        raise ValueError("You are trying to delete a non specified attribute ({0}) for the node with the ID = {1}.".format(attribute, str(node_id)))

    # ATTRIBUTE DELETION #
    # add the new property into the node, or update in with the new value if it already exists
    del(start_node['swai__' + df_row["db_name"].values[0]])
    driver.graph.push(start_node)
