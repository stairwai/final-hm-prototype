from ..kr.miscellaneous import *
from ..kr.getters import *
from ..kr.NeoDriver import NeoDriver


def get_business_categories(driver: NeoDriver, with_description=False):
    """
    Available business categories' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param with_description: if True it returns the descriptions, additionally to the labels of the business
    categories as a dictionary.
    :return: List of the available Business category instances.
    """
    if type(with_description) is not bool:
        raise ValueError("with_description parameter has to be a boolean.")

    if with_description:
        return [id_description_by_class(driver=driver, class_label=label) for label in driver.business_hierarchy]
    return driver.business_hierarchy


def get_aitechniques(driver: NeoDriver, with_description=False):
    """
    Available AI Techniques' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param with_description: if True it returns the descriptions, additionally to the labels of the business
    categories as a dictionary.
    :return: List of the available AI Technique instances.
    """
    if type(with_description) is not bool:
        raise ValueError("with_description parameter has to be a boolean.")

    if with_description:
        return [id_description_by_class(driver=driver, class_label=label) for label in driver.technique_hierarchy]
    return driver.technique_hierarchy


def get_aiexperts(driver: NeoDriver, uuid_identifier=None, swai_id_only=False):
    if type(uuid_identifier) not in [str, type(None)] or uuid_identifier == "":
        raise ValueError("uuid_identifier parameter has to be a non-empty String or None.")
    if type(swai_id_only) is not bool:
        raise ValueError("swai_id_only parameter has to be a boolean.")

    # Retrieve the required data
    if uuid_identifier is None:
        # In this care, retrieve the full expert list
        res_n = get_all_instances_by_class(driver=driver, class_label='AI expert', only_labels = False)
        res = [node_data_to_dict(driver=driver, node=node) for node in res_n]
        # If required, select just the IDs
        if swai_id_only:
            res = [n['swai__identifier'] for n in res]
    else:
        # In this case, retrive a single node
        res = get_node_by_uuid(driver=driver, uuid=uuid_identifier, class_label='AI expert',
                               to_dict=not swai_id_only)
        # If required, select just the IDs
        if res is None:
            raise ValueError("The UUID parameter does not correspond to any database instance.")
        if swai_id_only:
            res = res['swai__identifier']
    # Return results
    return res


def get_educational_resources(driver: NeoDriver, uuid_identifier=None, swai_id_only=False):
    if type(uuid_identifier) not in [str, type(None)] or uuid_identifier == "":
        raise ValueError("uuid_identifier parameter has to be a non-empty String or None.")
    if type(swai_id_only) is not bool:
        raise ValueError("swai_id_only parameter has to be a boolean.")

    # Retrieve the required data
    if uuid_identifier is None:
        # In this care, retrieve the full expert list
        res_n = get_all_instances_by_class(driver=driver, class_label='Educational Resource', only_labels=False)
        res = [node_data_to_dict(driver=driver, node=node) for node in res_n]
        # If required, select just the IDs
        if swai_id_only:
            res = [n['swai__identifier'] for n in res]
    else:
        # In this case, retrive a single node
        res = get_node_by_uuid(driver=driver, uuid=uuid_identifier, class_label='Educational Resource',
                               to_dict=not swai_id_only)
        if res is None:
            raise ValueError("The UUID parameter does not correspond to any database instance.")
        # If required, select just the IDs
        if swai_id_only:
            res = res['swai__identifier']
    # Return results
    return res


def get_academic_resources(driver: NeoDriver, uuid_identifier=None, swai_id_only=False):
    if type(uuid_identifier) not in [str, type(None)] or uuid_identifier == "":
        raise ValueError("uuid_identifier parameter has to be a non-empty String or None.")
    if type(swai_id_only) is not bool:
        raise ValueError("swai_id_only parameter has to be a boolean.")

    # Retrieve the required data
    if uuid_identifier is None:
        # In this care, retrieve the full expert list
        res_n = get_all_instances_by_class(driver=driver, class_label='Academic Resource', only_labels=False)
        res = [node_data_to_dict(driver=driver, node=node) for node in res_n]
        # If required, select just the IDs
        if swai_id_only:
            res = [n['swai__identifier'] for n in res]
    else:
        # In this case, retrive a single node
        res = get_node_by_uuid(driver=driver, uuid=uuid_identifier, class_label='Academic Resource',
                               to_dict=not swai_id_only)
        if res is None:
            raise ValueError("The UUID parameter does not correspond to any database instance.")
        # If required, select just the IDs
        if swai_id_only:
            res = res['swai__identifier']
    # Return results
    return res


def get_countries(driver: NeoDriver):
    """
    Available countries' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :return: List of the available Country instances.
    """
    return get_all_instances_by_class(driver=driver, class_label='Country')


def get_organisation_types(driver: NeoDriver):
    """
    Available organisation types' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :return: List of the available Organisation type instances.
    """
    return get_all_instances_by_class(driver=driver, class_label='Organisation Type')


def get_containers(driver: NeoDriver):
    """
    Available containers' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :return: List of the available Container instances.
    """
    return get_all_instances_by_class(driver=driver, class_label='Container')


def get_distributions(driver: NeoDriver):
    """
    Available distributions' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :return: List of the available Distribution instances.
    """
    return get_all_instances_by_class(driver=driver, class_label='Distribution')


def get_paces(driver: NeoDriver):
    """
    Available paces' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :return: List of the available Pace instances.
    """
    return get_all_instances_by_class(driver=driver, class_label='Pace')


def get_languages(driver: NeoDriver, with_description: bool = False):
    """
    Available languages' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param with_description: if true, apart from language label it retrieve the language description
    :return: List of the available Language instances.
    """
    if type(with_description) is not bool:
        raise ValueError("with_description parameter has to be a boolean.")

    return get_all_instances_by_class(driver=driver, class_label='Language') if not with_description else [
        {'label': node2["rdfs__label"], 'desc': node2['rdfs__comment']} for node2 in
        get_all_instances_by_class(driver=driver, class_label='Language', only_labels=False)]


def get_education_levels(driver: NeoDriver):
    """
    Available education levels' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :return: List of the available Education level instances.
    """
    return get_all_instances_by_class(driver=driver, class_label='Education level')


def get_education_types(driver: NeoDriver):
    """
    Available education types' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :return: List of the available Education type instances.
    """
    return get_all_instances_by_class(driver=driver, class_label='Education type')


def get_skills(driver: NeoDriver):
    """
    Available skills' getter.
    :param driver: Driver object which contains the connection to the database and the static variables
    :return: List of the available Skill instances.
    """
    return get_all_instances_by_class(driver=driver, class_label='Skill')
