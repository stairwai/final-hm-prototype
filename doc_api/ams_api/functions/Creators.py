from ..kr.attribute import *
from .Queries import *
from ..kr.NeoDriver import NeoDriver
import sys
from collections import Counter
import numbers

import time

def insert_instance(driver: NeoDriver, class_type: str, param_dict: dict):
    """
    This function allows the insertion of new instances into the database. To check the set of available
    instantiable classes, please call get_instantiable_classes() function. Additionally, to know the available
    relations and attributes for the instances of a specific class, call query_get_class_properties(class_type)
    function.
    :param driver: Driver object which contains the connection to the database and the static variable.
    :param class_type: The class of the inserted instance.
    :param param_dict: the instance specific dictionary (relations or attributes) that are going to be modified in
    the AMS. This dictionary would only accept the specific relations/attributes to the class of the node, to check
    it, call query_get_class_properties(class_type) where you could find the relations/attributes for the class, a
    description of them and their range.
    :return: the dictionary with the data of the node inserted.
    """
    if type(class_type) is not str or len(class_type) == 0:
        raise ValueError("class_type parameter has to be a non-empty String.")
    if type(param_dict) is not dict:
        raise ValueError("param_dict parameter has the wrong format, it has to be dictionary.")

    # check that swai__name exist, mandatory field
    if 'name' not in param_dict.keys():
        raise ValueError("'name' field in param_dict is mandatory to insert a new instance.")
    if type(param_dict['name']) is not str or len(param_dict['name']) == 0:
        raise ValueError("'name' field in param_dict has to be a none empty string.")

    # insert the new instance into the database
    try:
        node2 = insert_node(driver, param_dict['name'], class_type)
    except Exception:
        raise sys.exc_info()[1]
    del(param_dict['name'])

    # store the parameters, if error occurs delete the node to avoid adding trash
    try:
        modify_instance(driver=driver, neo_id=node2.identity, add_dict=param_dict)
    except Exception:
        delete_node(driver=driver, node_id=node2.identity)
        raise sys.exc_info()[1]

    return get_node_by_id(driver=driver, neo_id=node2.identity)


def delete_instance(driver: NeoDriver, neo_id: int):
    """
    Method to delete nodes in the AMS.
    :param driver: Driver object which contains the connection to the database and the static variable.
    :param neo_id: identifier of the instance which you want to modify.
    """
    if type(neo_id) is not int or neo_id < 0:
        raise ValueError("neo_id parameter has the wrong format, it has to be an integer and  >= 0.")

    # insert the new instance into the database
    try:
        delete_node(driver, neo_id)
    except Exception:
        raise sys.exc_info()[1]


def modify_instance(driver: NeoDriver, neo_id: int, add_dict: dict = None, delete_dict: dict = None):
    """
    This function allows the modification of any instance node in the database. To check the available instantiable
    classes, call get_instantiable_classes() function. Additionally, to check the available relations and attributes
    for the instances of a specific class, call query_get_class_properties(class_type) function.
    :param driver: Driver object which contains the connection to the database and the static variable.
    :param neo_id: identifier of the instance which is to be modified.
    :param add_dict: the instance specific dictionary (relations or attributes) that are going to be added in
    the AMS. This dictionary would only accept the specific relations/attributes to the class of the node, to check
    it, call query_get_class_properties(class_type) where you could find the relations/attributes for the class, a
    description of them and their range.
    :param delete_dict: the instance specific dictionary (relations or attributes) that are going to be added in
    the AMS. This dictionary would only accept the specific relations/attributes to the class of the node, to check
    it, call query_get_class_properties(class_type) where you could find the relations/attributes for the class, a
    description of them and their range.
    :return: the dictionary with the data of the node modified.
    """
    if type(neo_id) is not int or neo_id < 0:
        raise ValueError("neo_id parameter has the wrong format, it has to be an integer and  >= 0.")
    if type(add_dict) not in [dict, type(None)]:
        raise ValueError("add_dict parameter has the wrong format, it has to be dictionary.")
    if type(delete_dict) not in [dict, type(None)]:
        raise ValueError("delete_dict parameter has the wrong format, it has to be dictionary.")

    # if the starting node from which the relation wants to be inserted does not exist
    if not if_node_exist_by_id(driver, neo_id) or 'owl__NamedIndividual' not in str(
            get_node_by_id(driver, neo_id).labels):
        raise ValueError(
            'Relation insertion is cancelled, the following instance ID does not belongs to any instance in the Database: ' + str(
                neo_id))

    # extract all the possible relation for the class of the node
    if add_dict is None:
        add_dict = {}
    if delete_dict is None:
        delete_dict = {}
    clas = query_class_by_id(driver=driver, neo_id=neo_id)
    rel_all = extract_class_properties(driver=driver, name_class=clas)
    range_all = [a['property'] for a in rel_all['Relations']['properties'] + rel_all['Attributes']['properties']]

    # DELETE PROPERTIES DICTIONARY
    if bool(delete_dict):
        # insert new parameters
        for key, value in delete_dict.items():
            if value is None:
                continue
            if key not in range_all:
                raise ValueError(
                    "The property '{0}' is not a valid relation/attribute for a {1} instance. Check the spelling of the attribute/relation or the list of available properties for this class with 'get_class_properties(instance class)' function'".format(
                        key, clas))
            # if it is an ObjectProperty (Relations)
            df_row = driver.properties.loc[driver.properties["label"] == key]
            if df_row["type"].values[0] == "ObjectProperty":
                # if score deletion (in delete case there are any difference due to the deletion not need a score value)
                # if key in ['scored in', 'score']:
                #    for end_label, score in value.items():
                #        val_id = get_id_from_label_relation(driver, end_label, key)
                #        if val_id is None:
                #            raise Exception(
                #                "The relation {0} - {1} - {2} it's been impossible to add due to {2} can't be found.".format(
                #                    get_node_by_id(driver=driver, neo_id=neo_id)['rdfs__label'], key, end_label))
                #        modify_score(driver=driver, start_node_id=neo_id, end_node_id=val_id, delete_property=True)
                #    continue

                # if is a single relation, create a list of one element (standardize the values to lists)
                if type(value) is not list:
                    value = [value]
                # for each relation node insert the new property
                for val in value:
                    val_id = get_id_from_label_relation(driver, val, key)
                    if val_id is None:
                        raise ValueError(
                            "The relation {0} - {1} - {2} it's been impossible to add due to {2} can't be found.".format(
                                get_node_by_id(driver=driver, neo_id=neo_id)['rdfs__label'], key, val))
                    delete_relation(driver=driver, start_node_id=neo_id, end_node_id=val_id, relation=key)
            # if it is a DataTypeProperty (Attributes)
            else:
                delete_attribute(driver=driver, node_id=neo_id, attribute=key)

    # ADD PROPERTIES DICTIONARY
    if bool(add_dict):
        # insert new parameters
        for key, value in add_dict.items():
            if value is None:
                continue
            if key not in range_all:
                raise ValueError(
                    "The property '{0}' is not a valid relation/attribute for a {1} instance. Check the spelling of the attribute/relation or the list of available properties for this class with 'get_class_properties(instance class)' function'".format(
                        key, clas))
            # if it is an ObjectProperty (Relations)
            df_row = driver.properties.loc[driver.properties["label"] == key]
            if df_row["type"].values[0] == "ObjectProperty":
                # if score insertion
                if key in ['scored in', 'score']:
                    for end_label, score in value.items():
                        val_id = get_id_from_label_relation(driver, end_label, key)
                        if val_id is None:
                            raise ValueError(
                                "The relation {0} - {1} - {2} it's been impossible to add due to {2} can't be found.".format(
                                    get_node_by_id(driver=driver, neo_id=neo_id)['rdfs__label'], key, end_label))
                        modify_score(driver=driver, start_node_id=neo_id, end_node_id=val_id, score=score)
                    continue
                # if is a single relation, create a list of one element (standardize the values to lists)
                if type(value) is not list:
                    value = [value]
                # for each relation node insert the new property
                for val in value:
                    val_id = get_id_from_label_relation(driver, val, key)
                    if val_id is None:
                        raise ValueError(
                            "The relation {0} - {1} - {2} it's been impossible to add due to {2} can't be found.".format(
                                get_node_by_id(driver=driver, neo_id=neo_id)['rdfs__label'], key, val))
                    insert_relation(driver=driver, start_node_id=neo_id, end_node_id=val_id, relation=key)
            # if it is a DataTypeProperty (Attributes)
            else:
                insert_attribute(driver=driver, node_id=neo_id, attribute=key, value=value)


def modify_score(driver: NeoDriver, start_node_id: int, end_node_id: int, score: float = None, delete_property: bool = False):
    """
    Insert, or delete, a score in the AMS.
    :param driver: Driver object which contains the connection to the database and the static variable.
    :param start_node_id: Identifier of the instance (node) from which the new score is to be set.
    :param score: The value of the (float number).
    :param end_node_id: identifier of the node to connect.
    :param delete_property: whether to delete attributes or relations (True), or to insert new ones (False).
    """
    if type(start_node_id) is not int or start_node_id < 0:
        raise ValueError("start_node_id parameter has the wrong format, it has to be an integer and  >= 0.")
    if not (score is None or isinstance(score, numbers.Number)):
        raise ValueError("score parameter has the wrong format, it has to be a number or None.")
    if type(end_node_id) is not int or end_node_id < 0:
        raise ValueError("end_node_id parameter has the wrong format, it has to be an integer and  >= 0.")
    if type(delete_property) is not bool:
        raise ValueError("delete_property parameter has to be a boolean.")

    if not if_node_exist_by_id(driver=driver, node_id=start_node_id):
        raise ValueError("start_node_id does not correspond to any existent instance.")

    # if starting instance is a AITechnique/Business Area relation is 'score', if else 'scored in'
    relation = 'score' if get_node_by_id(driver=driver,
                                         neo_id=start_node_id)['rdfs__label'] in driver.technique_hierarchy + driver.business_hierarchy else 'scored in'

    try:
        # if already exists a score, then delete the original to be able to insert the new one below
        if not delete_property and if_relation_exists(driver=driver, start_node_id=start_node_id, end_node_id=end_node_id, relation=relation):
            delete_relation(driver=driver, start_node_id=start_node_id, end_node_id=end_node_id, relation=relation)
        # if the modification is a deletion
        if delete_property:
            delete_relation(driver=driver, start_node_id=start_node_id, end_node_id=end_node_id, relation=relation)
        # if the modification is an insertion
        else:
            insert_relation(driver=driver, start_node_id=start_node_id, end_node_id=end_node_id, relation=relation, value=score)
    except Exception:
        raise sys.exc_info()[1]


def score_list_modifier(driver: NeoDriver, score_list: list):
    """
    Insert a list of scores in the AMS using a list of dictionaries.
    :param driver: Driver object which contains the connection to the database and the static variable.
    :param score_list: list of dictionaries, where each dictionary is a score insertion, it requires the fields of
    'start_node_id' (with the ID of the relation starting instance), 'end_node_id' (with the ID of the relation
    ending instance) and 'score' (field that specify the score value).
    """
    if type(score_list) is not list:
        raise ValueError("score_list parameter has to be a list.")

    name_list = ['delete_property', 'start_node_id', 'end_node_id', 'score']
    for score_dict in score_list:
        if type(score_dict) is not dict or not set(name_list) == set(score_dict.keys()):
            raise ValueError("Score list modification stopped do to an incorrect parameter format in almost one of the score dictionaries of the list.")
        if type(score_dict['delete_property']) is not bool:
            raise ValueError("Score list element dictionary has delete_property as a non-boolean.")
        if score_dict['delete_property']:
            modify_score(driver=driver, start_node_id=score_dict['start_node_id'],
                         end_node_id=score_dict['end_node_id'], delete_property=score_dict['delete_property'])
        else:
            modify_score(driver=driver, start_node_id=score_dict['start_node_id'], score=score_dict['score'],
                         end_node_id=score_dict['end_node_id'], delete_property=score_dict['delete_property'])

