from ..kr.relation import *
from ..kr.miscellaneous import *
from ..kr.NeoDriver import NeoDriver


# QUERIES #
def free_query(driver: NeoDriver, query: str):
    """
    This query allows a personalized query construction.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param query: string with the Cypher code command (query) that is going to be used.
    :return: The result of the query, without post-processing.
    """
    if type(query) is not str or query == "":
        raise ValueError("query parameter has to be a non-empty String.")

    # control if the query has any of the forbidden clauses
    if any(ele in query.upper() for ele in driver.forbidden_clauses):
        raise ValueError("The query proposed contains at least one of the following forbidden clauses: {0}".format(
            driver.forbidden_clauses))
    # if any wrong query format raise Value error
    try:
        res = driver.graph.query(query)
    except:
        raise ValueError('Invalid input, check the query correctness.')

    return res


def query_exist_instance(driver: NeoDriver, neo_id: int):
    """
    This query returns true if a specific instance exists in the database.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param neo_id: ID of the instance searched.
    :return: True if the instance identified by id exists in the AMS, False otherwise.
    """
    if type(neo_id) is not int or neo_id < 0:
        raise ValueError("neo_id parameter has the wrong format, it has to be an integer and  >= 0.")

    return if_node_exist_by_id(driver=driver, node_id=neo_id)


def query_if_relation_exists(driver: NeoDriver, start_node_id: int, relation: str, end_node_id: int):
    """
    This query returns True if a specific relation exists in the AMS, or returns false if it does not exist
    :param start_node_id: identifier of the starting instance in the relation (all the relations in the AMS are
    directed).
    :param driver: Driver object which contains the connection to the database and the static variables
    :param relation: relation to be searched.
    :param end_node_id: identifier of the ending instance in the relation (all the relations in the AMS are
    directed).
    :return: A boolean with the result of the query.
    """
    if type(start_node_id) is not int or start_node_id < 0:
        raise ValueError("start_node_id parameter has the wrong format, it has to be an integer and  >= 0.")
    if type(relation) is not str or len(relation) == 0:
        raise ValueError("relation parameter has to be a non-empty String.")
    if type(end_node_id) is not int or end_node_id < 0:
        raise ValueError("end_node_id parameter has the wrong format, it has to be an integer and  >= 0.")

    return if_relation_exists(driver=driver, start_node_id=start_node_id, end_node_id=end_node_id, relation=relation)


def query_node_by_name_class(driver: NeoDriver, name: str, class_label: str, to_dict=False):
    """
    This query returns an instance, if it exists, based on its name and the class.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param name: name of the instance searched.
    :param class_label: class of the instance searched.
    :param to_dict: whether the result must be returned as a dictionary (True) or py2neo.Node instance (False).
    :return: Instance data or instance node, if it exists.
    """
    if type(name) is not str or len(name) == 0:
        raise ValueError("name parameter has to be a non-empty String.")
    if type(class_label) is not str or len(name) == 0:
        raise ValueError("class_label parameter has to be a non-empty String.")
    if type(to_dict) is not bool:
        raise ValueError("to_dict parameter has to be a boolean.")

    return get_node_by_name(driver=driver, instance_name=name, class_label=class_label, to_dict=to_dict)


def query_node_by_uuid(driver: NeoDriver, uuid_cd: str, class_label: str, to_dict: bool = False):
    """
    This query returns an instance, if it exists, based on its name and the class.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param uuid_cd: UUID identifier of the instance searched.
    :param class_label: class of the instance searched.
    :param to_dict: whether the result must be returned as a dictionary (True) or py2neo.Node instance (False).
    :return: Instance data or instance node, if it exists.
    """
    if type(uuid_cd) is not str or len(uuid_cd) == 0:
        raise ValueError("uuid parameter has to be a non-empty String.")
    if type(class_label) is not str or len(class_label) == 0:
        raise ValueError("class_label parameter has to be a non-empty String.")
    if type(to_dict) is not bool:
        raise ValueError("to_dict parameter has to be a boolean.")

    return get_node_by_uuid(driver=driver, uuid=uuid_cd, class_label=class_label, to_dict=to_dict)


def query_node_by_id(driver: NeoDriver, neo_id: int, to_dict=False):
    """
    This query returns the node, if it exists, given its identifier.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param neo_id: id of the instance searched.
    :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
    :return: Instance data or instance node, if it exists.
    """
    if type(neo_id) is not int or neo_id < 0:
        raise ValueError("neo_id parameter has the wrong format, it has to be an integer and  >= 0.")
    if type(to_dict) is not bool:
        raise ValueError("to_dict parameter has to be a boolean.")

    return get_node_by_id(driver=driver, neo_id=neo_id, to_dict=to_dict)


def query_instances_by_class(driver: NeoDriver, class_label: str, with_inference: bool = True, only_labels: bool = False):
    """
    This query returns all the instances of a specific class.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param class_label: class name.
    :param with_inference: in the case of True value in this parameter, the query will retrieve the instances of the
    class specified as a parameter and all the instances of its subclasses also.
    :param only_labels: If True, the result would be the list of all the instances labels. If False, the result would be
     the list of all the nodes of the class
    :return: List of the instance names or instance nodes, if they exist.
    """
    if type(class_label) is not str or len(class_label) == 0:
        raise ValueError("class_label parameter has to be a non-empty String.")
    if type(with_inference) is not bool:
        raise ValueError("with_inference parameter has to be a boolean.")
    if type(only_labels) is not bool:
        raise ValueError("only_labels parameter has to be a boolean.")

    if class_label in driver.business_hierarchy:
        return [elem for elem in driver.business_hierarchy if len(get_all_instances_by_class(driver=driver, class_label=elem, only_labels=only_labels)) > 0]
    if class_label in driver.technique_hierarchy:
        return [elem for elem in driver.technique_hierarchy if len(get_all_instances_by_class(driver=driver, class_label=elem, only_labels=only_labels)) > 0]
    if with_inference:
        subclasses = inference_class_subclasses(driver=driver, class_label=class_label)
        resu = []
        for subclass in subclasses:
            resu += get_all_instances_by_class(driver=driver, class_label=subclass, only_labels=only_labels)
        return resu
    else:
        return get_all_instances_by_class(driver=driver, class_label=class_label, only_labels=only_labels)


def query_class_by_id(driver: NeoDriver, neo_id: int):
    """
    This query returns the class label of an instance, given an identifier.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param neo_id: id of the instance searched.
    :return: The class label of the instance with the id.
    """
    if type(neo_id) is not int or neo_id < 0:
        raise ValueError("neo_id parameter has the wrong format, it has to be an integer and  >= 0.")

    # get the node using its id
    start_node = get_node_by_id(driver=driver, neo_id=neo_id)
    # if it does not exist, raise an exception
    if start_node is None:
        raise ValueError("There is not a node in the AMS with {0} as ID".format(str(neo_id)))
    if 'owl__NamedIndividual' not in start_node.labels:
        raise ValueError("The ID does not belong to an instance in the database")
    # return the class label (database version)
    return driver.graph.match((start_node,), r_type='owl__instanceOf').first().end_node['rdfs__label']


def query_related_nodes_by_relation(driver: NeoDriver, neo_id: int, relation: str, to_dict=False):
    """
    This query returns the data of the instances related to the given identifier and relation.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param neo_id: id of the instance searched.
    :param relation: relation to be searched.
    :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
    :return: List of the instance data, or instance nodes, if they exist.
    """
    if type(neo_id) is not int or neo_id < 0:
        raise ValueError("neo_id parameter has the wrong format, it has to be an integer and  >= 0.")
    if type(relation) is not str or len(relation) == 0:
        raise ValueError("relation parameter has to be a non-empty String.")
    if type(to_dict) is not bool:
        raise ValueError("to_dict parameter has to be a boolean.")

    # get the starting node
    start_node = get_node_by_id(driver=driver, neo_id=neo_id)
    # if it does not exist, return exception
    if start_node is None:
        raise ValueError(
            "There is not a node in the AMS with {0} as ID".format(str(neo_id)))
    # if relation does not exist in the db
    if relation not in driver.properties["label"].values:
        raise ValueError("The relation type: " + relation + " does not exist in the AMS.")
    # get the relations from the database that has as a label the relation passed as a parameter
    df_row = driver.properties.loc[driver.properties["label"] == relation]
    # if relation out of domain of the node
    node_class = node_data_to_dict(driver=driver, node=start_node)['class']
    if node_class not in df_row["domain"].to_list()[0]:
        raise ValueError(node_class + " class it's not in the domain of the relation: " + relation)
    # get the relation
    relations = driver.graph.match((start_node,), r_type="swai__" + df_row["db_name"].values[0]).all()
    # if any relation extracted return None
    if relations is None or len(relations) == 0:
        return []
    # if to_dict is True, return the node as a dictionary
    return [node_data_to_dict(driver, relation.end_node) if to_dict else relation.end_node for relation in
            relations]


def query_get_artifacts_by_tag(driver: NeoDriver, categories: list, limit: int, inference_cat: bool = False, to_dict: bool = False):
    """
    This query returns a list of AI Artifacts that belongs to a specific AI Technique.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param categories: list of AI Techniques from which to retrieve the instances.
    :param limit: limit of nodes to be retrieved by the query.
    :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
    :param inference_cat: whether to infer the AI Techniques subcategories of the ones in categories parameter.
    :return: A list of dictionaries where, for each category searched, a dictionary is retrieved with the artifact
    and the category that belongs to.
    """
    if type(categories) is not list:
        raise ValueError("categories parameter has to be a list.")
    if type(limit) is not int or limit <= 0:
        raise ValueError("limit parameter has the wrong format, it has to be an integer and  > 0.")
    if type(to_dict) is not bool:
        raise ValueError("to_dict parameter has to be a boolean.")
    if type(inference_cat) is not bool:
        raise ValueError("inference_cat parameter has to be a boolean.")

    #  relations of interest for this query
    interest_relations = ['applied on', 'has been applied in']
    # check that all the categories are supported by the ontology
    for cat1 in categories:
        if cat1 not in driver.technique_hierarchy:
            raise ValueError("All the categories passed should be AI Techniques, {0} is not one".format(str(cat1)))
    # infer sub-categories if the parameter inference_cat is True
    if inference_cat:
        unflat_list = [inference_class_subclasses(driver, category) for category in categories]
        categories = list(set([item for sublist in unflat_list for item in sublist]))
    # extract the categories nodes
    node_cat = [{'node': driver.graph.nodes.match(
        'aitec__' + driver.class_owl.loc[driver.class_owl["label"] == cat].values[0][1]).first(), 'cat': cat} for cat in
                categories]
    # get the AI assets extracted for each category
    nodes = []
    for node1 in node_cat:
        aux = []
        for rel in interest_relations:
            if node1['node'] is not None:
                a1 = query_related_nodes_by_relation(driver, node1['node'].identity, rel, to_dict=to_dict)
                if a1 is not None:
                    aux += a1
        if len(aux) > limit:
            aux = aux[:limit]
        if len(aux) > 0:
            nodes.append({'category': node1['cat'], 'nodes': aux})
    # if to_dict is True, return the node as a dictionary
    return nodes


def query_get_experts_by_tag(driver: NeoDriver, categories: list, limit: int, inference_cat: bool = False, to_dict: bool = False):
    """
    This query returns a list of AI Experts that belongs to a specific AI Technique.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param categories: list of AI Techniques from which to retrieve the instances.
    :param limit: limit of nodes to be retrieved by the query.
    :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
    :param inference_cat: whether to infer the AI Techniques subcategories of the ones in categories parameter.
    :return: A list of dictionaries where, for each category searched, a dictionary is retrieved with the expert
    and the category that belongs to.
    """
    if type(categories) is not list:
        raise ValueError("categories parameter has to be a list.")
    if type(limit) is not int or limit <= 0:
        raise ValueError("limit parameter has the wrong format, it has to be an integer and  > 0.")
    if type(to_dict) is not bool:
        raise ValueError("to_dict parameter has to be a boolean.")
    if type(inference_cat) is not bool:
        raise ValueError("inference_cat parameter has to be a boolean.")

    #  relations of interest for this query
    interest_relations = ['expertised by']
    # check that all the categories are supported by the ontology
    for cat1 in categories:
        if cat1 not in driver.technique_hierarchy:
            raise ValueError("All the categories passed should be AI Techniques, {0} is not one".format(str(cat1)))
    # infer sub-categories if the parameter inference_cat is True
    if inference_cat:
        unflat_list = [inference_class_subclasses(driver, category) for category in categories]
        categories = list(set([item for sublist in unflat_list for item in sublist]))
    # extract the categories nodes
    node_cat = [{'node': driver.graph.nodes.match(
        'aitec__' + driver.class_owl.loc[driver.class_owl["label"] == cat].values[0][1]).first(), 'cat': cat} for cat in
                categories]
    # get the AI assets extracted for each category
    nodes = []
    for node1 in node_cat:
        aux = []
        for rel in interest_relations:
            if node1['node'] is not None:
                a1 = query_related_nodes_by_relation(driver, node1['node'].identity, rel, to_dict=to_dict)
                if a1 is not None:
                    aux += a1
        if len(aux) > limit:
            aux = aux[:limit]
        if len(aux) > 0:
            nodes.append({'category': node1['cat'], 'nodes': aux})
    # if to_dict is True, return the node as a dictionary
    return nodes


def query_topk_assets(driver: NeoDriver, category: str, k: int, to_dict: bool = False):
    """
    This query returns an ordered asset list with, at most, K elements based on the scored assets in AI Technique.
    :param driver: Driver object which contains the connection to the database and the static variables
    :param category: AI Technique from which the assets will be retrieved.
    :param k: the maximum number of elements retrieved.
    :param to_dict: whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
    :return: An ordered list of assets (dictionaries{score, instance}).
    """
    if type(category) is not str or len(category) == 0:
        raise ValueError("category parameter has to be a non-empty String.")
    if type(k) is not int or k <= 0:
        raise ValueError("k parameter has the wrong format, it has to be an integer and  > 0.")
    if type(to_dict) is not bool:
        raise ValueError("to_dict parameter has to be a boolean.")

    # check that the category is an AI technique in the database
    if category not in driver.technique_hierarchy:
        raise ValueError("The category passed should be AI Technique, {0} is not one.".format(str(category)))
    # get the node from the category passed as a parameter
    cat_node = driver.graph.nodes.match(
        'aitec__' + driver.class_owl.loc[driver.class_owl["label"] == category].values[0][1]).first()
    # extract the top K elements scored for the category
    res_rel = driver.graph.match((cat_node,), r_type='swai__score').order_by('2 - (_.score + 1)').limit(k).all()
    nodes = [{'score': rel['score'], 'node': rel.end_node} for rel in res_rel]
    # if to_dict is True, return the node as a dictionary
    return [{'score': node2['score'], 'node': node_data_to_dict(driver, node2['node'])} if to_dict else node2 for node2
            in nodes]


def query_env_by_hp(driver: NeoDriver, hp: str):
    """
    Method that given a hardware platform, returns the associated environment. if it does not exist, returns
    'unknown_HP'.
    :param driver: Graph database connection using py2neo library.
    :param hp: hardware platform associated to an environment
    :return the environment associated a hardware platform
    """
    if type(hp) is not str or len(hp) == 0:
        raise ValueError("hp parameter has to be a non-empty String.")

    # get the Hardware platform node
    hp_node = get_node_by_name(driver=driver, instance_name=hp, class_label='Hardware Platform')

    # if not exists create an HP mode, in addition create an unknown environment and its relation
    if hp_node is None:
        insert_node(driver=driver, name=hp, node_class='Hardware Platform')
        hp_node = insert_node(driver=driver, name=hp, node_class='Hardware Platform')
        env_node = insert_node(driver=driver, name=hp + '_unknown', node_class='Environment')
        insert_relation(driver=driver, start_node_id=env_node.identity, relation='runs on',
                        end_node_id=hp_node.identity)
        return env_node

    # search for the environment associated to the Hardware platform
    env_node = driver.graph.run(
        "MATCH (startNode)-[:swai__runsOn]->(end:swai__HardwarePlatform {rdfs__label: $a}) RETURN startNode",
        {'a': hp_node['rdfs__label']}).data()

    # if there are any environment associated to the hardware platform search in the static dictionary and create
    # the relation between them
    if len(env_node) == 0:
        if hp not in list(driver.hwp2env.keys()):
            raise Exception("{0} Hardware Platform error".format(hp))
        env_name = driver.hwp2env[hp]
        env_node = get_node_by_name(driver=driver, instance_name=env_name, class_label='Environment')
        insert_relation(driver=driver, start_node_id=env_node.identity, relation='runs on',
                        end_node_id=hp_node['swai__identifier'])
        return env_node
    return env_node[0]['startNode']
