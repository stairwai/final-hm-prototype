# StairwAI Asset Management System (Admin version)

This README has the requirements, steps to install and possibilities of the current StairwAI Asset Management System (AMS) API version (Admin version). Additionally, it has a bref explanation of some important concepts to understand each part of the AMS and functionalities and extra functionalities, only available for the administrator.

## Requirements

Basic system requirements:
- python>=3.6
- numpy==1.17.4
- pandas==0.25.3
- py2neo==2021.2.3

## Steps to install and run the API

1. Import the class `Driver` from `doc-api.ams_api.StairwAIDriver.py`

2. Start to use the driver with the implemented functionalities explained below


## General description

This documentation explains the third draft structures and capabilities. The capabilities are the same as the second version but the API structure was separated into different modules to allow a better understandability of the code. The previous Admin/user versions are fusion into only one version.

StairwAI AMS is build on the StairwAI ontology, which specify the classes available, the relations between the classes and the attributes that each of these classes can have. Additionally, StairwAI ontology also specifies the instances for some classes that non-instantiable, these non-instantiable classes can be considered as a close-set of options in which the instances of other classes can be related with.

So, mainly, in StairwAI AMS we can distinguish two type of classes: (1) instantiable (can be instantiated by the user) and (2) non-instantiable (already defined in ontology). Each of these classes have instances which can be related with, and only, instances using the Relations defined in the StairwAI ontology as Object properties. In this direction, the user can only stablish a relation between two already created instances in the AMS. Apart from Relations, the Attributes are properties of each node individually, and they are also defined in the StairwAI ontology as DataType properties, such as, description, identifier or keywords.


## Possibilities of the current AMS version
This second draft follows a similar structure as the first one, you are able to find three main capabilities, plus extra admin functionalities:
1. **Admin functionalities**: The Admin functionalities can deeply modify the AMS structure, we can find three of them:
   * Generate/modify the swai_ams.json file. This file contains the fixed structures and domain/range associated to each property of the AMS. This file can only be read by the other users, but the admin can also modify it.
   * Back-up function, this functionality stores all the added instances to a file, this file can be read after a reset to avoid the lost of data during a reset. (Under development currently)
   * Hard reset function, this functionality allows the possibility to reset the AMS, e.g. in the case of StairwAI ontology update. This function automatically stores all the inserted instances in a back-up file, restart the AMS, importing the latest StairwAI ontology available, making the changes to the *swai_ams.json* file and finally incorporating the instances available in the back-up. WARNING: currently, an AMS reset will change all the node identifiers! (Back-up part under development currently)
1. **Getters**: These functionalities allow the fast extraction of fixed data from the AMS, we could divide this data into:
   * Get non-instantiable class instances, e.g., get_countries or get_aitechniques.
   * Get general data, such as, all the classes, instantiable classes, attributes or relations of the AMS.
   * Get node information or related class information. 
2. **Insert/Delete/Modify methods**: These three methods allow the data modification in the AMS allowing inserting, deleting or modifying instances with their attributes and relationships.
3. **Predefined queries**: This set of predefined queries can handle the usual queries that during the development of the tool the developers have proposed as useful, as well as some that have been added on demand of other project partners. It is worth mentioning that the development team is still open to adding new queries on demand, so do not hesitate to contact us if that is the case.

### Back-up functionalities
#### hard_db_reset
Property | Value
--- | ---
Description | Hard reset function, this functionality allows the possibility to reset the AMS, e.g., in the case of StairwAI ontology update. This function automatically stores all the inserted instances in a backup file, restarts the AMS, imports the latest StairwAI ontology available, makes the changes to the swai_ams.json file, and finally incorporates the instances available in the backup. WARNING: currently, an AMS reset will change all the node identifiers!
Parameter | with_backup: Boolean parameter, if set to true, a backup of the AMS instances is done before the reset, after which the instances are loaded to the AMS again. If set to False, the AMS INSTANCES WILL BE COMPLETELY AND PERMANENTLY DELETED.
Parameter | path: String indicating the path to store the backup files.
Return | None

#### create_backup
Property | Value
--- | ---
Description | This method creates a StairwAI AMS back-up, with the information extracted from the nodes and relations it creates two pickle files where these data are stored. Use 'read_backup()' function to store again the files' information to the database.
Parameter | path: String indicating the path to store the back-up files.
Return | None

#### read_backup
Property | Value
--- | ---
Description | This method reads the pickle files with the AMS instances back-up generated by the method create_backup(), and loads the instances in the AMS. Additionally, it deletes the pickle files.
Parameter | path: String indicating the path to store the back-up files.
Return | None


### Individual getter functionalities
#### get_classes
Property | Value
--- | ---
Description | Getter of the ontology defined classes.
Parameter | with_descriptions: Boolean, indicating whether the output includes the description associated with labels.
Return | A list of the available classes.

#### get_instantiable_classes
Property | Value
--- | ---
Description | Getter of the ontology instantiable classes with its definitions.
Parameter | with_descriptions: Boolean, indicating whether the output includes the description associated with labels.
Return | A list, or dictionary, of the available instantiable classes.

#### get_attributes
Property | Value
--- | ---
Description | Getter of the ontology defined attributes.
Parameter | with_conditions: Boolean, indicating whether the method must return the domain of each property.
Return | If with_conditions is True, a pandas DataFrame with the properties and their domains. Otherwise, it will return the list of available attributes.

#### get_relations
Property | Value
--- | ---
Description | Getter of the ontology defined relations.
Parameter | with_conditions: Boolean, indicating whether the method must return the domain/range of the relation.
Return | If with_conditions is True, a pandas DataFrame with the relations and their domain/range. Otherwise, it will return the list of available relations.

#### get_class_properties
Property | Value
--- | ---
Description | This method returns a dictionary with the properties, and their definitions, of a specific type of class.
Parameter | class_type: String, the name (label) of the class from which to extract the properties.
Return | A dictionary with the properties available for a specific class and its requirements.

#### get_node_data_to_dict
Property | Value
--- | ---
Description | This query returns the node data as a dictionary.
Parameter | instance_node: py2neo.Node instance.
Return | Node data as a dictionary; if node = None, returns None.

#### get_business_categories
Property | Value
--- | ---
Description | Available business categories' getter.
Parameter | with_description: Boolean, if True it returns the descriptions, in addition to the labels of the business categories as a dictionary.
Return | List of the available Business category instances.

#### get_aitechniques
Property | Value
--- | ---
Description | Available AI Techniques' getter.
Parameter | with_description: Boolean, if True it returns the descriptions, in addition to the labels of the business categories as a dictionary.
Return | List of the available AI Technique instances.

#### get_aiexperts
Property | Value
--- | ---
Description | Getter of the AI expert instances in the database. Depending on the parameters, it returns a specific AI expert instance or all of them. Additionally, can return the id only or the whole node.
Parameter | uuid_identifier: String, UUID identifier of a specific AI expert.
Parameter | swai_id_only: Boolean, if True, the method returns only IDs; if False, the node.
Return | List of the available AI expert instances or a specific resource.


### Creators functionalities
#### insert_instance
Property | Value
--- | ---
Description | This function allows the insertion of new instances into the database. It facilitates checking the available instantiable classes and understanding the relevant relations and attributes for instances of a specific class.
Parameter | class_type: The class of the inserted instance.
Parameter | param_dict: A dictionary specific to the instance (relations or attributes) to be modified in the AMS. This dictionary only accepts relations/attributes specific to the class of the node.
Return | A dictionary with the data of the node inserted.

#### delete_instance
Property | Value
--- | ---
Description | This function deletes the instance specified by its identifier.
Parameter | neo_id: Identifier of the instance to be deleted.
Return | None

#### modify_instance
Property | Value
--- | ---
Description | This function allows for the modification of any instance node in the database. It includes functionality to add or delete specific attributes or relations of an instance.
Parameter | neo_id: Identifier of the instance to be modified.
Parameter | add_dict: A dictionary of instance-specific relations or attributes to be added in the AMS.
Parameter | delete_dict: A dictionary of instance-specific relations or attributes to be deleted in the AMS.
Return | A dictionary with the data of the modified node.

#### modify_score
Property | Value
--- | ---
Description | Allows for inserting or deleting a score in the AMS.
Parameter | start_node_id: Identifier of the starting instance (node) for setting the new score.
Parameter | end_node_id: Identifier of the node to connect.
Parameter | score: The value of the score (float number).
Parameter | delete_property: Boolean indicating whether to delete (True) or insert (False) attributes or relations.
Return | The result of the score modification.

#### score_list_modifier
Property | Value
--- | ---
Description | Inserts a list of scores in the AMS using a list of dictionaries, each representing a score insertion.
Parameter | score_list: A list of dictionaries, where each dictionary includes 'start_node_id', 'end_node_id', and 'score' fields.
Return | The result of the score list modification.


### Queries functionalities
#### free_query
Property | Value
--- | ---
Description | This query allows a personalized query construction.
Parameter | query: String with the Cypher code command to be used.
Return | The result of the query, without post-processing.

#### query_exist_instance
Property | Value
--- | ---
Description | This query returns true if a specific instance exists in the database.
Parameter | neo_id: ID of the instance searched.
Return | True if the instance identified by ID exists in the AMS, False otherwise.

#### query_if_relation_exists
Property | Value
--- | ---
Description | This query returns True if a specific relation exists in the AMS, or returns false if it does not exist.
Parameter | start_node_id: Identifier of the starting instance in the relation.
Parameter | relation: Relation to be searched.
Parameter | end_node_id: Identifier of the ending instance in the relation.
Return | A boolean with the result of the query.

#### query_node_by_name_class
Property | Value
--- | ---
Description | This query returns an instance, if it exists, based on its name and the class.
Parameter | name: Name of the instance searched.
Parameter | ont_class: Class of the instance searched.
Parameter | to_dict: Whether to return the result as a dictionary (True) or py2neo.Node instance (False).
Return | Instance data or instance node, if it exists.

#### query_node_by_id
Property | Value
--- | ---
Description | This query returns the node, if it exists, given its identifier.
Parameter | neo_id: ID of the instance searched.
Parameter | to_dict: Whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | Instance data or instance node, if it exists.

#### query_node_by_uuid
Property | Value
--- | ---
Description | This query returns an instance, if it exists, based on its UUID and the class.
Parameter | uuid_db: UUID identifier of the instance searched.
Parameter | ont_class: Class of the instance searched.
Parameter | to_dict: Whether the result must be returned as a dictionary (True) or py2neo.Node instance (False).
Return | Instance data or instance node, if it exists.

#### query_instances_by_class
Property | Value
--- | ---
Description | This query returns all the instances of a specific class.
Parameter | class_label: Class name.
Parameter | with_inference: If True, retrieves instances of the specified class and its subclasses.
Parameter | only_labels: Whether to return the node data as labels list or py2neo.Node instances (False).
Return | List of the instance data or instance nodes, if they exist.

#### query_class_by_id
Property | Value
--- | ---
Description | This query returns the class label of an instance, given an identifier.
Parameter | neo_id: ID of the instance searched.
Return | The class label of the instance with the ID.

#### query_related_nodes_by_relation
Property | Value
--- | ---
Description | This query returns the data of instances related to the given identifier and relation.
Parameter | neo_id: ID of the instance searched.
Parameter | relation: Relation to be searched.
Parameter | to_dict: Whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | List of the instance data, or instance nodes, if they exist.

#### query_get_artifacts_by_tag
Property | Value
--- | ---
Description | This query returns a list of AI Artifacts belonging to a specific AI Technique.
Parameter | categories: List of AI Techniques from which to retrieve instances.
Parameter | limit: Limit of nodes to be retrieved by the query.
Parameter | inference_cat: Whether to infer AI Techniques subcategories of the ones in categories parameter.
Parameter | to_dict: Whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | A list of dictionaries for each category searched, with artifacts and the category they belong to.

#### query_get_experts_by_tag
Property | Value
--- | ---
Description | This query returns a list of AI Experts belonging to a specific AI Technique.
Parameter | categories: List of AI Techniques from which to retrieve instances.
Parameter | limit: Limit of nodes to be retrieved by the query.
Parameter | inference_cat: Whether to infer AI Techniques subcategories of the ones in categories parameter.
Parameter | to_dict: Whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | A list of dictionaries for each category searched, with experts and the category they belong to.

#### query_topk_assets
Property | Value
--- | ---
Description | This query returns an ordered asset list with, at most, K elements based on scored assets in AI Technique.
Parameter | category: AI Technique from which assets will be retrieved.
Parameter | k: The maximum number of elements to retrieve.
Parameter | to_dict: Whether to return the node data as a dictionary (True) or a py2neo.Node instance (False).
Return | An ordered list of assets (dictionaries with score and instance).


## Contact points

- miquel.buxons@upc.edu
- javier.vazquez-salceda@upc.edu

