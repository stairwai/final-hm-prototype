import unittest
import py2neo
import random
import time
from ams_api.StairwAIDriver import Driver


class TestAPIQueries(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        time.sleep(2)  # Delay for 2 seconds to have time to close all the socket connections
        cls.driver = Driver()
        cls.nodes = []
        cls.nodes = []
        for cl in ['Model', 'AI expert']:
            for n in range(5):
                cls.nodes.append(cls.driver.insert_instance(class_type=cl, param_dict={'name': 'Example_' + str(n),
                                                                                       'keyword': 'example_keyword',
                                                                                       'has language': 'Spanish',
                                                                                       'scored in': {
                                                                                           'Searching': random.random(),
                                                                                           'Machine learning': random.random()}}))
        cls.generates_node = cls.driver.free_query("MATCH (n:owl__ObjectProperty {rdfs__label: 'generates'}) RETURN n;").data()[0]['n']
    @classmethod
    def tearDownClass(cls):
        for node in cls.nodes:
            cls.driver.delete_instance(neo_id=node['swai__identifier'])

    def test_free_query(self):
        # create an incorrect query
        query = f"MATCH (target:owl__NamedIndividual:swai__Model)-[:owl__instanceOf]->(n:owl__Class) " \
                f"WHERE n.rdfs__label = 'Model' target.rdfs__label = 'Example_0' RETURN target"
        with self.assertRaisesRegex(ValueError, "Invalid input, check the query correctness."):
            self.driver.free_query(query)
        # create a forbiden query
        query = f"""CREATE (n:owl__Class {{rdfs__label: 'Ex'}}) RETURN n"""
        with self.assertRaises(ValueError):
            self.driver.free_query(query)
        # correct query
        query = f"MATCH (target:owl__NamedIndividual:swai__Model)-[:owl__instanceOf]->(n:owl__Class) " \
                f"WHERE n.rdfs__label = 'Model' AND target.rdfs__label = 'Example_0' RETURN target"
        self.assertIsInstance(self.driver.free_query(query), py2neo.cypher.Cursor)

    def test_query_exist_instance(self):
        # wrong ID
        with self.assertRaisesRegex(ValueError, "neo_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.query_exist_instance(neo_id=None)
        # no class and instance ID
        self.assertIsInstance(self.driver.query_exist_instance(neo_id=self.generates_node.identity), bool)
        # Correct ID
        self.assertIsInstance(self.driver.query_exist_instance(neo_id=self.nodes[0]['swai__identifier']), bool)

    def test_query_if_relation_exists(self):
        sp_n = self.driver.query_node_by_name_class(name='Spanish', ont_class='Language')
        # wrong parameter format (any of the 3 cases)
        with self.assertRaisesRegex(ValueError,
                                    "start_node_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.query_if_relation_exists(start_node_id=-1, end_node_id=sp_n['swai__identifier'], relation='has language')
        with self.assertRaisesRegex(ValueError,
                                    "end_node_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.query_if_relation_exists(start_node_id=self.nodes[-1]['swai__identifier'], end_node_id=-1,
                                                 relation='has language')
        with self.assertRaisesRegex(ValueError, "relation parameter has to be a non-empty String."):
            self.driver.query_if_relation_exists(start_node_id=self.nodes[-1]['swai__identifier'],
                                                 end_node_id=sp_n['swai__identifier'], relation='')
        # wrong id in a case
        self.assertIsInstance(
            self.driver.query_if_relation_exists(start_node_id=self.nodes[-1]['swai__identifier'], end_node_id=1340,
                                                 relation='has language'), bool)
        # wrong relation label
        self.assertIsInstance(
            self.driver.query_if_relation_exists(start_node_id=self.nodes[-1]['swai__identifier'],
                                                 end_node_id=sp_n['swai__identifier'],
                                                 relation='scored in'), bool)
        # nonexistent relation
        with self.assertRaisesRegex(ValueError, "The relation type: sco does not exist in the AMS."):
            self.driver.query_if_relation_exists(start_node_id=self.nodes[-1]['swai__identifier'],
                                                 end_node_id=sp_n['swai__identifier'],
                                                 relation='sco')
        # correct case
            self.assertIsInstance(
            self.driver.query_if_relation_exists(start_node_id=self.nodes[-1]['swai__identifier'],
                                                 end_node_id=sp_n['swai__identifier'],
                                                 relation='has language'), bool)

    def test_query_node_by_name_class(self):
        # None or "" for name label
        with self.assertRaisesRegex(ValueError,"name parameter has to be a non-empty String."):
            self.driver.query_node_by_name_class(name='', ont_class='Model', to_dict=False)
        with self.assertRaisesRegex(ValueError,"class_label parameter has to be a non-empty String."):
            self.driver.query_node_by_name_class(name='Example_0', ont_class=0, to_dict=False)
        with self.assertRaisesRegex(ValueError,"to_dict parameter has to be a boolean."):
            self.driver.query_node_by_name_class(name='Example_0', ont_class='Model', to_dict=0)
        # Ontology class wrong or None
        with self.assertRaisesRegex(ValueError, "{0} class its not in the database.".format("Modell")):
            self.driver.query_node_by_name_class(name='Example_0', ont_class="Modell", to_dict=False)
        # activate to dict to nonexistent node
        self.assertIsInstance(
            self.driver.query_node_by_name_class(name='Example_0', ont_class="Algorithm", to_dict=True), type(None))
        # Nonexistent node
        self.assertIsInstance(
            self.driver.query_node_by_name_class(name='Example_0', ont_class="Algorithm", to_dict=False), type(None))
        # Existent node
        self.assertIsInstance(
            self.driver.query_node_by_name_class(name='Example_0', ont_class="Model", to_dict=False), py2neo.Node)
        # existent node + to dict
        self.assertIsInstance(
            self.driver.query_node_by_name_class(name='Example_0', ont_class="Model", to_dict=False), dict)

    def test_query_node_by_id(self):
        node_id = self.nodes[0]['swai__identifier']
        # wrong ID
        with self.assertRaisesRegex(ValueError,
                                    "neo_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.query_node_by_id(neo_id=None, to_dict=False)
        with self.assertRaisesRegex(ValueError,
                                    "neo_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.query_node_by_id(neo_id=-1, to_dict=False)
        with self.assertRaisesRegex(ValueError,"to_dict parameter has to be a boolean."):
            self.driver.query_node_by_id(neo_id=node_id, to_dict=0)
        # activate to dict to nonexistent node
        self.assertIsInstance(self.driver.query_node_by_id(neo_id=self.generates_node.identity, to_dict=True), type(None))
        # Nonexistent node
        self.assertIsInstance(self.driver.query_node_by_id(neo_id=self.generates_node.identity, to_dict=False), type(None))
        # Existent node
        self.assertIsInstance( self.driver.query_node_by_id(neo_id=node_id, to_dict=False), py2neo.Node)
        # existent node + to dict
        self.assertIsInstance(self.driver.query_node_by_id(neo_id=node_id, to_dict=False), dict)

    def test_query_node_by_uuid(self):
        uuid = self.nodes[0]['swai__uuidIdentifier']
        # None or "" for name label
        with self.assertRaisesRegex(ValueError, "uuid parameter has to be a non-empty String."):
            self.driver.query_node_by_uuid(uuid_db='', ont_class='Model', to_dict=False)
        with self.assertRaisesRegex(ValueError, "class_label parameter has to be a non-empty String."):
            self.driver.query_node_by_uuid(uuid_db=uuid, ont_class=0, to_dict=False)
        with self.assertRaisesRegex(ValueError, "to_dict parameter has to be a boolean."):
            self.driver.query_node_by_uuid(uuid_db=uuid, ont_class='Model', to_dict=0)
            # Ontology class wrong or None
            with self.assertRaisesRegex(ValueError, "{0} class its not in the database.".format("Modell")):
                self.driver.query_node_by_uuid(uuid_db=uuid, ont_class="Modell", to_dict=False)
            # activate to dict to nonexistent node
            self.assertIsInstance(
                self.driver.query_node_by_uuid(uuid_db=uuid, ont_class="Algorithm", to_dict=True), type(None))
            # Nonexistent node
            self.assertIsInstance(
                self.driver.query_node_by_uuid(uuid_db=uuid, ont_class="Algorithm", to_dict=False),type(None))
            # Existent node
            self.assertIsInstance(
                self.driver.query_node_by_uuid(uuid_db=uuid, ont_class="Model", to_dict=False), py2neo.Node)
            # existent node + to dict
            self.assertIsInstance(
                self.driver.query_node_by_uuid(uuid_db=uuid, ont_class="Model", to_dict=False), dict)

    def test_query_instances_by_class(self):
        # Wrong format parameter checking
        with self.assertRaisesRegex(ValueError, "class_label parameter has to be a non-empty String."):
            self.driver.query_instances_by_class(class_label=0, with_inference=False, only_labels=True)
        with self.assertRaisesRegex(ValueError, "class_label parameter has to be a non-empty String."):
            self.driver.query_instances_by_class(class_label='', with_inference=False, only_labels=True)
        with self.assertRaisesRegex(ValueError, "with_inference parameter has to be a boolean."):
            self.driver.query_instances_by_class(class_label='Model', with_inference='str', only_labels=True)
        with self.assertRaisesRegex(ValueError, "only_labels parameter has to be a boolean."):
            self.driver.query_instances_by_class(class_label='Model', with_inference=False, only_labels=None)
        # Ontology class label wrong
        with self.assertRaisesRegex(ValueError, "Modell class its not in the database."):
            self.driver.query_instances_by_class(class_label='Modell', with_inference=False, only_labels=True)
        # empty class + only_labels = True
        self.assertIsInstance(
            self.driver.query_instances_by_class(class_label='Person', with_inference=False, only_labels=True), list)
        # Class with instances + with_inference = False + Only_labels = False
        self.assertIsInstance(
            self.driver.query_instances_by_class(class_label='Model', with_inference=False, only_labels=False), list)
        # Class with instances + with_inference = False + Only_labels = True
        self.assertIsInstance(
            self.driver.query_instances_by_class(class_label='Model', with_inference=False, only_labels=True), list)
        # Class with instances + with_inference = True + Only_labels = False
        self.assertIsInstance(
            self.driver.query_instances_by_class(class_label='AI Artifact', with_inference=True, only_labels=False), list)
        # Class with instances + with_inference = True + Only_labels = True
        self.assertIsInstance(
            self.driver.query_instances_by_class(class_label='AI Artifact', with_inference=True, only_labels=True), list)

    def test_query_class_by_id(self):
        cls_id = self.driver.free_query("MATCH (n:owl__Class {rdfs__label: 'Model'}) RETURN n;").data()[0]['n'].identity
        # wrong ID
        with self.assertRaisesRegex(ValueError,
                                    "neo_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.query_class_by_id(neo_id=None)
        with self.assertRaisesRegex(ValueError,
                                    "neo_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.query_class_by_id(neo_id=-1)
        # class ID
        with self.assertRaisesRegex(ValueError, "The ID does not belong to an instance in the database"):
            self.driver.query_class_by_id(neo_id=cls_id)
        # correct case
        self.assertIsInstance(self.driver.query_class_by_id(neo_id=self.nodes[0]['swai__identifier']), str)

    def test_query_related_nodes_by_relation(self):
        node_id = self.nodes[0]['swai__identifier']
        # wrong input parameters format
        with self.assertRaisesRegex(ValueError, "neo_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.query_related_nodes_by_relation(neo_id=1.3, relation='', to_dict=False)
        with self.assertRaisesRegex(ValueError, "relation parameter has to be a non-empty String."):
            self.driver.query_related_nodes_by_relation(neo_id=0, relation='', to_dict=False)
        with self.assertRaisesRegex(ValueError, "to_dict parameter has to be a boolean."):
            self.driver.query_related_nodes_by_relation(neo_id=0, relation='has language', to_dict=1)
        # incorrect id
        with self.assertRaisesRegex(ValueError, "There is not a node in the AMS with 30000000000 as ID"):
            self.driver.query_related_nodes_by_relation(neo_id=30000000000, relation='ha', to_dict=False)
        # incorrect relation
        with self.assertRaisesRegex(ValueError, "The relation type: ha does not exist in the AMS."):
            self.driver.query_related_nodes_by_relation(neo_id=node_id, relation='ha', to_dict=False)
        # relation out of domain
        with self.assertRaisesRegex(ValueError, "Model class it's not in the domain of the relation: acquires"):
            self.driver.query_related_nodes_by_relation(neo_id=node_id, relation='acquires', to_dict=False)
        # empty case
        self.assertIsInstance(
            self.driver.query_related_nodes_by_relation(neo_id=node_id, relation='distributed as', to_dict=True), list)
        # correct case
        self.assertIsInstance(
            self.driver.query_related_nodes_by_relation(neo_id=node_id, relation='has language', to_dict=True), list)

    def test_query_get_artifacts_by_tag(self):
        # wrong parameter format
        with self.assertRaisesRegex(ValueError,
                                    "limit parameter has the wrong format, it has to be an integer and  > 0."):
            self.driver.query_get_artifacts_by_tag(categories=['Searching', 'Machine learning'], limit=0,
                                                   inference_cat=False, to_dict=False)
        with self.assertRaisesRegex(ValueError,
                                    "limit parameter has the wrong format, it has to be an integer and  > 0."):
            self.driver.query_get_artifacts_by_tag(categories=['Searching', 'Machine learning'], limit=-1,
                                                   inference_cat=False, to_dict=False)
        with self.assertRaisesRegex(ValueError,
                                    "limit parameter has the wrong format, it has to be an integer and  > 0."):
            self.driver.query_get_artifacts_by_tag(categories=['Searching', 'Machine learning'], limit='',
                                                   inference_cat=False, to_dict=False)
        with self.assertRaisesRegex(ValueError, "categories parameter has to be a list."):
            self.driver.query_get_artifacts_by_tag(categories=1, limit=1, inference_cat=False, to_dict=False)
        with self.assertRaisesRegex(ValueError, "to_dict parameter has to be a boolean."):
            self.driver.query_get_artifacts_by_tag(categories=['Searching', 'Machine learning'], limit=1,
                                                   inference_cat=False, to_dict='False')
        with self.assertRaisesRegex(ValueError, "inference_cat parameter has to be a boolean."):
            self.driver.query_get_artifacts_by_tag(categories=['Searching', 'Machine learning'], limit=1,
                                                   inference_cat="False", to_dict=False)
        # categories has a wrong category
        with self.assertRaisesRegex(ValueError,
                                    "All the categories passed should be AI Techniques, Machine learn is not one"):
            self.driver.query_get_artifacts_by_tag(categories=['Searching', 'Machine learn'], limit=1,
                                                   inference_cat=False, to_dict=False)
        # inference_cat = False & to_dict = False
        self.assertIsInstance(
            self.driver.query_get_artifacts_by_tag(categories=['Searching', 'Machine learning'], limit=5,
                                                   inference_cat=False, to_dict=False), list)
        # inference_cat = False & to_dict = True
        self.assertIsInstance(
            self.driver.query_get_artifacts_by_tag(categories=['Searching', 'Machine learning'], limit=5,
                                                   inference_cat=False, to_dict=True), list)
        # inference_cat = True & to_dict = False
        self.assertIsInstance(
            self.driver.query_get_artifacts_by_tag(categories=['Searching', 'Machine learning'], limit=5,
                                                   inference_cat=True, to_dict=False), list)
        # inference_cat = True & to_dict = True
        self.assertIsInstance(
            self.driver.query_get_artifacts_by_tag(categories=['Searching', 'Machine learning'], limit=5,
                                                   inference_cat=True, to_dict=True), list)

    def test_query_get_experts_by_tag(self):
        # wrong parameter format
        with self.assertRaisesRegex(ValueError,
                                    "limit parameter has the wrong format, it has to be an integer and  > 0."):
            self.driver.query_get_experts_by_tag(categories=['Searching', 'Machine learning'], limit=0,
                                                 inference_cat=False, to_dict=False)
        with self.assertRaisesRegex(ValueError,
                                    "limit parameter has the wrong format, it has to be an integer and  > 0."):
            self.driver.query_get_experts_by_tag(categories=['Searching', 'Machine learning'], limit=-1,
                                                 inference_cat=False, to_dict=False)
        with self.assertRaisesRegex(ValueError,
                                    "limit parameter has the wrong format, it has to be an integer and  > 0."):
            self.driver.query_get_experts_by_tag(categories=['Searching', 'Machine learning'], limit='',
                                                 inference_cat=False, to_dict=False)
        with self.assertRaisesRegex(ValueError, "categories parameter has to be a list."):
            self.driver.query_get_experts_by_tag(categories=1, limit=1, inference_cat=False, to_dict=False)
        with self.assertRaisesRegex(ValueError, "to_dict parameter has to be a boolean."):
            self.driver.query_get_experts_by_tag(categories=['Searching', 'Machine learning'], limit=1,
                                                 inference_cat=False, to_dict='False')
        with self.assertRaisesRegex(ValueError, "inference_cat parameter has to be a boolean."):
            self.driver.query_get_experts_by_tag(categories=['Searching', 'Machine learning'], limit=1,
                                                 inference_cat="False", to_dict=False)
        # categories has a wrong category
        with self.assertRaisesRegex(ValueError,
                                    "All the categories passed should be AI Techniques, Machine learn is not one"):
            self.driver.query_get_experts_by_tag(categories=['Searching', 'Machine learn'], limit=1,
                                                 inference_cat=False, to_dict=False)
        # inference_cat = False & to_dict = False
        self.assertIsInstance(
            self.driver.query_get_experts_by_tag(categories=['Searching', 'Machine learning'], limit=5,
                                                 inference_cat=False, to_dict=False), list)
        # inference_cat = False & to_dict = True
        self.assertIsInstance(
            self.driver.query_get_experts_by_tag(categories=['Searching', 'Machine learning'], limit=5,
                                                 inference_cat=False, to_dict=True), list)
        # inference_cat = True & to_dict = False
        self.assertIsInstance(
            self.driver.query_get_experts_by_tag(categories=['Searching', 'Machine learning'], limit=5,
                                                 inference_cat=True, to_dict=False), list)
        # inference_cat = True & to_dict = True
        self.assertIsInstance(
            self.driver.query_get_experts_by_tag(categories=['Searching', 'Machine learning'], limit=5,
                                                 inference_cat=True, to_dict=True), list)

    def test_query_topk_assets(self):
        # wrong parameter format
        with self.assertRaisesRegex(ValueError,
                                    "k parameter has the wrong format, it has to be an integer and  > 0."):
            self.driver.query_topk_assets(category='Model', k=0, to_dict=False)
        with self.assertRaisesRegex(ValueError,
                                    "k parameter has the wrong format, it has to be an integer and  > 0."):
            self.driver.query_topk_assets(category='Model', k=-1, to_dict=False)
        with self.assertRaisesRegex(ValueError,
                                    "k parameter has the wrong format, it has to be an integer and  > 0."):
            self.driver.query_topk_assets(category='Model', k="", to_dict=False)
        with self.assertRaisesRegex(ValueError,
                                    "category parameter has to be a non-empty String."):
            self.driver.query_topk_assets(category=0, k=1, to_dict=False)
        with self.assertRaisesRegex(ValueError,
                                    "category parameter has to be a non-empty String."):
            self.driver.query_topk_assets(category='', k=1, to_dict=False)
        with self.assertRaisesRegex(ValueError,
                                    "to_dict parameter has to be a boolean."):
            self.driver.query_topk_assets(category='Model', k=1, to_dict=1)
        # erroneous class label
        with self.assertRaisesRegex(ValueError,
                                    "The category passed should be AI Technique, Modell is not one."):
            self.driver.query_topk_assets(category='Modell', k=1, to_dict=False)
        # Non AITechnique class label
        with self.assertRaisesRegex(ValueError,
                                    "The category passed should be AI Technique, Model is not one."):
            self.driver.query_topk_assets(category='Model', k=1, to_dict=False)
        # Empty output + to_dict == True
        self.assertIsInstance(self.driver.query_topk_assets(category='Optimisation', k=1, to_dict=True), list)
        # k = 1 + to_dict == True
        self.assertIsInstance(self.driver.query_topk_assets(category='Searching', k=1, to_dict=True), list)
        # k = 5 + to_dict == True
        self.assertIsInstance(self.driver.query_topk_assets(category='Searching', k=5, to_dict=True), list)
        # k = 5 + to_dict == False
        self.assertIsInstance(self.driver.query_topk_assets(category='Searching', k=5, to_dict=True), list)

