import unittest
import os
import random
from ams_api.StairwAIDriver import Driver
import warnings
import time


def populate_kg(self_driver: Driver):
    self_ins_dict = {}
    # insert all the instantiable classes example nodes
    for in_cls in self_driver.get_instantiable_classes():
        att_all = [a['property'] for a in self_driver.get_class_properties(class_type=in_cls)['Attributes']['properties']]
        # get the full attribute parameter dictionary
        param_dict = {att: 'Att_Example' for att in att_all if att not in ['identifier', 'uuid identifier', 'name']}
        param_dict['name'] = 'Example_' + in_cls
        self_ins_dict[in_cls] = self_driver.insert_instance(class_type=in_cls, param_dict=param_dict)

    all_cls_dict = {key: [node['rdfs__label']] for key, node in self_ins_dict.items()}
    all_cls_dict['AI Technique'] = self_driver.query_instances_by_class(class_label='AI Technique', with_inference=True, only_labels=True)
    all_cls_dict['Business categories'] = self_driver.query_instances_by_class(class_label='Business categories', with_inference=True, only_labels=True)
    for cls in self_driver.get_classes():
        if cls not in all_cls_dict.keys():
            ins = self_driver.query_instances_by_class(class_label=cls, with_inference=False, only_labels=True)
            if cls in self_driver.get_aitechniques() + self_driver.get_business_categories():
                continue
            elif len(ins) > 0:
                all_cls_dict[cls] = ins

    for cls, node in self_ins_dict.items():
        rel_all = [(a['property'], a['range']) for a in self_driver.get_class_properties(class_type=cls)['Relations']['properties']]
        param_dict = {rel[0]: [] for rel in rel_all}
        for rel, rang in rel_all:
            if 'AI Technique' in rang or 'Business categories' in rang:
                if 'AI Technique' in rang:
                    param_dict[rel] += all_cls_dict['AI Technique']
                if 'Business categories' in rang:
                    param_dict[rel] += all_cls_dict['Business categories']
                param_dict[rel] = random.sample(param_dict[rel], k=5)
                if rel == 'scored in':
                    score_dict = {cat: random.random() for cat in param_dict[rel]}
                    param_dict[rel] = score_dict
            else:
                aux = []
                for rg in rang:
                    if rg in all_cls_dict.keys():
                        end_label = random.choice(all_cls_dict[rg])
                        if not self_driver.query_if_relation_exists(node['swai__identifier'], rel, self_driver.query_node_by_name_class(end_label, rg)['swai__identifier']):
                            aux.append(end_label)
                param_dict[rel] = aux
        self_driver.modify_instance(neo_id=node['swai__identifier'], add_dict=param_dict)
    return self_ins_dict


class TestAPIBackUp(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.driver = Driver()
        warnings.filterwarnings("ignore", category=ResourceWarning)  # Suppress ResourceWarnings

    @classmethod
    def tearDownClass(cls):
        time.sleep(2)  # Delay for 5 seconds to have time to close all the socket connections

    def test_hard_db_reset(self):
        # create a temporal folder to put a copy of the original database instances
        print("Generating a copy of the current database instance.")
        os.makedirs('./test_reset', exist_ok=True)
        self.driver.create_backup(path="./test_reset")

        # initialize an empty database and fill with test cases
        print("\nRestarting the database and inserting the test cases.")
        self.driver.hard_db_reset(with_backup=False)
        self.ins_dict = populate_kg(self.driver)

        # generate a hard reset with the back-up
        print("\nGenerating a complete hard reset with back-up")
        self.driver.hard_db_reset(with_backup=True)

        # Empty again the database and inserting the old instance instances
        print("\nBack-up function test end correctly, deleting test cases and inserting old instances.")
        self.driver.hard_db_reset(with_backup=False)
        self.driver.read_backup("./test_reset")
        os.rmdir('./test_reset')
