import unittest
import pandas
import py2neo
import time
from ams_api.StairwAIDriver import Driver


class TestAPIGetters(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        time.sleep(2)  # Delay for 2 seconds to have time to close all the socket connections
        cls.driver = Driver()
        # insert a test case instance
        expert_params = {'name': 'Example',
                         'short description': "Example of description",
                         'keyword': 'Example',
                         'has language': 'Greek',
                         'has organisation type': 'Startup company',
                         'has educational level': 'Master',
                         'resides': 'Kazakhstan',
                         'experience in': ['Agriculture', 'Energy'],
                         'scored in': {'Searching': 0.9, 'Machine learning': 0.8}
                         }
        cls.test_node = cls.driver.insert_instance(class_type='AI expert', param_dict=expert_params)

    @classmethod
    def tearDownClass(cls):
        # delete the test case instance
        cls.driver.delete_instance(neo_id=cls.test_node.identity)

    def test_get_classes(self):
        # with descriptions included
        self.assertIsInstance(self.driver.get_classes(with_descriptions=True), dict)
        # with no descriptions included
        self.assertIsInstance(self.driver.get_classes(with_descriptions=False), list)

    def test_get_instantiable_classes(self):
        # with descriptions included
        self.assertIsInstance(self.driver.get_instantiable_classes(with_descriptions=True), dict)
        # with no descriptions included
        self.assertIsInstance(self.driver.get_instantiable_classes(with_descriptions=False), list)

    def test_get_attributes(self):
        # with the conditions included
        self.assertIsInstance(self.driver.get_attributes(with_conditions=True), pandas.DataFrame)
        # with the conditions no included
        self.assertIsInstance(self.driver.get_attributes(with_conditions=False), list)

    def test_get_relations(self):
        # with the conditions included
        self.assertIsInstance(self.driver.get_relations(with_conditions=True), pandas.DataFrame)
        # with the conditions no included
        self.assertIsInstance(self.driver.get_relations(with_conditions=False), list)

    def test_get_class_properties(self):
        # non-existent class
        with self.assertRaisesRegex(ValueError, "Trying to extract properties from non-existent class."):
            self.driver.get_class_properties('')
        # extra class
        with self.assertRaisesRegex(ValueError, "Trying to extract properties from non-existent class."):
            self.driver.get_class_properties('Organization')
        # non-instantiable class
        self.assertIsInstance(self.driver.get_class_properties('Person'), dict)
        # instantiable class
        self.assertIsInstance(self.driver.get_class_properties('Machine learning'), dict)

    def test_get_node_data_to_dict(self):
        # Dirty node insertion
        with self.assertRaisesRegex(ValueError, "The node does not belong to this graph."):
            self.driver.get_node_data_to_dict(py2neo.Node("Person", name="Alice", age=30))
        # Correct case
        self.assertIsInstance(self.driver.get_node_data_to_dict(self.test_node), dict)

    def test_get_business_categories(self):
        # with descriptions included
        self.assertIsInstance(self.driver.get_business_categories(with_description=True), list)
        # with descriptions no included
        self.assertIsInstance(self.driver.get_business_categories(with_description=False), list)

    def test_get_aitechniques(self):
        # with descriptions included
        self.assertIsInstance(self.driver.get_aitechniques(with_description=True), list)
        # with no descriptions included
        self.assertIsInstance(self.driver.get_aitechniques(with_description=False), list)

    def test_get_aiexperts(self):
        # uuid = None and swai_id_only = False
        self.assertIsInstance(self.driver.get_aiexperts(), list)
        # uuid = Incorrect id and swai_id_only = False
        with self.assertRaisesRegex(ValueError, "The UUID parameter does not correspond to any database instance."):
            self.driver.get_aiexperts(uuid_identifier='00000', swai_id_only=False)
        # uuid = Incorrect id and swai_id_only = True
        with self.assertRaisesRegex(ValueError, "The UUID parameter does not correspond to any database instance."):
            self.driver.get_aiexperts(uuid_identifier='00000', swai_id_only=True)
        # uuid = Correct id and swai_id_only = False
        self.assertIsInstance(
            self.driver.get_aiexperts(uuid_identifier=self.test_node['swai__uuidIdentifier'], swai_id_only=False), dict)
        # uuid = Correct id and swai_id_only = True
        self.assertIsInstance(
            self.driver.get_aiexperts(uuid_identifier=self.test_node['swai__uuidIdentifier'], swai_id_only=True), int)

    def test_get_educational_resources(self):
        # uuid = None and swai_id_only = False
        self.assertIsInstance(self.driver.get_educational_resources(), list)
        # uuid = Incorrect id and swai_id_only = False
        with self.assertRaisesRegex(ValueError, "The UUID parameter does not correspond to any database instance."):
            self.driver.get_educational_resources(uuid_identifier='00000', swai_id_only=False)
        # uuid = Incorrect id and swai_id_only = True
        with self.assertRaisesRegex(ValueError, "The UUID parameter does not correspond to any database instance."):
            self.driver.get_educational_resources(uuid_identifier='00000', swai_id_only=True)
        # uuid = Correct id and swai_id_only = False
        edu_node = self.driver.insert_instance(class_type='Educational Resource', param_dict={'name': 'Example'})
        self.assertIsInstance(
            self.driver.get_educational_resources(uuid_identifier=edu_node['swai__uuidIdentifier'], swai_id_only=False), dict)
        # uuid = Correct id and swai_id_only = True
        self.assertIsInstance(
            self.driver.get_educational_resources(uuid_identifier=edu_node['swai__uuidIdentifier'], swai_id_only=True), int)
        self.driver.delete_instance(neo_id=edu_node.identity)

    def test_get_countries(self):
        # correct case
        self.assertIsInstance(self.driver.get_countries(), list)

    def test_get_organisation_types(self):
        # correct case
        self.assertIsInstance(self.driver.get_organisation_types(), list)

    def test_get_containers(self):
        # correct case
        self.assertIsInstance(self.driver.get_containers(), list)

    def test_get_distributions(self):
        # correct case
        self.assertIsInstance(self.driver.get_distributions(), list)

    def test_get_paces(self):
        # correct case
        self.assertIsInstance(self.driver.get_paces(), list)

    def test_get_languages(self):
        # correct case
        self.assertIsInstance(self.driver.get_languages(), list)

    def test_get_education_levels(self):
        # correct case
        self.assertIsInstance(self.driver.get_education_levels(), list)

    def test_get_education_types(self):
        # correct case
        self.assertIsInstance(self.driver.get_education_types(), list)

    def test_get_skills(self):
        # correct case
        self.assertIsInstance(self.driver.get_skills(), list)
