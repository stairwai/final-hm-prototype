import unittest
import random
import time
from ams_api.StairwAIDriver import Driver


def populate_kg(self_driver: Driver):
    self_ins_dict = {}
    # insert all the instantiable classes example nodes
    for in_cls in self_driver.get_instantiable_classes():
        att_all = [a['property'] for a in self_driver.get_class_properties(class_type=in_cls)['Attributes']['properties']]
        # get the full attribute parameter dictionary
        param_dict = {att: 'Att_Example' for att in att_all if att not in ['identifier', 'uuid identifier', 'name']}
        param_dict['name'] = 'Example_' + in_cls
        self_ins_dict[in_cls] = self_driver.insert_instance(class_type=in_cls, param_dict=param_dict)

    all_cls_dict = {key: [node['rdfs__label']] for key, node in self_ins_dict.items()}
    all_cls_dict['AI Technique'] = self_driver.query_instances_by_class(class_label='AI Technique', with_inference=True, only_labels=True)
    all_cls_dict['Business categories'] = self_driver.query_instances_by_class(class_label='Business categories', with_inference=True, only_labels=True)
    for cls in self_driver.get_classes():
        if cls not in all_cls_dict.keys():
            ins = self_driver.query_instances_by_class(class_label=cls, with_inference=False, only_labels=True)
            if cls in self_driver.get_aitechniques() + self_driver.get_business_categories():
                continue
            elif len(ins) > 0:
                all_cls_dict[cls] = ins

    for cls, node in self_ins_dict.items():
        rel_all = [(a['property'], a['range']) for a in self_driver.get_class_properties(class_type=cls)['Relations']['properties']]
        param_dict = {rel[0]: [] for rel in rel_all}
        for rel, rang in rel_all:
            if 'AI Technique' in rang or 'Business categories' in rang:
                if 'AI Technique' in rang:
                    param_dict[rel] += all_cls_dict['AI Technique']
                if 'Business categories' in rang:
                    param_dict[rel] += all_cls_dict['Business categories']
                param_dict[rel] = random.sample(param_dict[rel], k=5)
                if rel == 'scored in':
                    score_dict = {cat: random.random() for cat in param_dict[rel]}
                    param_dict[rel] = score_dict
            else:
                aux = []
                for rg in rang:
                    if rg in all_cls_dict.keys():
                        end_label = random.choice(all_cls_dict[rg])
                        if not self_driver.query_if_relation_exists(node['swai__identifier'], rel, self_driver.query_node_by_name_class(end_label, rg)['swai__identifier']):
                            aux.append(end_label)
                param_dict[rel] = aux
        self_driver.modify_instance(neo_id=node['swai__identifier'], add_dict=param_dict)
    return self_ins_dict


def unpopulate_kg(self_driver: Driver, ins_dict: dict):
    for cls, node in ins_dict.items():
        self_driver.delete_instance(neo_id=node['swai__identifier'])


class TestAPICreators(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        time.sleep(2)  # Delay for 2 seconds to have time to close all the socket connections
        cls.driver = Driver()
        # insert the special test case instance
        expert_params = {'name': 'Example',
                         'short description': "Example of description",
                         'has language': 'Greek',
                         'has organisation type': 'Startup company',
                         'has educational level': 'Master',
                         'resides': 'Kazakhstan',
                         'experience in': ['Agriculture', 'Energy'],
                         'scored in': {'Searching': 0.9, 'Machine learning': 0.8}
                         }
        cls.test_node = cls.driver.insert_instance(class_type='AI expert', param_dict=expert_params)
        cls.ins_dict = {}

    @classmethod
    def tearDownClass(cls):
        # delete the special test case instance
        cls.driver.delete_instance(neo_id=cls.test_node.identity)

    def test_insert_instance(self):
        # Wrong parameters format
        with self.assertRaisesRegex(ValueError, "class_type parameter has to be a non-empty String."):
            self.driver.insert_instance(class_type='', param_dict={})
        with self.assertRaisesRegex(ValueError, "class_type parameter has to be a non-empty String."):
            self.driver.insert_instance(class_type=0, param_dict={})
        with self.assertRaisesRegex(ValueError, "param_dict parameter has the wrong format, it has to be dictionary."):
            self.driver.insert_instance(class_type='Model', param_dict=0)
        with self.assertRaisesRegex(ValueError, "'name' field in param_dict is mandatory to insert a new instance."):
            self.driver.insert_instance(class_type='Model', param_dict={})
        with self.assertRaisesRegex(ValueError, "'name' field in param_dict has to be a none empty string."):
            self.driver.insert_instance(class_type='Model', param_dict={'name': 0})
        with self.assertRaisesRegex(ValueError, "'name' field in param_dict has to be a none empty string."):
            self.driver.insert_instance(class_type='Model', param_dict={'name': ''})
        # insert a non-existent class
        with self.assertRaisesRegex(ValueError, "Modell class instances cannot be instantiated or deleted, only instantiable classes can."):
            self.driver.insert_instance(class_type='Modell', param_dict={'name': 'Example1'})
        # insert a non-instantiable class
        with self.assertRaisesRegex(ValueError,
                                    "Language class instances cannot be instantiated or deleted, only instantiable classes can."):
            self.driver.insert_instance(class_type='Language', param_dict={'name': 'Example1'})
        # insert a non-existent class
        with self.assertRaisesRegex(ValueError,
                                    "Modell class instances cannot be instantiated or deleted, only instantiable classes can."):
            self.driver.insert_instance(class_type='Modell', param_dict={'name': 'Example1'})
        # instantiate specifying the id
        with self.assertRaisesRegex(ValueError,
                                    "identifier is a primary attribute it can't be modified."):
            self.driver.insert_instance(class_type='Model', param_dict={'name': 'Example1', 'identifier': 0})
        # instantiate with wrong property
        with self.assertRaises(ValueError):
            self.driver.insert_instance(class_type='Model', param_dict={'name': 'Example1', 'uuid_identifier': '0'})
        # insert a node of each instantiable class with all the possible properties (correct case for all the instances)
        print("Populating the database classes with fully connected instances.")
        self.ins_dict = populate_kg(self.driver)
        # delete a node of each instantiable class with all the possible properties (correct case for all the instances)
        unpopulate_kg(self_driver=self.driver, ins_dict=self.ins_dict)

    def test_delete_instance(self):
        # Wrong parameter format
        with self.assertRaisesRegex(ValueError, "neo_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.delete_instance(neo_id=-1)
        with self.assertRaisesRegex(ValueError, "neo_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.delete_instance(neo_id='')
        # Delete a non-existent node
        with self.assertRaisesRegex(ValueError, "Deletion cancelled, the following instance ID does not belongs to any instance in the Database: 3000000"):
            self.driver.delete_instance(neo_id=3000000)
        # Delete a non instantiable instance
        nn_id = self.driver.query_node_by_name_class(name='Machine learning', ont_class='Machine learning')['swai__identifier']
        with self.assertRaisesRegex(ValueError, "Machine learning class instances cannot be instantiated or deleted, only instantiable classes can."):
            self.driver.delete_instance(neo_id=nn_id)
        # Delete a non Class or instance node
        gen_id = self.driver.free_query("MATCH (n:owl__ObjectProperty {rdfs__label: 'generates'}) RETURN n;").data()[0]['n'].identity
        with self.assertRaisesRegex(ValueError,
                                    'Deletion cancelled, the following instance ID does not belongs to any instance in the Database: ' + str(gen_id)):
            self.driver.delete_instance(gen_id)
        # Delete a Class
        cls_id = self.driver.free_query("MATCH (n:owl__Class {rdfs__label: 'Model'}) RETURN n;").data()[0]['n'].identity
        with self.assertRaisesRegex(ValueError, "The node ID {0} does not belong to an instance node.".format(str(cls_id))):
            self.driver.delete_instance(neo_id=cls_id)

    def test_modify_instance(self):
        cls_id = self.driver.free_query("MATCH (n:owl__Class {rdfs__label: 'Model'}) RETURN n;").data()[0]['n'].identity
        node_id = self.test_node['swai__identifier']
        clus_id = self.driver.query_node_by_name_class(name='Clustering', ont_class='Clustering')['swai__identifier']
        # self.driver.modify_instance(neo_id=0, add_dict={}, delete_dict={})
        # Modify a class
        with self.assertRaisesRegex(ValueError, 'Relation insertion is cancelled, the following instance ID does not belongs to any instance in the Database: ' + str(cls_id)):
            self.driver.modify_instance(neo_id=cls_id, add_dict={'has language': 'French'})
        # delete a relation that is not in the database
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, delete_dict={'has language': 'French'})
        # delete and add a relation in the same call
        frc_id = self.driver.query_node_by_name_class(name='French', ont_class='Language')['swai__identifier']
        with self.assertRaisesRegex(ValueError, "The relation: has language between the nodes with the IDs: "+str(node_id)+" and "+str(frc_id)+" can't be deleted because does not exist."):
            self.driver.modify_instance(neo_id=node_id, add_dict={'has language': 'French'}, delete_dict={'has language': 'French'})
        # delete an attribute that is not in the database
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, delete_dict={'keyworde': 'French'})
        # delete and add an attribute in the same call
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, add_dict={'keyword': 'French'}, delete_dict={'keyword': 'French'})
        # add a non-existent relation to the instance
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, add_dict={'hasa language': 'French'})
        # add an incorrect (not in the domain) relation to the instance
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, add_dict={'runs on': 'French'})
        # add a non-existent attribute to the instance
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, add_dict={'hyperparame': 'French'})
        # add an incorrect (not in the domain) attribute to the instance
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, add_dict={'hyperparams': 'French'})
        # delete a non-existent relation to the instance
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, delete_dict={'hasa language': 'French'})
        # delete an incorrect (not in the domain) relation to the instance
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, delete_dict={'runs on': 'French'})
        # add a correct relation with correct start node id but non-existent end node label
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, add_dict={'has language': 'Frencheeeze'})
        # add a correct relation with correct start node id but out of range node label
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, add_dict={'has language': 'Machine learning'})
        # delete a correct relation with correct start node id but non-existent end node label
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, delete_dict={'has language': 'Frencheeeze'})
        # delete a correct relation with correct start node id but out of range node label
        with self.assertRaises(ValueError):
            self.driver.modify_instance(neo_id=node_id, delete_dict={'has language': 'Machine learning'})
        # Correct: add and delete correct relations/attributes
        self.driver.modify_instance(neo_id=node_id, add_dict={'has language': 'Spanish'}, delete_dict={'scored in': ['Searching', 'Machine learning']})

    def test_modify_score(self):
        cls_id = self.driver.free_query("MATCH (n:owl__Class {rdfs__label: 'Model'}) RETURN n;").data()[0]['n'].identity
        node_id = self.test_node['swai__identifier']
        clus_id = self.driver.query_node_by_name_class(name='Clustering', ont_class='Clustering')['swai__identifier']
        # Wrong parameter format
        with self.assertRaisesRegex(ValueError, "start_node_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.modify_score(start_node_id=-1, end_node_id=0, score=0.1, delete_property=True)
        with self.assertRaisesRegex(ValueError, "score parameter has the wrong format, it has to be float and  >= 0."):
            self.driver.modify_score(start_node_id=0, end_node_id=0, score=-1, delete_property=True)
        with self.assertRaisesRegex(ValueError, "end_node_id parameter has the wrong format, it has to be an integer and  >= 0."):
            self.driver.modify_score(start_node_id=0, end_node_id=None, score=1, delete_property=True)
        with self.assertRaisesRegex(ValueError, "delete_property parameter has to be a boolean."):
            self.driver.modify_score(start_node_id=0, end_node_id=0, score=1, delete_property=0)
        # non-existent starting node id
        with self.assertRaisesRegex(ValueError, "start_node_id does not correspond to any existent instance."):
            self.driver.modify_score(start_node_id=3000000, end_node_id=clus_id, score=1.0, delete_property=False)
        # non-existent ending node id
        with self.assertRaisesRegex(ValueError, "Relation insertion is cancelled, the following instance ID does not belongs to any instance in the Database: 300000000"):
            self.driver.modify_score(start_node_id=clus_id, end_node_id=300000000, score=1.0, delete_property=False)
        # Out of domain starting node id
        with self.assertRaisesRegex(ValueError, "Relation insertion is cancelled, the following instance ID does not belongs to any instance in the Database: {0}".format(cls_id)):
            self.driver.modify_score(start_node_id=cls_id, end_node_id=node_id, score=1.0, delete_property=False)
        # Out of range ending node id
        with self.assertRaisesRegex(ValueError, "Relation insertion is cancelled, the following instance ID does not belongs to any instance in the Database: {0}".format(cls_id)):
            self.driver.modify_score(start_node_id=node_id, end_node_id=cls_id, score=1.0, delete_property=False)
        # No score specified
        with self.assertRaisesRegex(ValueError, "The relation scored in from {0} to {1} requires a valid score value.".format(node_id, clus_id)):
            self.driver.modify_score(start_node_id=node_id, end_node_id=clus_id, score=None, delete_property=False)
        # correct case, insert
        self.driver.modify_score(start_node_id=node_id, end_node_id=clus_id, score=0.1, delete_property=False)
        # Insert an already existent score (Automatically substitute with the new value)
        self.driver.modify_score(start_node_id=node_id, end_node_id=clus_id, score=0.1, delete_property=False)
        # correct case, delete
        self.driver.modify_score(start_node_id=node_id, end_node_id=clus_id, score=0.1, delete_property=True)
        # Delete a non-existent score relation
        with self.assertRaisesRegex(ValueError, "The relation: scored in between the nodes with the IDs: {0} and {1} can't be deleted because does not exist.".format(node_id, clus_id)):
            self.driver.modify_score(start_node_id=node_id, end_node_id=clus_id, score=0.1, delete_property=True)

    def test_score_list_modifier(self):
        # Wrong parameter format
        with self.assertRaisesRegex(ValueError, "score_list parameter has to be a list."):
            self.driver.score_list_modifier(score_list=0)
        # Wrong list elements format
        with self.assertRaisesRegex(ValueError, "Score list modification stopped do to an incorrect parameter format in almost one of the score dictionaries of the list."):
            self.driver.score_list_modifier(score_list=[1, 2])
        # Wrong dictionary keys format
        with self.assertRaisesRegex(ValueError, "Score list modification stopped do to an incorrect parameter format in almost one of the score dictionaries of the list."):
            self.driver.score_list_modifier(score_list=self.driver.score_list_modifier(score_list=[{'node_id': 0, 'end_node_id': 0, 'score': 0.1, 'delete_property': False}]))
        # wrong delete_property element in a score_list element
        with self.assertRaisesRegex(ValueError, "Score list element dictionary has delete_property as a non-boolean."):
            self.driver.score_list_modifier(score_list=[{'start_node_id': 0, 'end_node_id': 0, 'score': 0.1, 'delete_property': 0}])
        # Delete and insertion correct case
        node_id = self.test_node['swai__identifier']
        clus_id = self.driver.query_node_by_name_class(name='Clustering', ont_class='Clustering')['swai__identifier']
        del_dict = {'start_node_id': clus_id, 'end_node_id': node_id, 'score': None, 'delete_property': True}
        insert_dict = {'start_node_id': node_id, 'end_node_id': clus_id, 'score': 0.1, 'delete_property': False}
        self.driver.score_list_modifier(score_list=[insert_dict, del_dict])
