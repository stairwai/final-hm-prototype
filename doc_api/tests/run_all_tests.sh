#!/bin/bash

# Set PYTHONPATH
python set_pythonpath.py

# Now run your tests
python -m unittest discover -s tests
