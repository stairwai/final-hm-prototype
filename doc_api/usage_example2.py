from ams_api.StairwAIDriver import Driver

# init the driver
driver = Driver()
# back-up example
# driver.hard_db_reset(with_backup=False)

# getter example
print(driver.get_instantiable_classes())

# Creators example (insert)
expert_params = {'name': 'Example',
                 'short description': "Example of description",
                 'has language': 'Greek',
                 'has organisation type': 'Startup company',
                 'has educational level': 'Master',
                 'resides': 'Kazakhstan',
                 'experience in': ['Agriculture', 'Energy'],
                 'scored in': {'Searching': 0.9, 'Machine learning': 0.8}
                 }
test_node = driver.insert_instance(class_type='AI expert', param_dict=expert_params)
# Creators example (modify)
driver.modify_instance(neo_id=test_node['swai__identifier'], add_dict={'has language': 'Spanish'}, delete_dict={'scored in': ['Searching', 'Machine learning']})

# Queries example
print(driver.query_instances_by_class(class_label='AI expert', with_inference=True, only_labels=True))

# Creators example (delete)
driver.delete_instance(neo_id=test_node.identity)
