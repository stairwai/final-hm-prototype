import os
from util import ssac
from doc_api.ams_api.StairwAIDriver import Driver
from argparse import ArgumentParser
from core.commands import setup_registry
import glob
import time
import json
import shutil
import ntpath

if __name__ == '__main__':
    setup_registry(directory=os.getcwd())

    parser = ArgumentParser()
    parser.add_argument('--source', '-s', default='use-cases-3rd-open-call-tokens', type=str)
    args = parser.parse_args()

    # Loop over all the stored use case files
    uc_dir_glob = os.path.join('data', 'queries', args.source, '*.json')
    lines = []
    for fname in glob.glob(uc_dir_glob):
        # Retrieve use case data from the SSAC file
        to_load = False
        with open(fname) as fp:
            # Retrive SSAC data for this expert
            ucdata = json.load(fp)
            # Check whether a "name" field is present
            if 'name' in ucdata:
                ucname = ucdata['name']
                token, _ = os.path.splitext(ntpath.basename(fname))
                lines.append(f'{ucname},http://127.0.0.1:5000/hm_query_usecase_gui?token={token}\n')
                # Mark the token as to load
                to_load = True
            # Update the UUID field name, if needed
            if 'swai__uuid_identifier' in ucdata:
                uuid = ucdata['swai__uuid_identifier']
                del ucdata['swai__uuid_identifier']
                ucdata['uuid identifier'] = uuid
        # Write back the changes
        if to_load:
            ofname = os.path.join('data', 'ssac', 'matchmaking_usecase', ntpath.basename(fname))
            with open(ofname, 'w') as fp:
                json.dump(ucdata, fp)

    # Write all tokens to the token file
    token_fname = os.path.join('data', 'usecase_tokens.csv')
    with open(token_fname, 'w') as fp:
        fp.writelines(lines)
