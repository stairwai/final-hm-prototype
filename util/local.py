#!/bin/python3

# from labeler import base as lbase
from labeler import labeler as lbase
import json
import pandas as pd
from matchmaking import base as mbase
# from labeler.unibo.utility.log_utils import Logger
from utility.log_utils import Logger
# from labeler.unibo.assets_labeler import SBertLabeler
from labeler.unibo.labeler import SBertLabeler
from matchmaking.online_matchmaking import OnlineMatchmaking
from matchmaking.online_matchmaking import ScalProdScore, CosineSimilarityScore
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np

def load_labels(fname):
    # Read labels from a JSON file
    with open(fname) as fp:
        label_json = json.load(fp)
    labels_dict = label_json['labels']
    # Convert labels in the required format
    labels = [lbase.Label(l['identifier'], l['name'], l['description']) for l in labels_dict]
    # Return the results
    return labels


def load_resources(fname):
    # Load raw CSV
    res = pd.read_csv(fname)
    # Select a subset of the columns
    cols = ['Name', 'Description']
    return res[cols]


def load_queries(fname):
    # Load raw CSV
    res = pd.read_csv(fname)
    # Return descriptions
    return list(res['description'])


def build_unibo_labeler(label_fname):
    # Configure the labeler
    labeler_info = {
        'labels_filepath': label_fname,
        'strategy': 'descr2text',
        'similarity_metric': 'cosine_similarity',
        'save_path': None
    }
    # Build and configure the labeler
    labeler = SBertLabeler(**labeler_info)
    labeler.configure(force_reload=False)
    # Return the labeler
    return labeler


def build_online_matchmaking(resources_index, score_distance='cosine'):
    assert(score_distance in ('cosine', 'scalprod'))
    # Buid the algorithm object
    if score_distance == 'scalprod':
        online_alg = OnlineMatchmaking(metrics=ScalProdScore())
    elif score_distance == 'cosine':
        online_alg = OnlineMatchmaking(metrics=CosineSimilarityScore())
    # online_alg = OnlineMatchmaking(metrics=cosine_similarity)
    # Load the resources
    online_alg.loadResources(resources_index)
    return online_alg


def get_score_df(scores, labels):
    res = pd.DataFrame(scores)
    res.rename(columns={l.identifier: l.name for l in labels}, inplace=True)
    return res


def build_resource_index(resources, resource_scores):
    res = [mbase.Resource(identifier=i, name = resources.iloc[i]['Name'], scores=resource_scores[i], description=resources.iloc[i]['Description'])
            for i in range(len(resource_scores))]
    return res


def get_relative_scores(query_scores, online_alg):
    query_vector = pd.Series(query_scores).to_numpy()
    # query_vector = pd.Series(query_scores[query_id]).to_numpy()
    q_norm = np.linalg.norm(query_vector)
    query_relative_scores = {i:query_vector[i]/q_norm for i in range(query_vector.shape[0]) }

    assets_matrix = pd.DataFrame(online_alg.resources_scores).to_numpy()
    assets_norm = np.linalg.norm(assets_matrix, axis = 1)
    assets_relative_scores = dict()
    for i, asset in  enumerate(online_alg.resources_scores):
        scores = pd.Series(asset).to_numpy()
        assets_relative_scores[i] = scores / assets_norm[i]

    query_relative_scores = pd.Series(query_relative_scores).T
    assets_relative_scores = pd.DataFrame(assets_relative_scores).T
    return query_relative_scores, assets_relative_scores
