"""
Simple storage and access control module
"""

import json
import os
import uuid
from pathlib import Path
from warnings import warn
import glob

from utility.json_utility import load_json
from utility.logging_utility import Logger

testing_flag = '_test'


def already_exists(directory, metadata) -> bool:
    directory: Path = Path(directory) if type(directory) != Path else directory
    if 'uuid identifier' not in metadata:
        Logger.get_logger(__name__).warning(f'Expected resource metadata to have field uuid identifier...'
                                            f'Got {metadata}')
        return False

    for filename in directory.glob('*.json'):
        file_data = load_json(filename)
        if 'uuid identifier' not in file_data:
            warn(f'Expected resource {filename} metadata to have field uuid identifier. Skipping...')
            continue

        if file_data['uuid identifier'] == metadata['uuid identifier']:
            return True

    return False


def generate_storage_files(metadata, directory, testing):
    """
    Generate storage (and access control) files.

    Generate storage and access control files for the metadata and directory
    passed as input. Each file will be in json format and containt one
    element from the metatada list by default. Files are overwritten if they
    already exist.

    Parameters
    ----------
    metatada: list
        metadata that should be used to define the initian content of the
        stoage and access control files. Each metatada should be a dictionary
    directory: str
        location for the files to be generated
    testing: bool
        specify whether the files are for testing purpose, in which case a
        particular flag will be set to True in the content.

    Returns
    -------
    list:
        list of the generated file identifies (file names, no extension)
    
    """
    # Simple parameter control
    assert (len(metadata) > 0)
    assert (os.path.isdir(directory))


    # Generate uuids
    all_file_ids = []
    for i, content in enumerate(metadata):
        if already_exists(directory=directory,
                          metadata=content):
            continue

        # Generate file identifier
        file_id = uuid.uuid1(clock_seq=i)
        all_file_ids.append(file_id)
        # Add testing info
        if testing:
            content = content.copy()
            content[testing_flag] = True
        # Write content in the file
        file_name = os.path.join(directory, f'{file_id}.json')
        with open(file_name, 'w') as fp:
            json.dump(content, fp)
    # Return ids
    return all_file_ids


def clean_files(directory, only_test_files=True):
    """
    Remove all SSAC files created for testing purpose

    Parameters
    ----------
    directory: str
        the directory with the files

    Returns
    -------
    list:
        names of all all removed files
    """

    # List all json files in the diretory
    all_files = [f for f in os.listdir(directory) if f.endswith('.json')]
    # Loop over all fiels
    removed_files = []
    for fname in all_files:
        full_fname = os.path.join(directory, fname)
        with open(full_fname) as fp:
            content = json.load(fp)
            if not only_test_files or (testing_flag in content and content[testing_flag]):
                os.remove(full_fname)
                removed_files.append(fname)
    # Return all removed files
    return removed_files


def check_and_read(directory, file_id):
    """
    Simple access control check

    Checks whether the specified file id is present in the directory. In case
    this is found, it returns the file content. This is not very secure,
    but it is fine enough at the current development stage.

    Parameters
    ----------
    directory: str
        The direcory where the file should be located
    file_id: str
        The file ID

    Returns
    -------
        dict:
            None if access is not granted (the file does not exist).
            Otherwise, a dictionary with the file content
    """
    full_fname = os.path.join(directory, f'{file_id}.json')
    if not os.path.isfile(full_fname):
        return None
    else:
        with open(full_fname) as fp:
            content = json.load(fp)
            return content


def check_and_store(new_content, directory, file_id):
    """
    Simple storage API

    Checks whether the specified file id is present in the directory. In
    case it is found, it updates the content with the information passed as
    a parameter (by calling the dict.update method). This is not verys
    secure, but it is fine enough at the current development stage.

    Parameters
    ----------
    content: dict
        A dictionary with updated information for one ore more fields
    directory: str
        The direcory where the file should be located
    file_id: str
        The file ID

    Returns
    -------
        dict:
            None if access is not granted (the file does not exist).
            Otherwise, a dictionary with the updated file content
    """
    full_fname = os.path.join(directory, f'{file_id}.json')
    if not os.path.isfile(full_fname):
        return None
    else:
        # Read content
        with open(full_fname) as fp:
            content = json.load(fp)
        # Update content
        # content.update(new_content)
        content = new_content
        # Write back the content
        with open(full_fname, 'w') as fp:
            json.dump(content, fp)
        # Return the updated content
        return content

def read_all(directory):
    """
    Read all data from a SSAC directory

    Parameters
    ----------
    directory: str
        The direcory where the file should be located

    Returns
    -------
        list [dict]:
            A list of dictionaries with data from all files
    """
    res = []
    data_glob = os.path.join(directory, '*.json')
    for fname in glob.glob(data_glob):
        with open(fname) as fp:
            res.append(json.load(fp))
    return res
