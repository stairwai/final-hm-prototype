
import numpy as np
from scipy.spatial.distance import braycurtis
from sklearn.metrics.pairwise import cosine_similarity
from textdistance import jaccard


def batch_compliant_braycurtis(
        a,
        b
):
    similarities = []
    for a_emb in a:
        for b_emb in b:
            similarities.append(braycurtis(a_emb, b_emb))

    return 1. - np.array(similarities).reshape(a.shape[0], b.shape[0])


def batch_compliant_jaccard(
        a,
        b
):
    similarities = []
    for a_item in a:
        for b_item in b:
            similarities.append(jaccard(a_item, b_item))

    return np.array(similarities).reshape(len(a), len(b))


def get_metric(
        metric_name: str
):
    if metric_name == 'cosine_similarity':
        return cosine_similarity, True
    elif metric_name == 'braycurtis':
        return batch_compliant_braycurtis, True
    elif metric_name == 'jaccard':
        return batch_compliant_jaccard, False
    else:
        raise RuntimeError('Invalid metric name given! Got {0}'.format(metric_name))