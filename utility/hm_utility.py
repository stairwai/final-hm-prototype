#!/bin/python3
from labeler import labeler
import pandas as pd
from labeler.unibo.labeler import SBertLabeler
from matchmaking import base as mbase

# TODO do we still need this module?

def load_labels_ams(driver):
    # Load labels and descriptions
    label_data = driver.get_aitechniques(with_details=True)
    # Convert to the internal label format
    labels = [labeler.Label(d[0], l, d[1]) for l, d in label_data.items()]
    return labels

def load_resources_ams(driver, labels):
    # Load resources ID, descriptions and score
    resource_data = driver.get_aiexperts()
    # Convert scores in the required format (id-based, rather than name based)
    label_name_to_id = {}
    for label in labels:
        label_name_to_id[label.name] = label.identifier
    # Convert to the internal resource format
    resources = []
    for resource in resource_data:
        idx = resource['swai__identifier']
        name = resource['swai__name'] if 'swai__name' in resource else ''
        desc = resource['swai__short_description'] if 'swai__short_description' in resource else ''
        if 'swai__scored in' in resource:
            scores = {label_name_to_id[n]: v for n, v in resource['swai__scored in'].items()}
        else:
            scores = {}
        resources.append(mbase.Resource(name, idx, desc, scores))
        # resources = [mbase.Resource(d['swai__name'], d['swai__identifier'],  d['swai__short_description'], d['swai__scores_id']) for d in resource_data]
    return resources

def load_resources(fname):
    # Load raw CSV
    res = pd.read_csv(fname)
    # Select a subset of the columns
    cols = ['Name', 'Description']
    return res[cols]


def build_unibo_labeler(labels=None):
    # Configure the labeler
    labeler_info = {
        'strategy': 'descr2text',
        'similarity_metric': 'cosine_similarity',
        'save_path': None
    }
    # Build and configure the labeler
    labeler = SBertLabeler(**labeler_info)
    labeler.configure(labels)
    # Return the labeler
    return labeler


def build_resource_index(resources, resource_scores):
    res = [mbase.Resource(identifier=None, name=resources.iloc[i]['Name'], scores=resource_scores[i], description=resources.iloc[i]['Description'])
            for i in range(len(resource_scores))]
    return res


def score_queries(queries, labeler):
    q_scores_raw = labeler.score(queries, return_sentences=True, return_amount=3)
    q_scores_quantile = [{k: v['scores'].min() for k, v in V.items()} for V in q_scores_raw]
    q_scores_n = [{k:v / sum(d.values()) for k, v in d.items()} for d in q_scores_quantile]
    return q_scores_n
