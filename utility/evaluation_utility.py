from abc import ABC
from typing import Dict

import numpy as np
import sklearn.metrics


class Metric(ABC):

    def __init__(
            self,
            name: str,
            metric_arguments: Dict = {}
    ):
        self.name = name
        self.metric_arguments = metric_arguments

    def __call__(
            self,
            y_pred,
            y_true
    ):
        raise NotImplementedError()

    def __str__(
            self
    ) -> str:
        return self.name


class SklearnMetric(Metric):

    def __init__(
            self,
            function_name: str,
            **kwargs):
        super(SklearnMetric, self).__init__(**kwargs)
        self.function_name = function_name

        assert hasattr(sklearn.metrics, function_name), f'Could not find specified metric in sklearn.metrics module. ' \
                                                        f'Got {function_name}'
        self.metric_method = getattr(sklearn.metrics, self.function_name)

    def __call__(
            self,
            y_pred,
            y_true
    ):
        y_pred = np.array(y_pred) if type(y_pred) != np.ndarray else y_pred
        y_true = np.array(y_true) if type(y_true) != np.ndarray else y_true

        return self.metric_method(y_true=y_true, y_pred=y_pred, **self.metric_arguments)
