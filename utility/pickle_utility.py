import cloudpickle as pickle
from typing import AnyStr, Any


def load_pickle(
        filepath: AnyStr
) -> Any:
    with open(filepath, 'rb') as f:
        data = pickle.load(f)

    return data


def save_pickle(
        filepath: AnyStr,
        data: Any
):
    with open(filepath, 'wb') as f:
        pickle.dump(data, f)
