# Specify the base image
# FROM python:3
FROM ubuntu:20.04

# Configure time zone
RUN apt-get update && \
    apt-get install -yq tzdata && \
    ln -fs /usr/share/zoneinfo/Europe/Rome /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata

# Update the package manager and install a simple module. The RUN command
# will execute a command on the container and then save a snapshot of the
# results. The last of these snapshots will be the final image

# libqt5printsupport5 is needed for MiniZinc + Gecode to work
RUN apt-get update -y && apt-get install -y zip wget libqt5printsupport5 \
    python3-pip nodejs npm

# Prepare for server certification
# RUN apt-get install -y software-properties-common

# Make sure the contents of our repo are in /app
COPY . /app

# Specify working directory
WORKDIR /app

# Install main dependencies
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
RUN pip install https://github.com/overhangio/py2neo/releases/download/2021.2.3/py2neo-2021.2.3.tar.gz

# Install the front-end packages
# RUN npm install

# Load an NLP model
# TODO turn this into a proper requirement
RUN python3 -m spacy download en_core_web_sm

# Add MiniZinc to the PATH
# ENV PATH=$PATH:/app/dependencies/minizinc/2.6.2/bin
# echo 'export PATH=$PATH:/app/dependencies/minizinc/2.6.2/bin' >> ~/.bashrc

# # Add the AMS module to the path
# ENV NEO4J_URL=bolt://localhost:7687
# ENV NEO4J_USER neo4j
# ENV NEO4J_PASSWORD s3cr3t

# Install testing dependencies
# RUN pip3 install -r requirements_testing.txt

# # Install CPLEX (vertical-matchmaking)
# COPY cplex_studio2210.linux_x86_64.bin .
# RUN ./cplex_studio2210.linux_x86_64.bin -DLICENSE_ACCEPTED=true -i silent
# RUN python3 /opt/ibm/ILOG/CPLEX_Studio221/python/setup.py install

# Use CMD to specify the starting command
# CMD ["jupyter", "notebook", "--port=8888", "--no-browser", \
#      "--ip=0.0.0.0", "--allow-root"]
CMD ["sh", "cmd_start_flask_service.sh"]
