import os
from typing import Dict
from typing import cast

from core.data import FieldSet
from core.registry import Registry
from labeler.components.labeler import TextLabeler
from matchmaking.components.algorithm import MatchmakingAlgorithm
from shared.components.data_manager import DataManager
from util import ssac
from utility.json_utility import load_json
from utility.logging_utility import Logger


class HorizontalMatchmaking:
    """
    Horizontal matchmaking service.

    This is a base class, to be specialized for handling specific resources
    """

    def __init__(
            self,
            label_manager_config_regr_key,
            resource_manager_config_regr_key,
            labeler_config_regr_key,
            matchmaking_config_regr_key
    ):
        self.params = load_json('conf.json')
        logger = Logger.get_logger(self.__class__.__name__)

        # LabelManager (lm)
        lm = Registry.retrieve_component_from_key(config_regr_key=label_manager_config_regr_key)
        lm = cast(DataManager, lm)
        self.labels = lm.run()
        logger.info(f'Labels: {os.linesep}{self.labels}')

        # ResourceManager (rm)
        self.rm = Registry.retrieve_component_from_key(config_regr_key=resource_manager_config_regr_key)
        self.rm = cast(DataManager, self.rm)
        self.resources = self.rm.run()
        logger.info(f'Resources: {os.linesep}{self.resources}')

        # TextLabeler
        self.labeler = Registry.retrieve_component_from_key(config_regr_key=labeler_config_regr_key)
        self.labeler = cast(TextLabeler, self.labeler)
        if 'labeler_score' not in self.resources:
            self.labeler.run(data=self.resources,
                             labels=self.labels)

        # MatchmakingAlgorithm
        self.matchmaking = Registry.retrieve_component_from_key(config_regr_key=matchmaking_config_regr_key)
        self.matchmaking = cast(MatchmakingAlgorithm, self.matchmaking)

    @property
    def label_options(
            self
    ) -> Dict[int, str]:
        return {identifier: name for identifier, name in zip(self.labels.identifier,
                                                             self.labels.name)}

    # Run the matchmaking algorithm
    def run_matchmaking(
            self,
            query: str,
            matchmaking_weights={},
            metadata: dict = None,
            return_explanations: bool = False
    ):
        """
        Run the matchmaking algorithm

        Run the matchmaking algorithm to obtain a ranked list of relevant
        resources.

        Parameters
        ----------
        query: str
            A textual description w.r.t. to which relevance scores will be
            computed. Ideally, this should be a few sentences long.
        metadata: list
            A list of dictionaries, containing symbolic information. Dictionary
            keys represent the field name. This information is processed ad-hoc
            depending on the specific algorithm or resource type
        return_explanations: bool
            If True, explanations are returned as a matrix of sub-scores for
            individual asset and label
        matchmaking_weights: TODO description

        Returns
        -------
        dict:
            A dictionary containing the matchmaking results
        """

        # -----------------------------------------------------------------------
        # Compute query scores
        # -----------------------------------------------------------------------

        # Build query
        # TODO: maybe define API for DataManager to build from input (even partially defined)
        query_example = FieldSet()
        query_example.add_short(name='identifier',
                                value=[0],
                                tags={'ID'})
        query_example.add_short(name='description',
                                value=[query],
                                tags={'text'})

        if metadata is not None:
            for key, value in metadata.items():
                query_example.add_short(name=key,
                                        value=[value],
                                        tags={'metadata'})

        # Run labeler
        query = self.labeler.run(data=query_example,
                                 labels=self.labels,
                                 normalize_scores=True)

        # Apply modifications if the "emph_labels" key is present in the metadata
        if metadata is not None and 'emph_labels' in metadata:
            for label_name in metadata['emph_labels']:
                if label_name not in query.labeler_score[0]:
                    res['warnings'].append(
                        f'label {label_name} not found (likely due to a DB reset). Dropping to continue execution')
                else:
                    query.labeler_score[0][label_name] += self.params['emph_label_increment']

        # -----------------------------------------------------------------------
        # Run the matchmaking algorithm
        # -----------------------------------------------------------------------

        # Run the matchmaking algorithm for the query
        mm_res = self.matchmaking.run(queries=query,
                                      resources=self.resources,
                                      labels=self.labels,
                                      weights=matchmaking_weights)

        # -----------------------------------------------------------------------
        # Return the raw results
        # -----------------------------------------------------------------------

        return mm_res

    # Run the labeling algorithm
    def run_labeling(
            self,
            query: str,
            return_explanations: bool = False
    ):
        res = {
            'labels': []
        }
        if return_explanations:
            res['explanations'] = {}

        # Build query
        # TODO: maybe define API for DataManager to build from input (even partially defined)
        query_example = FieldSet()
        query_example.add_short(name='identifier',
                                value=[0],
                                tags={'ID'})
        query_example.add_short(name='description',
                                value=[query],
                                tags={'text'})

        # Run labeler
        query = self.labeler.run(data=query_example,
                                 labels=self.labels,
                                 normalize_scores=True)

        # Build a result data structure
        for label_name, label_score in sorted(query.labeler_score[0].items(), key=lambda x: -x[1]):
            ldesc = {
                'name': label_name,
                'score': label_score
            }
            res['labels'].append(ldesc)
            # Compute the aggregate explanations
            if return_explanations:
                xpl = query.labeler_explanation[0][label_name]
                xpl_q = sorted(xpl, key=lambda t: -t[0])
                xpl_q = list(set([t[1] for t in xpl_q]))
                res['explanations'][label_name] = xpl_q

        return res


class UseCaseExpertHorizontalMatchmaking(HorizontalMatchmaking):
    """
    Horizontal matchmaking service.
    """

    def __init__(
            self,
            label_manager_config_regr_key,
            resource_manager_config_regr_key,
            labeler_config_regr_key,
            matchmaking_config_regr_key
    ):
        # Call the default initializer
        HorizontalMatchmaking.__init__(self, label_manager_config_regr_key,
                                       resource_manager_config_regr_key,
                                       labeler_config_regr_key,
                                       matchmaking_config_regr_key)

        # Configuration specific to this form of the horizontal matchmaking service
        n_labels = len(self.labels.name)
        self.team_size_min_importance_map = {
            'normal': 0.75 / n_labels,
            'low': 0.38 / n_labels,
            'high': 1.5 / n_labels
        }

        self.languages_importance_map = {
            'normal': 1 / n_labels,
            'low': 0.5 / n_labels,
            'high': 2 / n_labels
        }

        self.min_desc_size = 200

    # Run the matchmaking algorithm
    def run_matchmaking(
            self,
            query: str,
            matchmaking_weights={},
            metadata: dict = None,
            return_explanations: bool = False
    ):
        """
        Run the matchmaking algorithm

        Run the matchmaking algorithm to obtain a ranked list of relevant
        resources.

        Parameters
        ----------
        query: str
            A textual description w.r.t. to which relevance scores will be
            computed. Ideally, this should be a few sentences long.
        metadata: list
            A list of dictionaries, containing symbolic information. Dictionary
            keys represent the field name. This information is processed ad-hoc
            depending on the specific algorithm or resource type
        return_explanations: bool
            If True, explanations are returned as a matrix of sub-scores for
            individual asset and label
        matchmaking_weights: TODO description

        Returns
        -------
        dict:
            A dictionary containing the matchmaking results
        """

        # Run the base class matchmaking algorithm
        mm_res = super().run_matchmaking(query, matchmaking_weights, metadata, return_explanations)

        # -----------------------------------------------------------------------
        # Convert the results to a dictionary
        # -----------------------------------------------------------------------

        # Prepare result data structure
        res = {
            'assets': [],
            'warnings': []
        }

        # Add the ranked assets
        for index, (identifier, name, description, score, price, languages, team_size) \
                in enumerate(zip(mm_res.identifier,
                                 mm_res.name if 'name' in mm_res else mm_res.identifier,
                                 mm_res.description,
                                 mm_res.matchmaking_score,
                                 mm_res.price,
                                 mm_res.languages,
                                 mm_res.team_size)):
            rdesc = {
                'id': identifier,
                'name': name,
                'score': score,
                'description': description,
                'price': price,
                'languages': [languages] if isinstance(languages, str) else languages,
                'team_size': team_size
            }
            res['assets'].append(rdesc)

            if return_explanations and 'matchmaking_explanation' in mm_res:
                res.setdefault('explanations', []).append(mm_res.matchmaking_explanation[index])

        # Return results
        return res

    def get_language_options(
            self
    ):
        # return self.rm.driver.get_languages(with_code=True)
        return self.rm.driver.get_languages()

    def get_country_options(
            self
    ):
        return self.rm.driver.get_countries()

    def asset_expert_access_control(
            self,
            token
    ):
        # Configuration parameters
        ep_dir = self.params['asset_expert']

        # Perform access control
        content = ssac.check_and_read(ep_dir, token)
        return content is not None, content

    def asset_expert_get(
            self,
            expert_id,
            request,
            ssac_expert_id=None,
            ssac_content=None
    ):
        # Prepare result fields
        name = None
        description = None
        country = None
        languages = None
        team_size = None
        price = None
        label_list = None
        explanation_list = None

        # CONTENT RETRIEVAL =================================================
        # Retrieve the node and all content
        expert_data = self.rm.driver.get_aiexperts(uuid_identifier=expert_id)

        # Populate content fields with stored values
        if expert_data is not None and 'name' in expert_data:
            name = expert_data['name']
        if expert_data is not None and 'short description' in expert_data:
            description = expert_data['short description']
        if expert_data is not None and 'resides' in expert_data:
            country = expert_data['resides']
        else:
            country = ''
        if expert_data is not None and 'has language' in expert_data:
            languages = expert_data['has language']
        else:
            languages = []
        if expert_data is not None and 'team size' in expert_data:
            team_size = expert_data['team size']
        else:
            team_size = ''
        if expert_data is not None and 'price' in expert_data:
            price = expert_data['price']
        else:
            price = ''

        # LABEL RETRIEVAL ===================================================
        # Retrieve the stored scores
        if 'scored in' in expert_data:
            scores = expert_data['scored in']
            # Adapt the format
            label_list = [(l, v) for l, v in scores.items()]
            label_list = sorted(label_list, key=lambda t: -t[1])
            if 'explanations' in ssac_content:
                xpl = ssac_content['explanations']
                explanation_list = [xpl[l[0]] for l in label_list]

        # RESULT PACKAGING ==================================================
        # Return results
        res = {
            'name': name,
            'description': description,
            'country': country,
            'languages': languages,
            'team_size': team_size,
            'price': price,
            'label_list': label_list,
            'explanation_list': explanation_list
        }
        return res

    def asset_expert_post(
            self,
            expert_id,
            request,
            ssac_expert_id=None,
            ssac_content=None
    ):
        # Default values for the content fields
        name = None
        description = None
        country = None
        languages = None
        team_size = None
        price = None
        scores = None
        explanations = None
        explanation_list = None
        label_list = None

        # CONTENT RETRIEVAL =================================================
        # Retrieve content object
        # TODO we need better separation between the http and AMS part
        json_content = True
        content = request.get_json(silent=True)
        if content is None:
            json_content = False
            content = request.form

        # Replace fields with new values
        if 'description' in content:
            description = content['description']
        if 'country' in content:
            country = content['country']  # TODO add correctness check
        if 'languages' in content:
            if json_content:
                languages = content['languages']  # TODO add correctness check
            else:
                languages = content.getlist('languages')  # TODO add correctness check
        if 'team_size' in content:
            if content['team_size'] == '':
                team_size = None
            else:
                team_size = int(content['team_size'])  # TODO add correctness check
        if 'price' in content:
            if content['price'] == '':
                price = None
            else:
                price = float(content['price'])  # TODO add correctness check

        # Recompute the scores, if enough information is provided
        if description is not None:
            # Retrieve the label list, in the internal format
            labeling_res = self.run_labeling(description, return_explanations=True)
            label_list = labeling_res['labels']
            explanations = labeling_res['explanations']
            # Convert into the AMS format
            scores = {le['name']: le['score'] for le in label_list}

        # ADJUST DATA FOR STORAGE ===========================================
        # Prepare a data structure to store in the AMS
        new_expert_data = {}
        if description is not None:
            new_expert_data['short description'] = description
        if country is not None:
            new_expert_data['resides'] = country
        if languages is not None:
            if len(languages) > 0:
                new_expert_data['has language'] = languages
            else:
                new_expert_data['has language'] = languages[0]
        if team_size is not None:
            new_expert_data['team size'] = team_size
        if price is not None:
            new_expert_data['price'] = price
        if scores is not None:
            new_expert_data['scored in'] = scores

        # Retrieve existing data for the expert
        driver = self.rm.driver
        old_expert_data = driver.get_aiexperts(uuid_identifier=expert_id)

        # print(f'Old expert data: \n{old_expert_data}')
        # print(f'New expert data: \n{new_expert_data}')

        # Memorize the expert id
        expert_swai_id = old_expert_data['identifier']
        # Identify the properties to be deleted
        data_to_delete = {k: v for k, v in old_expert_data.items()
                          if k in new_expert_data}
        # Convert the score dictionary intoa list containing only the labels
        if 'scored in' in data_to_delete:
            data_to_delete['scored in'] = list(data_to_delete['scored in'].keys())
        # Delete old properties and add new ones
        driver.modify_instance(expert_swai_id, add_dict=new_expert_data,
                               delete_dict=data_to_delete)

        # Store the explanations in the file DB
        if explanations is not None:
            e_dir = self.params['asset_expert']
            ssac_content['explanations'] = explanations
            tmp = ssac.check_and_store(ssac_content, e_dir, ssac_expert_id)

        # ADJUST LABEL FORMAT FOR OUTPUT ===================================
        # Retrieve the stored scores
        if 'scored in' in new_expert_data:
            scores = new_expert_data['scored in']
            # Adapt the format
            label_list = [(l, v) for l, v in scores.items()]
            label_list = sorted(label_list, key=lambda t: -t[1])
            explanation_list = [explanations[l[0]] for l in label_list]

        # RESULT PACKAGING ==================================================
        res = {
            'name': old_expert_data['name'],
            'description': description,
            'country': country,
            'languages': languages,
            'team_size': team_size,
            'price': price,
            'label_list': label_list,
            'explanation_list': explanation_list
        }
        return res

    def query_usecase_access_control(
            self,
            token
    ):
        # Configuration parameters
        uc_dir = self.params['matchmaking_usecase']

        # Perform access control
        content = ssac.check_and_read(uc_dir, token)
        return content is not None, content

    def query_usecase_get(
            self,
            usecase_id,
            request
    ):
        # Prepare result fields
        name = None
        description = None
        languages = []
        languages_importance = 'normal'
        emph_labels = []
        team_size_min = None
        team_size_min_importance = 'normal'
        ai_readiness = None
        asset_list = None
        explanation_list = None
        selected_assets = []
        feedback = None

        # CONTENT RETRIEVAL =================================================
        # Storage folder
        uc_dir = self.params['matchmaking_usecase']
        # Read stored content
        content = ssac.check_and_read(uc_dir, usecase_id)

        # Populate content fields with stored values
        if content is not None and 'name' in content and content['name'] is not None:
            name = content['name']
        if content is not None and 'description' in content and content['description'] is not None:
            description = content['description']
        if content is not None and 'team_size_min' in content and content['team_size_min'] is not None:
            team_size_min = int(content['team_size_min'])
        if content is not None and 'team_size_min_importance' in content and content[
            'team_size_min_importance'] is not None:
            team_size_min_importance = content['team_size_min_importance']
        if content is not None and 'ai_readiness' in content and content['ai_readiness'] is not None:
            ai_readiness = content['ai_readiness']
        if content is not None and 'languages' in content and content['languages'] is not None:
            languages = content['languages']  # TODO add correctness check
        if content is not None and 'languages_importance' in content and content['languages_importance'] is not None:
            languages_importance = content['languages_importance']
        if content is not None and 'emph_labels' in content and content['emph_labels'] is not None:
            emph_labels = content['emph_labels']
        if content is not None and 'selected_assets' in content and content['selected_assets'] is not None:
            selected_assets = content['selected_assets']
        if content is not None and 'feedback' in content and content['feedback'] is not None:
            feedback = content['feedback']

        # TODO: @Michi -> fill this
        # Format: Dict[str, float] -> str: score name, float: score weight
        # Names -> 'team_size', 'languages', 'labeler'
        # The default weights are 1.0 if not specified
        matchmaking_weights = {'labeler': 1,
                               'team_size': self.team_size_min_importance_map[team_size_min_importance],
                               'languages': self.languages_importance_map[languages_importance]
                               }

        # RESOURCE RETRIEVAL ================================================
        # Retrieve ranked resources
        if description is not None:
            metadata = {
                'team_size': team_size_min,  # TODO the matchmaking algorithms uses a different field name for this
                'ai_readiness': ai_readiness,
                'languages': languages,
                'emph_labels': emph_labels
            }

            mm_result = self.run_matchmaking(description,
                                             metadata=metadata,
                                             return_explanations=True,
                                             matchmaking_weights=matchmaking_weights)


            # Discard resources if their description is too short
            asset_list = [e for e in mm_result['assets'] if len(e['description']) >= self.min_desc_size]
            explanation_list = [x for x, e in zip(mm_result['explanations'], mm_result['assets']) if
                                len(e['description']) >= self.min_desc_size]

            #     asset_list = mm_result['assets']
            #     explanation_list = mm_result['explanations']

            # Augment resource list with email info
            ssac_expert_data = ssac.read_all(self.params['asset_expert'])
            ssac_email_map = {}
            for e_data in ssac_expert_data:  # Build a name-to-email map
                if 'name' not in e_data:
                    continue

                e_name = e_data['name']
                if e_name not in ssac_email_map:
                    ssac_email_map[e_name] = e_data['email']
                else:
                    ssac_email_map[e_name] = 'check the full list'
            for asset in asset_list:  # Add email information to the assets
                a_name = asset['name']
                if a_name in ssac_email_map:
                    asset['email'] = ssac_email_map[a_name]
                else:
                    asset['email'] = 'check the full list'

        # RESULT PACKAGING ==================================================
        # Return results
        res = {
            'name': name,
            'description': description,
            'languages': languages,
            'languages_importance': languages_importance,
            'emph_labels': emph_labels,
            'team_size_min': team_size_min,
            'team_size_min_importance': team_size_min_importance,
            'ai_readiness': ai_readiness,
            'asset_list': asset_list,
            'explanation_list': explanation_list,
            'selected_assets': selected_assets,
            'feedback': feedback,
        }
        return res

    def query_usecase_post(
            self,
            usecase_id,
            request
    ):
        # Prepare result fields
        name = None
        description = None
        languages = []
        languages_importance = 'normal'
        emph_labels = []
        team_size_min = None
        team_size_min_importance = 'normal'
        ai_readiness = None
        asset_list = None
        explanation_list = None
        selected_assets = []
        feedback = None

        # CONTENT RETRIEVAL =================================================
        # Retrieve new content
        # TODO we need better separation between the http and AMS part
        json_content = True
        content = request.get_json(silent=True)
        if content is None:
            json_content = False
            content = request.form

        # Replace fields with new values
        if 'name' in content:
            name = content['name']
        if 'description' in content:
            description = content['description']
        if 'team_size_min' in content:
            if content['team_size_min'] != '':
                team_size_min = int(content['team_size_min'])  # TODO add correctness check
            # team_size_min = content['team_size_min']
        if 'team_size_min_importance' in content:
            if content['team_size_min_importance'] in self.team_size_min_importance_map:
                team_size_min_importance = content['team_size_min_importance']
        if 'ai_readiness' in content:
            if content['ai_readiness'] != '':
                ai_readiness = int(content['ai_readiness'])  # TODO add correctness check
            # ai_readiness = content['ai_readiness']
        if 'languages' in content:
            if json_content:
                languages = content['languages']  # TODO add correctness check
            else:
                languages = content.getlist('languages')  # TODO add correctness check
        if 'languages_importance' in content:
            if content['languages_importance'] in self.languages_importance_map:
                languages_importance = content['languages_importance']
        if 'emph_labels' in content:
            if json_content:
                emph_labels = emph_labels
            else:
                emph_labels = request.form.getlist('emph_labels')
        if 'selected_assets' in content:
            if json_content:
                selected_assets = selected_assets
            else:
                selected_assets = request.form.getlist('selected_assets')
            selected_assets = [int(v) for v in selected_assets]
        if 'feedback' in content:
            feedback = content['feedback']

        # STORAGE ===========================================================
        # Store new information
        new_info = dict()
        new_info['name'] = name
        new_info['description'] = description
        new_info['team_size_min'] = team_size_min
        new_info['team_size_min_importance'] = team_size_min_importance
        new_info['ai_readiness'] = ai_readiness
        new_info['languages'] = languages
        new_info['languages_importance'] = languages_importance
        new_info['emph_labels'] = emph_labels
        new_info['asset_list'] = asset_list
        new_info['selected_assets'] = selected_assets
        new_info['feedback'] = feedback

        # Store the data
        uc_dir = self.params['matchmaking_usecase']
        ssac.check_and_store(new_info, uc_dir, usecase_id)

        # RESOURCE RETRIEVAL AND RETURN =====================================
        res = self.query_usecase_get(usecase_id, request)
        return res


class EduResourceHorizontalMatchmaking(HorizontalMatchmaking):
    """
    Horizontal matchmaking service.
    """

    def __init__(
            self,
            label_manager_config_regr_key,
            resource_manager_config_regr_key,
            labeler_config_regr_key,
            matchmaking_config_regr_key
    ):
        # Call the default initializer
        HorizontalMatchmaking.__init__(self, label_manager_config_regr_key,
                                       resource_manager_config_regr_key,
                                       labeler_config_regr_key,
                                       matchmaking_config_regr_key)

    # Run the matchmaking algorithm
    def run_matchmaking(
            self,
            query: str,
            matchmaking_weights={},
            metadata: dict = None,
            return_explanations: bool = False
    ):
        """
        Run the matchmaking algorithm

        Run the matchmaking algorithm to obtain a ranked list of relevant
        resources.

        Parameters
        ----------
        query: str
            A textual description w.r.t. to which relevance scores will be
            computed. Ideally, this should be a few sentences long.
        metadata: list
            A list of dictionaries, containing symbolic information. Dictionary
            keys represent the field name. This information is processed ad-hoc
            depending on the specific algorithm or resource type
        return_explanations: bool
            If True, explanations are returned as a matrix of sub-scores for
            individual asset and label
        matchmaking_weights: TODO description

        Returns
        -------
        dict:
            A dictionary containing the matchmaking results
        """

        # Run the base class matchmaking algorithm
        mm_res = super().run_matchmaking(query, matchmaking_weights, metadata, return_explanations)

        # -----------------------------------------------------------------------
        # Convert the results to a dictionary
        # -----------------------------------------------------------------------

        # Prepare result data structure
        res = {
            'assets': [],
            'warnings': []
        }

        # Add the ranked assets
        for index, (identifier, title, description, score) \
                in enumerate(zip(mm_res.identifier,
                                 mm_res.title if 'title' in mm_res else mm_res.identifier,
                                 mm_res.description,
                                 mm_res.matchmaking_score)):
            rdesc = {
                'id': identifier,
                'title': title,
                'description': description,
                'score': score
            }
            res['assets'].append(rdesc)

            if return_explanations and 'matchmaking_explanation' in mm_res:
                res.setdefault('explanations', []).append(mm_res.matchmaking_explanation[index])

        # Return results
        return res

    def edu_post(
            self,
            request
    ):
        # Prepare result fields
        title = None
        description = None
        asset_list = None
        explanation_list = None

        # CONTENT RETRIEVAL =================================================
        # Retrieve new content
        # TODO we need better separation between the http and AMS part
        json_content = True
        content = request.get_json(silent=True)
        if content is None:
            json_content = False
            content = request.form

        # Replace fields with new values
        if 'title' in content:
            title = content['title']
        if 'description' in content:
            description = content['description']

        # RESOURCE RETRIEVAL ================================================
        # Retrieve ranked resources
        if description is not None:
            mm_result = self.run_matchmaking(description,
                                             metadata={},
                                             return_explanations=True,
                                             matchmaking_weights={})

        # Discard resources if their description is too short
        asset_list = [e for e in mm_result['assets'] if len(e['description'])]
        explanation_list = [x for x, e in zip(mm_result['explanations'], mm_result['assets'])]

        # RESULT PACKAGING ==================================================
        # Return results
        res = {
            'title': title,
            'description': description,
            'asset_list': asset_list,
            'explanation_list': explanation_list
        }
        return res


class PaperHorizontalMatchmaking(HorizontalMatchmaking):
    """
    Horizontal matchmaking service.
    """

    def __init__(
            self,
            label_manager_config_regr_key,
            resource_manager_config_regr_key,
            labeler_config_regr_key,
            matchmaking_config_regr_key
    ):
        # Call the default initializer
        HorizontalMatchmaking.__init__(self, label_manager_config_regr_key,
                                       resource_manager_config_regr_key,
                                       labeler_config_regr_key,
                                       matchmaking_config_regr_key)

    # Run the matchmaking algorithm
    def run_matchmaking(
            self,
            query: str,
            matchmaking_weights={},
            metadata: dict = None,
            return_explanations: bool = False
    ):
        """
        Run the matchmaking algorithm

        Run the matchmaking algorithm to obtain a ranked list of relevant
        resources.

        Parameters
        ----------
        query: str
            A textual description w.r.t. to which relevance scores will be
            computed. Ideally, this should be a few sentences long.
        metadata: list
            A list of dictionaries, containing symbolic information. Dictionary
            keys represent the field name. This information is processed ad-hoc
            depending on the specific algorithm or resource type
        return_explanations: bool
            If True, explanations are returned as a matrix of sub-scores for
            individual asset and label
        matchmaking_weights: TODO description

        Returns
        -------
        dict:
            A dictionary containing the matchmaking results
        """

        # Run the base class matchmaking algorithm
        mm_res = super().run_matchmaking(query, matchmaking_weights, metadata, return_explanations)

        # -----------------------------------------------------------------------
        # Convert the results to a dictionary
        # -----------------------------------------------------------------------

        # Prepare result data structure
        res = {
            'assets': [],
            'warnings': []
        }

        # Add the ranked assets
        for index, (identifier, title, description, score) \
                in enumerate(zip(mm_res.identifier,
                                 mm_res.title if 'title' in mm_res else mm_res.identifier,
                                 mm_res.abstract,
                                 mm_res.matchmaking_score)):
            rdesc = {
                'id': identifier,
                'title': title,
                'description': description,
                'score': score
            }
            res['assets'].append(rdesc)

            if return_explanations and 'matchmaking_explanation' in mm_res:
                res.setdefault('explanations', []).append(mm_res.matchmaking_explanation[index])

        # Return results
        return res

    def paper_post(
            self,
            request
    ):
        # Prepare result fields
        title = None
        description = None
        asset_list = None
        explanation_list = None

        # CONTENT RETRIEVAL =================================================
        # Retrieve new content
        # TODO we need better separation between the http and AMS part
        json_content = True
        content = request.get_json(silent=True)
        if content is None:
            json_content = False
            content = request.form

        # Replace fields with new values
        if 'title' in content:
            title = content['title']
        if 'description' in content:
            description = content['description']

        # RESOURCE RETRIEVAL ================================================
        # Retrieve ranked resources
        if description is not None:
            mm_result = self.run_matchmaking(description,
                                             metadata={},
                                             return_explanations=True,
                                             matchmaking_weights={})

        # Discard resources if their description is too short
        asset_list = [e for e in mm_result['assets'] if len(e['description'])]
        explanation_list = [x for x, e in zip(mm_result['explanations'], mm_result['assets'])]

        # RESULT PACKAGING ==================================================
        # Return results
        res = {
            'title': title,
            'description': description,
            'asset_list': asset_list,
            'explanation_list': explanation_list
        }
        return res
