from typing import List, Iterable

import numpy as np

from core.configuration import Configuration
from core.data import ParameterSet
from core.registry import Registry, Framework
from labeler.components.classification import LabelerClassifier
from utility.evaluation_utility import Metric, SklearnMetric


class LabelerClassifierConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = ParameterSet()

        parameter_set.add_short(name='metrics',
                                typing=List[Metric],
                                value=[
                                    SklearnMetric(name='F1',
                                                  function_name='f1_score',
                                                  metric_arguments={'average': 'binary',
                                                                    'pos_label': 1})
                                ],
                                is_required=False,
                                description="Metrics to compute for ontology matching evaluation")
        parameter_set.add_short(name='thresholds',
                                typing=Iterable[float],
                                value=np.arange(0.20, 1.05, 0.05),
                                is_required=False,
                                description='Classification thresholds for metric computation')

        return parameter_set


def register_labeler_classifiers():
    Registry.register_and_map(configuration=LabelerClassifierConfig.get_default(),
                              component_class=LabelerClassifier,
                              name='labeler_classifier',
                              framework=Framework.GENERIC,
                              is_default=True)
