from core.configuration import Configuration
from core.data import ParameterSet
from core.registry import Registry, Framework
from labeler.components.labeler import RandomLabeler, ZeroShotLabeler, SBertLabeler


class TextLabelerConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        return ParameterSet()


class ZeroShotLabelerConfig(TextLabelerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.add_short(name='model_name',
                                value='cross-encoder/nli-roberta-base',
                                is_required=True,
                                typing=str,
                                description="HuggingFace's zero-shot model card name")

        return parameter_set


class SBertLabelerConfig(TextLabelerConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.add_short(name='spacy_model_name',
                                value='en_core_web_sm',
                                typing=str,
                                is_required=True,
                                description="Spacy model name for text processing")
        parameter_set.add_short(name='sbert_model_name',
                                value='all-mpnet-base-v2',
                                typing=str,
                                is_required=True,
                                description="HuggingFace's SBERT model card name")
        parameter_set.add_short(name='similarity_metric_name',
                                value='cosine_similarity',
                                is_required=True,
                                typing=str,
                                description='Similarity metric name for text matching')
        parameter_set.add_short(name='strategy',
                                is_required=True,
                                value='descr2text',
                                allowed_range=lambda value: value in ['descr2text', 'name2text'],
                                typing=str,
                                description='Text matching strategy')
        parameter_set.add_short(name='return_amount',
                                value=3,
                                typing=int,
                                description="Number of best-matching sentences to return (along with their scores)")

        parameter_set.add_condition_short(name='positive_return_amount',
                                          condition=lambda parameters: parameters.return_amount >= 1)

        return parameter_set


def register_labelers():
    # Random
    Registry.register_and_map(configuration=TextLabelerConfig.get_default(),
                              component_class=RandomLabeler,
                              name='labeler',
                              framework=Framework.GENERIC,
                              tags={'random'},
                              namespace='baseline')

    # ZeroShot
    Registry.register_and_map(configuration=ZeroShotLabelerConfig.get_default(),
                              component_class=ZeroShotLabeler,
                              framework=Framework.TORCH,
                              name='labeler',
                              tags={'zero-shot', 'cross-encoder/nli-roberta-base'},
                              namespace='tilde')

    # SBERT
    Registry.register_and_map(configuration=SBertLabelerConfig.get_default(),
                              component_class=SBertLabeler,
                              framework=Framework.TORCH,
                              name='labeler',
                              tags={'sbert', 'all-mpnet-base-v2', 'cosine', 'descr2text'},
                              namespace='unibo')
