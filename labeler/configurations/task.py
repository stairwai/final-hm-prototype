from core.configuration import Configuration
from core.data import ParameterSet
from core.registry import Registry, Framework, RegistrationKey
from labeler.components.classification import LabelerClassifier
from labeler.components.labeler import TextLabeler
from labeler.components.task import LabelerTask, LabelerClassifierTask
from shared.components.data_manager import DataManager
from shared.components.processor import Processor


class LabelerTaskConfig(Configuration):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = ParameterSet()

        parameter_set.add_short(name='name',
                                typing=str,
                                is_required=True,
                                description="Task unique identifier.")
        parameter_set.add_short(name='label_manager',
                                typing=DataManager,
                                value=RegistrationKey(framework=Framework.GENERIC,
                                                      name='data_manager',
                                                      tags={'labels', 'file', 'default'}),
                                is_required=True,
                                is_registration=True,
                                description='Data manager component for loading labels.')
        parameter_set.add_short(name='query_manager',
                                typing=DataManager,
                                value=RegistrationKey(framework=Framework.GENERIC,
                                                      name='data_manager',
                                                      tags={'queries', 'file', 'default'}),
                                is_required=True,
                                is_registration=True,
                                description='Data manager component for loading queries.')
        parameter_set.add_short(name='assets_manager',
                                typing=DataManager,
                                value=RegistrationKey(framework=Framework.GENERIC,
                                                      name='data_manager',
                                                      tags={'assets', 'file', 'default'}),
                                is_required=True,
                                is_registration=True,
                                description='Data manager component for loading assets.')
        parameter_set.add_short(name='processor',
                                typing=Processor,
                                value=RegistrationKey(framework=Framework.GENERIC,
                                                      name='processor',
                                                      tags={'pipeline', 'default'}),
                                is_required=True,
                                is_registration=True,
                                description='Text processor for pre-processing loaded data.')
        parameter_set.add_short(name='labeler',
                                typing=TextLabeler,
                                value=RegistrationKey(framework=Framework.GENERIC,
                                                      name='labeler',
                                                      tags={'random'},
                                                      namespace='baseline'),
                                is_required=True,
                                is_registration=True,
                                description='Text labeler for ontology matching')

        return parameter_set

    @classmethod
    def get_random_config(
            cls
    ) -> 'LabelerTaskConfig':
        default = cls.get_default()

        default.parameter_set.name = 'random_labeler'
        default.parameter_set.labeler = RegistrationKey(framework=Framework.GENERIC,
                                                        name='labeler',
                                                        tags={'random'},
                                                        namespace='baseline')
        return default

    @classmethod
    def get_zero_shot_config(
            cls
    ) -> 'LabelerTaskConfig':
        default = cls.get_default()

        default.parameter_set.name = 'nli-roberta-zero_labeler'
        default.parameter_set.labeler = RegistrationKey(framework=Framework.TORCH,
                                                        name='labeler',
                                                        tags={'cross-encoder/nli-roberta-base', 'zero-shot'},
                                                        namespace='tilde')
        return default

    @classmethod
    def get_sbert_config(
            cls
    ) -> 'LabelerTaskConfig':
        default = cls.get_default()

        default.parameter_set.name = 'sbert-cosine_labeler'
        default.parameter_set.labeler = RegistrationKey(framework=Framework.TORCH,
                                                        name='labeler',
                                                        tags={'descr2text', 'sbert', 'all-mpnet-base-v2', 'cosine'},
                                                        namespace='unibo')
        return default


def register_labeler_tasks():
    # Random
    Registry.register_and_map(configuration=LabelerTaskConfig.get_random_config(),
                              component_class=LabelerTask,
                              framework=Framework.GENERIC,
                              name='labeler_task',
                              tags={'random'},
                              namespace='baseline')

    # Zero-shot
    Registry.register_and_map(configuration=LabelerTaskConfig.get_zero_shot_config(),
                              component_class=LabelerTask,
                              framework=Framework.TORCH,
                              name='labeler_task',
                              tags={'zero-shot', 'cross-encoder/nli-roberta-base'},
                              namespace='tilde')

    # SBERT
    Registry.register_and_map(configuration=LabelerTaskConfig.get_sbert_config(),
                              component_class=LabelerTask,
                              framework=Framework.TORCH,
                              name='labeler_task',
                              tags={'sbert', 'all-mpnet-base-v2', 'cosine', 'descr2text'},
                              namespace='unibo')


class LabelerClassifierTaskConfig(LabelerTaskConfig):

    @classmethod
    def get_parameter_set(
            cls
    ) -> ParameterSet:
        parameter_set = super().get_parameter_set()

        parameter_set.add_short(name='labeler_classifier',
                                typing=LabelerClassifier,
                                value=RegistrationKey(framework=Framework.GENERIC,
                                                      name='labeler_classifier',
                                                      tags={'default'}),
                                is_required=True,
                                is_registration=True,
                                description="Labeler classifier for metric computation")

        return parameter_set


def register_labeler_classifier_tasks():
    # Random
    Registry.register_and_map(configuration=LabelerClassifierTaskConfig.get_random_config(),
                              component_class=LabelerClassifierTask,
                              framework=Framework.GENERIC,
                              name='labeler_classifier_task',
                              tags={'random'},
                              namespace='baseline')

    # Zero-shot
    Registry.register_and_map(configuration=LabelerClassifierTaskConfig.get_zero_shot_config(),
                              component_class=LabelerClassifierTask,
                              framework=Framework.TORCH,
                              name='labeler_classifier_task',
                              tags={'zero-shot', 'cross-encoder/nli-roberta-base'},
                              namespace='tilde')

    # SBERT
    Registry.register_and_map(configuration=LabelerClassifierTaskConfig.get_sbert_config(),
                              component_class=LabelerClassifierTask,
                              framework=Framework.TORCH,
                              name='labeler_classifier_task',
                              tags={'sbert', 'all-mpnet-base-v2', 'cosine', 'descr2text'},
                              namespace='unibo')


def register_tasks():
    register_labeler_tasks()
    register_labeler_classifier_tasks()
