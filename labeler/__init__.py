from labeler.configurations.labeler import register_labelers
from labeler.configurations.task import register_tasks
from labeler.configurations.classification import register_labeler_classifiers


def register():
    register_labelers()
    register_labeler_classifiers()
    register_tasks()
