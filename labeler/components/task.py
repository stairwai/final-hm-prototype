from core.data import FieldSet
from shared.components.task import DataTask
from utility.logging_utility import Logger


class LabelerTask(DataTask):

    def run(
            self,
    ) -> FieldSet:
        Logger.get_logger(self.__class__.__name__).info(self.parameter_set)

        return_field = FieldSet()

        # Labels
        labels = self.label_manager.run()
        labels = self.processor.run(data=labels)
        return_field.add_short(name='labels',
                               value=labels,
                               description='Ontology labels')

        # Queries
        queries = self.query_manager.run()
        queries = self.processor.run(data=queries)

        # Assets
        assets = self.assets_manager.run()
        assets = self.processor.run(data=assets)

        # Labeler (memorize labels at first run)
        queries = self.labeler.run(data=queries,
                                   labels=labels)
        return_field.add_short(name='queries',
                               value=queries,
                               description='Queries')

        assets = self.labeler.run(data=assets)
        return_field.add_short(name='assets',
                               value=assets,
                               description='Assets')
        return return_field


class LabelerClassifierTask(LabelerTask):

    def run(
            self,
    ) -> FieldSet:
        return_field = super().run()

        query_metrics = self.labeler_classifier.run(predictions=return_field.queries.labeler_score,
                                                    ground_truth=return_field.queries.labels,
                                                    labels=return_field.labels,
                                                    show=True)
        return_field.add_short(name='query_metrics',
                               value=query_metrics,
                               description='Ontology matching metric values concerning queries')

        # Assets
        assets_metrics = self.labeler_classifier.run(predictions=return_field.assets.labeler_score,
                                                     ground_truth=return_field.assets.labels,
                                                     labels=return_field.labels,
                                                     show=True)
        return_field.add_short(name='assets_metrics',
                               value=assets_metrics,
                               description='Ontology matching metric values concerning assets')

        return return_field
