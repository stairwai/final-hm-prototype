import abc
from subprocess import Popen
from typing import List, Dict, Any, Iterable, Tuple, Optional

import numpy as np
import spacy
from sentence_transformers import SentenceTransformer
from transformers import pipeline

from core.component import Component
from core.data import FieldSet
from utility.logging_utility import Logger
from utility.metric_utility import get_metric


class TextLabeler(Component):
    """
    A labeler for matching input textual documents to ontology labels.
    """

    def __init__(
            self,
            **kwargs
    ):
        super().__init__(**kwargs)
        self.labels = None

    def configure(
            self,
            labels: FieldSet
    ):
        self.labels = labels

    @abc.abstractmethod
    def compute_score(
            self,
            data: FieldSet,
            labels: FieldSet,
            normalize_scores: bool = False
    ) -> Tuple[Iterable[Dict[str, float]], Optional[Iterable[Any]]]:
        pass

    @abc.abstractmethod
    def run(self,
            data: FieldSet = None,
            labels: FieldSet = None,
            normalize_scores: bool = False
            ) -> FieldSet:
        """
        Computes ontology matching for each input text in the given list.
        Ontology matching for an input text involves computing similarity scores for each ontology label.

        Args:

        Returns:
        """
        pass


class RandomLabeler(TextLabeler):

    def compute_score(
            self,
            data: FieldSet,
            labels: FieldSet,
            normalize_scores: bool = False
    ) -> Tuple[Iterable[Dict[str, float]], Optional[Iterable[Any]]]:
        scores = list(map(lambda _: {name: np.random.random() for name in labels.name}, data.identifier))

        if normalize_scores:
            scores = list(map(lambda score: {k: v / sum(score.values()) for k, v in score.items()}, scores))

        return scores, None

    def run(self,
            data: FieldSet = None,
            labels: FieldSet = None,
            normalize_scores: bool = False
            ) -> FieldSet:
        """
        Computes the unsupervised ontology matching for an input list of resources (e.g., Assets).
        Each input resource is parsed and scored according to the selected zero-shot classifier model.
        Args:

        Returns:
        """
        if self.labels is None:
            assert labels is not None, "Expected some labels for running the labeler."
            self.configure(labels=labels)

        labels = labels if self.labels is None else self.labels

        Logger.get_logger(self.__class__.__name__).info(f'Starting ontology matching:\n'
                                                        f'Total documents: {len(data.identifier)}\n'
                                                        f'Total ontology labels: {len(labels.identifier)}\n')

        labeler_score, labeler_explanation = self.compute_score(data=data,
                                                                labels=labels,
                                                                normalize_scores=normalize_scores)
        if 'label_score' in data:
            data.labeler_score = labeler_score
        else:
            data.add_short(name='labeler_score',
                           value=labeler_score,
                           typing=Iterable[Dict[str, float]],
                           description='Input to label matching score')
        if 'labeler_explanation' in data:
            data.labeler_explanation = labeler_explanation
        else:
            data.add_short(name='labeler_explanation',
                           value=labeler_explanation,
                           typing=Optional[Iterable[Any]],
                           description='Input to label matching score explanation')

        return data


class ZeroShotLabeler(TextLabeler):

    def __init__(self,
                 **kwargs
                 ):
        """
        Instantiates a ZeroShotLabeler object.

        Args:
            labels_filepath (str): the path where labels (in textual format) are stored.
            zero_shot_model_name (str): the name of the zero-shot classifier model to use for ontology matching.
            save_path (str): the path where to save or load SBertLabeler data (e.g., configuration files).
        """

        super(ZeroShotLabeler, self).__init__(**kwargs)
        self.model = pipeline('zero-shot-classification', model=self.model_name)

    def compute_score(
            self,
            data: FieldSet,
            labels: FieldSet,
            normalize_scores: bool = False,
    ) -> Tuple[Iterable[Dict[str, float]], Optional[Iterable[Any]]]:
        text_fields = data.search_by_tag('text')
        assert len(text_fields) == 1, f'Expect only one text field. Got {len(text_fields)}'

        model_scores = self.model(list(text_fields.values())[0].value,
                                  labels.name,
                                  multi_label=True)
        model_scores = list(map(lambda score: {name: score['scores'][score['labels'].index(name)]
                                               for name in labels.name},
                                model_scores))

        if normalize_scores:
            model_scores = list(map(lambda score: {k: v / sum(score.values()) for k, v in score.items()},
                                    model_scores))

        return model_scores, None

    def run(self,
            data: FieldSet = None,
            labels: FieldSet = None,
            normalize_scores: bool = False
            ) -> FieldSet:
        """
        Computes the unsupervised ontology matching for an input list of resources (e.g., Assets).
        Each input resource is parsed and scored according to the selected zero-shot classifier model.

        Args:

        Returns:
        """
        if self.labels is None:
            assert labels is not None, "Expected some labels for running the labeler."
            self.configure(labels=labels)

        labels = labels if self.labels is None else self.labels
        Logger.get_logger(self.__class__.__name__).info(f'Starting ontology matching:\n'
                                                        f'Total documents: {len(data.identifier)}\n'
                                                        f'Total ontology labels: {len(labels.identifier)}\n'
                                                        f'Zero-shot model: {self.model_name}')

        labeler_score, labeler_explanation = self.compute_score(data=data,
                                                                labels=labels,
                                                                normalize_scores=normalize_scores)
        if 'label_score' in data:
            data.labeler_score = labeler_score
        else:
            data.add_short(name='labeler_score',
                           value=labeler_score,
                           typing=Iterable[Dict[str, float]],
                           description='Input to label matching score')
        if 'labeler_explanation' in data:
            data.labeler_explanation = labeler_explanation
        else:
            data.add_short(name='labeler_explanation',
                           value=labeler_explanation,
                           typing=Optional[Iterable[Any]],
                           description='Input to label matching score explanation')

        return data


class SBertLabeler(TextLabeler):
    """
    An extension of the standard TextLabeler to support S-BERT encoding.
    """

    def __init__(self,
                 **kwargs):
        """
        Instantiates a SBertLabeler object.

        Args:
            spacy_model_name (str): the name of the SpaCy tokenizer model to use for sentence splitting.
            strategy (str): the name of strategy to use for performing unsupervised ontology matching.
            sbert_model_name (str): the name of a S-BERT model to instantiate for embedding textual data.
            similarity_metric (str): the name of the similarity metric to adopt for comparing embedding vectors.
        """

        super(SBertLabeler, self).__init__(**kwargs)
        spacy_model = self._setup_spacy_model(spacy_model_name=self.spacy_model_name)
        spacy_model.add_pipe('sentencizer')
        self.spacy_model = spacy_model

        self.sbert_model = SentenceTransformer(self.sbert_model_name)

        similarity_metric, is_metric_embedding_based = get_metric(self.similarity_metric_name)
        self.similarity_metric = similarity_metric
        self.is_metric_embedding_based = is_metric_embedding_based

        self.score_strategy_map = {
            'name2text': self._score_name2text,
            'descr2text': self._score_descr2text,
        }

        assert self.strategy in self.score_strategy_map.keys(), \
            f"Strategy not supported! Got {self.strategy}," \
            f" but currently supporting {list(self.score_strategy_map.keys())}"

    def _setup_spacy_model(
            self,
            spacy_model_name: str
    ):
        if not spacy.util.is_package(spacy_model_name):
            cmd = ['python',
                   '-m spacy',
                   f'download {spacy_model_name}']

            cmd = ' '.join(cmd)
            Logger.get_logger(self.__class__.__name__).info(f'Downloading spacy model...\n{cmd}')

            # Run process
            process = Popen(cmd, shell=True)
            process.wait()

        return spacy.load(spacy_model_name)

    def configure(
            self,
            labels: FieldSet
    ):
        self.labels = self._parse_labels(labels=labels)

    def _parse_labels(
            self,
            labels: FieldSet
    ) -> FieldSet:

        label_text = labels.search_by_tag('text')
        assert len(label_text) == 1, f'Expected only one text field. Got {len(label_text)}'

        label_field_key = list(label_text.keys())[0]
        parsed_label_text = list(map(lambda l: self._split_text(l), label_text[label_field_key]))
        labels[label_field_key] = parsed_label_text

        label_text_embedding = list(map(lambda t: self.sbert_model.encode(t), parsed_label_text))
        labels.add_short(name='description_embedding',
                         value=label_text_embedding)

        label_name_embedding = list(map(lambda t: self.sbert_model.encode(t), labels.name))
        labels.add_short(name='name_embedding',
                         value=label_name_embedding)

        return labels

    def _split_text(
            self,
            text: str
    ) -> List[str]:
        """
        Splits a text into sentences and filters out empty sentences.

        Args:
            text (str): the text to be split into sentences.

        Returns:
             a list of sentences extracted by using the SpaCy tokenizer.
        """

        sentences = [sentence.text for sentence in self.spacy_model(text).sents
                     if len(sentence.text.strip())]
        return sentences

    def _score_name2text(
            self,
            text_sentences: List[str],
            labels: FieldSet,
            normalize_scores: bool = False
    ) -> Tuple[Dict[str, float], Optional[Any]]:
        """
        Computes the similarity between an input sentence list, representing a single resource (e.g., an Asset),
        and the stored list of labels.

        The similarity is computed according to the 'name2text' strategy. In this strategy, the method
        computes the similarity between: a input list of sentences and each label's name and synonyms.

        Args:
            text_sentences (List[str]): a list of input text sentences representing a single resource (e.g., an Asset).

        Returns:
            a dictionary where:
                key: label's identifier
                value: the maximum similarity obtained for that label.
        """

        # M Sentences
        if self.is_metric_embedding_based:
            text_sentences_embedding = self.sbert_model.encode(text_sentences)
        else:
            text_sentences_embedding = text_sentences

        # N Labels
        scores = {}
        explanations = {}
        for name, name_embedding in zip(labels.name, labels.name_embedding):

            if self.is_metric_embedding_based:
                label_embedding = name_embedding
            else:
                label_embedding = name

            # Similarity
            label_similarities = self.similarity_metric(label_embedding, text_sentences_embedding).ravel()

            return_amount = min(len(label_similarities), self.return_amount)

            # Take top (return_amount) scores
            top_indexes = np.argpartition(label_similarities, -return_amount)[-return_amount:]
            top_sorted_indexes = top_indexes[np.argsort(label_similarities[top_indexes])][::-1]
            top_values = label_similarities[top_sorted_indexes]

            column_indexes = np.apply_along_axis(lambda x: x % len(text_sentences), axis=0, arr=top_sorted_indexes)
            top_sentences = text_sentences[column_indexes]

            scores.setdefault(name, np.min(top_values))
            explanations.setdefault(name, list(zip(top_values, top_sentences)))

        if normalize_scores:
            scores = {k: v / sum(scores.values()) for k, v in scores.items()}

        return scores, explanations

    def _score_descr2text(
            self,
            text_sentences: List[str],
            labels: FieldSet,
            normalize_scores: bool = False
    ) -> Tuple[Dict[str, float], Optional[Any]]:
        """
        Computes the similarity between an input sentence list, representing a single resource (e.g., an Asset),
        and the stored list of labels.

        The similarity is computed according to the 'descr2text' strategy. In this strategy, the method
        computes the similarity between: a input list of sentences and each label's textual description.

        Args:
            text_sentences (List[str]): a list of input text sentences representing a single resource (e.g., an Asset).

        Returns:
            a dictionary where:
                key: label's identifier
                value: the maximum similarity obtained for that label.
        """

        # M Sentences
        if self.is_metric_embedding_based:
            text_sentences_embedding = self.sbert_model.encode(text_sentences)
        else:
            text_sentences_embedding = text_sentences

        # N Labels
        scores = {}
        explanations = {}
        for name, description, description_embedding in zip(labels.name,
                                                            labels.description,
                                                            labels.description_embedding):
            if self.is_metric_embedding_based:
                label_embedding = description_embedding
            else:
                label_embedding = description

            if label_embedding.shape[0] > 0:
                # Similarity
                label_similarities = self.similarity_metric(label_embedding, text_sentences_embedding).ravel()

                return_amount = min(len(label_similarities), self.return_amount)

                # Take top (return_amount) scores
                top_indexes = np.argpartition(label_similarities, -return_amount)[-return_amount:]
                top_sorted_indexes = top_indexes[np.argsort(label_similarities[top_indexes])][::-1]
                top_values = label_similarities[top_sorted_indexes]

                column_indexes = np.apply_along_axis(lambda x: x % len(text_sentences), axis=0, arr=top_sorted_indexes)
                top_sentences = np.array(text_sentences)[column_indexes]

                row_indexes = np.apply_along_axis(lambda x: x // len(text_sentences), axis=0,
                                                  arr=top_sorted_indexes).astype(np.int32)
                top_label_sentences = np.array(description)[row_indexes]

                assert len(top_sentences) == len(top_label_sentences), \
                    f'Inconsistent number of sentences for pair definition.' \
                    f'Query sentences {len(top_sentences)} -- label sentences {len(top_label_sentences)}'

                explanation = list(zip(top_values, top_sentences.tolist(), top_label_sentences.tolist()))
                score = np.min(top_values)
            else:
                explanation = None
                score = 0.00

            scores.setdefault(name, score)
            explanations.setdefault(name, explanation)

        if normalize_scores:
            scores = {k: v / sum(scores.values()) for k, v in scores.items()}

        return scores, explanations

    def compute_score(
            self,
            data: FieldSet,
            labels: FieldSet,
            normalize_scores: bool = False
    ) -> Tuple[Iterable[Dict[str, float]], Optional[Iterable[Any]]]:
        text_fields = data.search_by_tag(tags='text')
        assert len(text_fields) == 1, f'Expected only one text field. Got {len(text_fields)}'

        split_text = map(lambda t: self._split_text(text=t), list(text_fields.values())[0].value)
        model_result = list(map(lambda t: self.score_strategy_map[self.strategy](text_sentences=t,
                                                                                 labels=labels,
                                                                                 normalize_scores=normalize_scores),
                                split_text))
        model_scores = [item[0] for item in model_result]
        model_explanations = [item[1] for item in model_result]

        return model_scores, model_explanations

    def run(self,
            data: FieldSet = None,
            labels: FieldSet = None,
            normalize_scores: bool = False
            ) -> FieldSet:
        """
        Computes the unsupervised ontology matching for a input list of resources (e.g., Assets).
        Each input resource is parsed and scored according to the SBertLabeler's strategy.

        Args:

        Returns:
        """
        # Labels
        if self.labels is None:
            assert labels is not None, "Expected some labels for running the labeler."
            self.configure(labels=labels)

        labels = labels if self.labels is None else self.labels

        Logger.get_logger(self.__class__.__name__).info(f'Starting ontology matching:\n'
                                                        f'Total documents: {len(data.identifier)}\n'
                                                        f'Total ontology labels: {len(labels.identifier)}\n'
                                                        f'Strategy: {self.strategy}\n'
                                                        f'S-BERT model: {self.sbert_model}')

        labeler_score, labeler_explanation = self.compute_score(data=data,
                                                                labels=labels,
                                                                normalize_scores=normalize_scores)
        if 'label_score' in data:
            data.labeler_score = labeler_score
        else:
            data.add_short(name='labeler_score',
                           value=labeler_score,
                           typing=Iterable[Dict[str, float]],
                           description='Input to label matching score')
        if 'labeler_explanation' in data:
            data.labeler_explanation = labeler_explanation
        else:
            data.add_short(name='labeler_explanation',
                           value=labeler_explanation,
                           typing=Optional[Iterable[Any]],
                           description='Input to label matching score explanation')
        return data
