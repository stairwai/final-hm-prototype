from typing import Iterable, Dict, Any, List

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from core.component import Component
from core.data import FieldSet
from utility.logging_utility import Logger


class LabelerClassifier(Component):

    def show(
            self,
            metrics: Dict[str, Dict[float, Any]],
            thresholds: Iterable[float]
    ):
        matplotlib.use('tkagg')

        fig, axs = plt.subplots(1, 1)
        legend = []
        for metric_name, metric_value in metrics.items():
            axs.plot(thresholds, [metric_value[threshold] for threshold in thresholds])
            legend.append(metric_name)

        axs.set_xlabel('Threshold')
        axs.set_ylabel('Percentage')
        axs.legend(legend)

        plt.show()

    def run(
            self,
            labels: FieldSet,
            predictions: Iterable[Dict[str, float]],
            ground_truth: Iterable[List[str]],
            thresholds: Iterable[float] = None,
            show: bool = False
    ) -> FieldSet:
        logger = Logger.get_logger(self.__class__.__name__)

        thresholds = thresholds if thresholds is not None else self.thresholds
        assert thresholds is not None, f'Expected a list of thresholds for evaluation.'

        logger.info(f'Computing classification metrics..\n'
                    f'Thresholds: {thresholds}\n'
                    f'Metrics: {self.metrics}')

        ordered_labels = sorted(labels.name)

        metric_values = {}
        for threshold in tqdm(thresholds):
            for label in ordered_labels:
                y_pred = [1 if label.casefold() in list(map(lambda x: x.casefold(), prediction_set))
                               and prediction_set[label] >= threshold else 0
                          for prediction_set in predictions]
                y_true = [1 if label.casefold() in list(map(lambda x: x.casefold(), truth_set)) else 0
                          for truth_set in ground_truth]
                assert len(y_pred) == len(y_true), \
                    f'Inconsistent predictions ({len(y_pred)}) and ground-truth ({len(y_true)}).'

                # Skip due to missing annotations
                if np.sum(y_true) == 0:
                    continue

                for metric in self.metrics:
                    metric_value = metric(y_pred=y_pred, y_true=y_true)
                    metric_values.setdefault(metric.name, {}).setdefault(threshold, []).append(metric_value)

        # Compute average metric score for each threshold value
        for metric_name in metric_values:
            for threshold in thresholds:
                metric_values[metric_name][threshold] = np.mean(metric_values[metric_name][threshold])

        if show:
            logger.info(f'Showing classification metrics...')
            self.show(metrics=metric_values,
                      thresholds=thresholds)

        return_field = FieldSet()
        return_field.add_short(name='metrics',
                               value=metric_values,
                               typing=Dict[str, Dict[float, Any]])
        return return_field
